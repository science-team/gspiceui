v 20110115 2
C 2800 1500 1 270 1 capacitor-1.sym
{
T 3100 2100 5 10 1 1 0 0 1
refdes=C1
T 3100 1700 5 10 1 1 0 0 1
value=1.0n
}
C 1900 2300 1 0 0 resistor-2.sym
{
T 2250 2600 5 10 1 1 0 0 1
refdes=R1
T 2200 2100 5 10 1 1 0 0 1
value=10k
}
N 2800 2400 3000 2400 4
{
T 3000 2500 5 10 1 1 0 0 1
netname=Vout
}
N 1900 2400 1700 2400 4
C 2300 1200 1 0 0 gnd-1.sym
N 1700 1500 3000 1500 4
T 2300 3050 9 10 1 0 0 4 1
Last Updated : 27/09/2011
T 900 200 9 10 1 0 0 0 4
The half power (3dB) point :
   - Calculated	= 1/6.28RC = 15.92 KHz
   - NG-Spice	= 15.92 KHz
   - GNU-Cap	= 15.92 KHz
C 1900 1500 1 90 0 voltage-1.sym
{
T 1400 1600 5 10 0 0 90 0 1
device=VOLTAGE_SOURCE
T 1400 2000 5 10 1 1 180 0 1
refdes=Vin
}
T 2350 3300 9 14 1 0 0 4 1
Single Pole Passive RC Phase Shifter
