v 20110115 2
C 3300 700 1 90 0 resistor-2.sym
{
T 2750 1250 5 10 1 1 0 0 1
refdes=R2
T 2700 1000 5 10 1 1 0 0 1
value=100
}
C 2750 3300 1 0 0 resistor-2.sym
{
T 2850 3550 5 10 1 1 0 0 1
refdes=R4
T 3300 3550 5 10 1 1 0 0 1
value=10K
}
C 4750 3300 1 0 0 resistor-2.sym
{
T 4850 3550 5 10 1 1 0 0 1
refdes=R5
T 5300 3550 5 10 1 1 0 0 1
value=10K
}
C 3750 2500 1 270 0 capacitor-1.sym
{
T 3900 2350 5 10 1 1 180 0 1
refdes=C2
T 3550 1800 5 10 1 1 0 0 1
value=1.0p
}
N 3650 3400 4750 3400 4
N 5950 2700 6550 2700 4
N 5650 3400 5950 3400 4
N 5950 3400 5950 2700 4
N 3650 2700 4450 2700 4
N 4450 2100 4300 2100 4
N 4300 2100 4300 3400 4
N 2450 2700 2100 2700 4
N 900 700 6550 700 4
N 2750 3400 2450 3400 4
N 2450 3400 2450 2500 4
N 5950 2100 5950 700 4
C 4400 400 1 0 0 gnd-1.sym
C 900 3000 1 270 0 vac-1.sym
{
T 1400 3100 5 10 1 1 0 0 1
refdes=Vin
T 1100 2200 5 10 0 1 0 0 1
value=dc 0 ac 1
}
C 6450 2550 1 270 0 resistor-2.sym
{
T 6750 2150 5 10 1 1 0 0 1
refdes=Rout
T 7200 2000 5 10 1 1 180 0 1
value=100K
}
T 3700 5750 9 14 1 0 0 4 1
Ideal Two Pole Steffan RC Phase Shifter
C 600 1150 1 0 0 vdc-1.sym
{
T 1300 1750 5 10 1 1 0 0 1
refdes=Vbias
T 1350 1550 5 10 1 1 0 0 1
value=2.5V
}
N 900 2350 900 2700 4
T 3650 5400 9 10 1 0 0 4 1
Last Updated on 08/08/2005
C 4450 2000 1 0 0 vcvs-1.sym
{
T 5050 2900 5 12 1 1 0 0 1
refdes=E1
T 4450 2000 5 10 0 0 0 0 1
value=10000
}
N 6550 2700 6550 2550 4
N 6550 1650 6550 700 4
T 3750 4500 9 8 1 0 0 4 5
For R1 =   1K Ohm and Vin(freq) = 10.0 GHz phase at Rout =    1.8 Degree
For R1 = 100 Ohm and Vin(freq) = 10.0 GHz phase at Rout =  18.5 Degree
For R1 =   50 Ohm and Vin(freq) = 10.0 GHz phase at Rout = 32.6 Degree
For R1 =   10 Ohm and Vin(freq) = 10.0 GHz phase at Rout = -31.7 Degree
For R1 =     1 Ohm and Vin(freq) = 10.0 GHz phase at Rout =  -7.0 Degree
C 2650 2500 1 90 1 capacitor-1.sym
{
T 2550 2350 5 10 1 1 180 6 1
refdes=C1
T 2900 1800 5 10 1 1 0 6 1
value=1.0p
}
C 3650 2800 1 180 0 resistor-2.sym
{
T 2850 2850 5 10 1 1 0 0 1
refdes=R1
T 3300 2850 5 10 1 1 0 0 1
value=100
}
C 5500 1700 1 180 0 resistor-2.sym
{
T 4700 1300 5 10 1 1 0 0 1
refdes=R3
T 5150 1300 5 10 1 1 0 0 1
value=100
}
N 900 1150 900 700 4
N 2450 1600 4600 1600 4
N 5500 1600 6200 1600 4
N 6200 1600 6200 2700 4
N 2450 2700 2750 2700 4
N 3950 2500 3950 2700 4
