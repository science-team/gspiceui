v 20130925 2
C 40000 40000 0 0 0 title-A3.sym
C 43800 48000 1 0 0 resistor-2.sym
{
T 44200 48350 5 10 0 0 0 0 1
device=RESISTOR
T 44100 48300 5 10 1 1 0 0 1
refdes=R2
T 44000 47700 5 10 1 1 0 0 1
value=309
}
C 46300 48000 1 0 0 resistor-2.sym
{
T 46700 48350 5 10 0 0 0 0 1
device=RESISTOR
T 46600 48300 5 10 1 1 0 0 1
refdes=R4
T 46600 47700 5 10 1 1 0 0 1
value=9.1
}
C 45100 47600 1 270 0 resistor-2.sym
{
T 45450 47200 5 10 0 0 270 0 1
device=RESISTOR
T 44700 47200 5 10 1 1 0 0 1
refdes=R3
T 44600 46900 5 10 1 1 0 0 1
value=500
T 44600 45600 5 10 1 1 0 0 1
comment=0-1000 Ohm
T 45000 45900 5 10 1 1 0 0 1
documentation=Bias
}
C 42100 48000 1 0 0 resistor-2.sym
{
T 42500 48350 5 10 0 0 0 0 1
device=RESISTOR
T 42500 48500 5 10 1 1 180 0 1
refdes=R1
T 42900 48500 5 10 1 1 180 0 1
value=1K
}
C 49100 47200 1 0 0 inductor-1.sym
{
T 49300 47700 5 10 0 0 0 0 1
device=INDUCTOR
T 49100 47000 5 10 1 1 0 0 1
refdes=L2
T 49300 47900 5 10 0 0 0 0 1
symversion=0.1
T 49500 47000 5 10 1 1 0 0 1
value=50nH
}
C 48700 50400 1 270 0 inductor-1.sym
{
T 49200 50200 5 10 0 0 270 0 1
device=INDUCTOR
T 49000 50000 5 10 1 1 0 0 1
refdes=L1
T 49400 50200 5 10 0 0 270 0 1
symversion=0.1
T 49000 49700 5 10 1 1 0 0 1
value=90nH
}
C 48300 49500 1 90 0 capacitor-1.sym
{
T 47600 49700 5 10 0 0 90 0 1
device=CAPACITOR
T 48000 50300 5 10 1 1 180 0 1
refdes=C2
T 47400 49700 5 10 0 0 90 0 1
symversion=0.1
T 47300 49600 5 10 1 1 0 0 1
value=1500pF
}
C 52000 46300 1 90 0 capacitor-1.sym
{
T 51300 46500 5 10 0 0 90 0 1
device=CAPACITOR
T 52200 47100 5 10 1 1 180 0 1
refdes=C6
T 51100 46500 5 10 0 0 90 0 1
symversion=0.1
T 51900 46400 5 10 1 1 0 0 1
value=150pF
}
C 52000 47600 1 90 0 capacitor-1.sym
{
T 51300 47800 5 10 0 0 90 0 1
device=CAPACITOR
T 52300 48300 5 10 1 1 180 0 1
refdes=C4
T 51100 47800 5 10 0 0 90 0 1
symversion=0.1
T 52000 47700 5 10 1 1 0 0 1
value=3pF
}
C 46200 46700 1 90 0 capacitor-1.sym
{
T 45500 46900 5 10 0 0 90 0 1
device=CAPACITOR
T 46400 47500 5 10 1 1 180 0 1
refdes=C1
T 45300 46900 5 10 0 0 90 0 1
symversion=0.1
T 46100 46800 5 10 1 1 0 0 1
value=1uF
}
C 50800 46300 1 90 0 capacitor-1.sym
{
T 50100 46500 5 10 0 0 90 0 1
device=CAPACITOR
T 51000 47100 5 10 1 1 180 0 1
refdes=C5
T 49900 46500 5 10 0 0 90 0 1
symversion=0.1
T 50700 46400 5 10 1 1 0 0 1
value=80pF
T 50200 45600 5 10 1 1 0 0 1
comment=15-150pF
T 50300 45900 5 10 1 1 0 0 1
documentation=Tuning
}
C 52000 48700 1 0 0 capacitor-1.sym
{
T 52200 49400 5 10 0 0 0 0 1
device=CAPACITOR
T 52600 49000 5 10 1 1 0 0 1
refdes=C3
T 52200 49600 5 10 0 0 0 0 1
symversion=0.1
T 52600 48600 5 10 1 1 0 0 1
value=1000pF
}
C 48300 47900 1 0 0 nmos-3.sym
{
T 48900 48400 5 10 0 0 0 0 1
device=SUB_CIRCUIT
T 48900 48300 5 10 1 1 0 0 1
refdes=XQ1
T 48900 48000 5 10 0 1 0 0 1
value=ARF446
T 48900 48000 5 10 1 1 0 0 1
comment=MRF150
}
C 43600 46700 1 90 0 zener-1.sym
{
T 43000 47100 5 10 0 0 90 0 1
device=ZENER_DIODE
T 42900 47500 5 10 1 1 180 0 1
refdes=D1
T 42600 46700 5 10 0 1 0 0 1
value=1N751
T 42600 47000 5 10 1 1 0 0 1
comment=5.1V
T 42600 46700 5 10 1 1 0 0 1
description=1N751A
}
C 41200 46400 1 0 0 vdc-1.sym
{
T 40700 46850 5 10 1 1 0 0 1
refdes=Vsw
T 41900 47250 5 10 0 0 0 0 1
device=VOLTAGE_SOURCE
T 41900 47450 5 10 0 0 0 0 1
footprint=none
T 40500 46550 5 10 1 1 0 0 1
value=DC 30V
}
C 41200 49000 1 0 0 vdc-1.sym
{
T 40600 49450 5 10 1 1 0 0 1
refdes=Vosc
T 41900 49850 5 10 0 0 0 0 1
device=VOLTAGE_SOURCE
T 41900 50050 5 10 0 0 0 0 1
footprint=none
T 40500 49150 5 10 1 1 0 0 1
value=DC 30V
}
C 50100 48800 1 0 0 txline4-1.sym
{
T 50200 48700 5 10 0 1 0 0 1
device=TLIN
T 50300 48500 5 10 1 1 0 0 1
refdes=TL1
T 49500 49100 5 10 0 1 0 0 1
documentation=1/4 wave transformer
T 50100 49100 5 10 1 1 0 0 1
comment=Zo = 26.7
T 50100 48800 5 10 0 0 0 0 1
value=Z0=26.7 F=50MEG NL=0.25
}
C 49900 48500 1 0 0 gnd-1.sym
C 50900 48500 1 0 0 gnd-1.sym
C 48000 45700 1 0 0 gnd-1.sym
C 48000 49200 1 0 0 gnd-1.sym
C 48700 47600 1 0 0 gnd-1.sym
N 47200 48100 48300 48100 4
N 48100 48100 48100 47300 4
N 48100 47300 49100 47300 4
N 50000 47300 51800 47300 4
N 51800 47600 51800 47200 4
N 50900 48900 52000 48900 4
N 51800 48500 51800 48900 4
N 48800 49500 48800 48700 4
N 48800 48900 50100 48900 4
N 44700 48100 46300 48100 4
N 46000 47600 46000 48100 4
N 45200 47600 45200 48100 4
N 43400 47600 43400 48100 4
N 43800 48100 43000 48100 4
N 41500 50500 48800 50500 4
N 48800 50400 48800 50500 4
N 48100 50400 48100 50500 4
N 51800 46200 43400 46200 4
N 43400 46700 43400 46200 4
N 45200 46700 45200 46200 4
N 46000 46700 46000 46200 4
N 41500 48100 42100 48100 4
N 41500 50500 41500 50200 4
C 41400 48700 1 0 0 gnd-1.sym
C 41400 46100 1 0 0 gnd-1.sym
N 52900 48900 53800 48900 4
N 53800 48900 53800 48400 4
T 47400 48300 9 10 1 0 0 0 2
Ferrit
Bead
B 47500 48000 300 200 3 0 0 0 -1 -1 0 -1 -1 -1 -1 -1
C 53700 48400 1 270 0 resistor-2.sym
{
T 54050 48000 5 10 0 0 270 0 1
device=RESISTOR
T 53100 48000 5 10 1 1 0 0 1
refdes=Rload
T 53300 47700 5 10 1 1 0 0 1
value=50
}
C 53700 47200 1 0 0 gnd-1.sym
N 41500 48100 41500 47600 4
T 50800 40900 9 18 1 0 0 0 1
Synrad CO2 Laser - RF Driver IV
T 50500 40100 9 10 1 0 0 0 1
1
T 52100 40100 9 10 1 0 0 0 1
1
T 50500 40400 9 10 1 0 0 0 1
synrad-rf-driver.sch
T 54000 40400 9 10 1 0 0 0 1
2015 - 02 - 08
T 54000 40100 9 10 1 0 0 0 1
M.S.Waters
C 43000 49200 1 0 0 spice-model-1.sym
{
T 43100 49900 5 10 0 1 0 0 1
device=model
T 43100 49800 5 10 1 1 0 0 1
refdes=A1
T 44300 49500 5 10 1 1 0 0 1
model-name=1N751A
T 43600 49300 5 10 1 1 0 0 1
file=../../lib/diode/1n751.mod
}
C 53100 45900 1 0 0 spice-model-1.sym
{
T 53200 46600 5 10 0 1 0 0 1
device=model
T 53200 46500 5 10 1 1 0 0 1
refdes=A2
T 54400 46200 5 10 1 1 0 0 1
model-name=MRF150
T 53700 46000 5 10 1 1 0 0 1
file=../../lib/mosfet/arf446.mod
}
N 50600 47300 50600 47200 4
N 50600 46300 50600 46200 4
N 51800 46300 51800 46200 4
T 50000 49700 9 10 1 0 0 0 4
It seems that there are no good SPICE models for an RF power MOSFET
such as the MRF150. So I've had to analyse this circuit by disecting it
eg. isolating and analysing the feedback path. And by using MOSFET
models that can be modeled ie. for the DC operating point.
T 40500 40500 9 10 1 0 0 0 11
The feedback path incorporates a
first order bandpass notch filter. The
center frequency, Fo is set using C5.
At the center frequency the pole/zero
pair cancel and so the phase change
is zero. The table to the right contains
simulation results where C5 has been
varied and the notch filter center
frequency (Fo) has been determined.
The relationship between C5 and Fo
is approximately linear.
T 44400 40600 9 10 1 0 0 0 10
C5 (pF)
----------
    10
    20
    40
    60
    80
  100
  120
  150
T 45500 40400 9 10 1 0 0 0 11
Fo (MHz)
------------
  56.45
  54.19
  51.68
  48.63
  46.81
  44.86
  43.17
  40.91

N 51000 48800 50800 48800 4
N 50000 48800 50200 48800 4
T 54500 47100 9 10 1 0 0 0 10
Synrad calls TL1 a 1/4
wave transformer. I
think this means it's a
90 deg. phase shifter.
Positive feedback is
required to generate
the oscillator action.
(TL1 would need to be
1.5m long at 50MHz.)

N 48100 46000 48100 46200 4
