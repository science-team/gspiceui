v 20091004 2
C 950 83900 1 270 1 pnp-3.sym
{
T 1350 85200 5 10 1 1 180 6 1
refdes=Q1
T 1150 84800 5 10 1 1 0 0 1
value=C9015
}
C 4800 83250 1 0 0 resistor-2.sym
{
T 5000 83550 5 10 1 1 0 0 1
refdes=R1
T 5400 83550 5 10 1 1 0 0 1
value=12
}
C 2000 82950 1 0 0 resistor-2.sym
{
T 2300 83250 5 10 1 1 0 0 1
refdes=R2
T 2300 82750 5 10 1 1 0 0 1
value=220
}
C 7250 83050 1 90 0 resistor-2.sym
{
T 6950 83750 5 10 1 1 180 0 1
refdes=R3
T 6650 83250 5 10 1 1 0 0 1
value=5.6k
}
C 9450 83450 1 0 0 vdc-1.sym
{
T 8850 84100 5 10 1 1 0 0 1
refdes=Vbatt
T 8750 83800 5 10 1 1 0 0 1
value=DC 12V
}
C 2800 84550 1 0 0 led-1.sym
{
T 3050 84350 5 10 1 1 0 0 1
refdes=LED1
T 2800 84550 5 10 0 1 0 0 1
value=LED
}
C 2650 86700 1 0 0 gnd-1.sym
T 3800 90400 9 18 1 0 0 4 1
Battery Charger 14.4V
C 9250 88950 1 0 0 spice-model-1.sym
{
T 9350 89550 5 10 1 1 0 0 1
refdes=A1
T 10550 89250 5 10 1 1 0 0 1
model-name=BC548
T 9750 89050 5 10 1 1 0 0 1
file=../../lib/bjt/npn/bc548.mod
}
C 9250 87950 1 0 0 spice-model-1.sym
{
T 9350 88550 5 10 1 1 0 0 1
refdes=A2
T 10550 88250 5 10 1 1 0 0 1
model-name=2N5401
T 9750 88050 5 10 1 1 0 0 1
file=../../lib/bjt/pnp/2n5401.mod
}
C 9250 86950 1 0 0 spice-model-1.sym
{
T 9350 87550 5 10 1 1 0 0 1
refdes=A3
T 10550 87250 5 10 1 1 0 0 1
model-name=LED
T 9750 87050 5 10 1 1 0 0 1
file=../../lib/diode/led.mod
}
C 6350 86800 1 0 0 diode-1.sym
{
T 6700 87250 5 10 1 1 0 0 1
refdes=D2
T 6550 86600 5 10 1 1 0 0 1
value=1N914
}
C 9250 85950 1 0 0 spice-model-1.sym
{
T 9350 86550 5 10 1 1 0 0 1
refdes=A4
T 10550 86250 5 10 1 1 0 0 1
model-name=1N914
T 9750 86050 5 10 1 1 0 0 1
file=../../lib/diode/1n914.mod
}
C 3550 83350 1 0 0 led-1.sym
{
T 3800 83150 5 10 1 1 0 0 1
refdes=LED2
T 3550 83350 5 10 0 1 0 0 1
value=LED
}
C 1200 88000 1 0 0 vac-1.sym
{
T 800 88450 5 10 1 1 0 0 1
refdes=Vin
T 1900 88850 5 10 0 0 0 0 1
device=vac
T 1900 89050 5 10 0 0 0 0 1
footprint=none
T 800 88150 5 10 1 1 0 0 1
value=AC 18
}
C 3750 89000 1 180 0 diode-bridge-1.sym
{
T 2850 88075 5 10 1 1 180 0 1
refdes=U1
T 2550 88925 5 8 0 0 180 0 1
device=DIODE-BRIDGE
}
N 1500 88000 1750 88000 4
N 1500 89200 3750 89200 4
N 3750 89200 3750 88000 4
N 2750 89000 4000 89000 4
N 2750 87000 4000 87000 4
C 4900 87800 1 0 0 capacitor-4.sym
{
T 5100 88900 5 10 0 0 0 0 1
device=POLARIZED_CAPACITOR
T 5100 88300 5 10 1 1 0 0 1
refdes=C?
T 5100 88500 5 10 0 0 0 0 1
symversion=0.1
}
C 3800 87900 1 270 0 capacitor-4.sym
{
T 4900 87700 5 10 0 0 270 0 1
device=POLARIZED_CAPACITOR
T 4350 87450 5 10 1 1 0 0 1
refdes=C1
T 4500 87700 5 10 0 0 270 0 1
symversion=0.1
}
C 6400 87850 1 0 0 diode-1.sym
{
T 6750 88300 5 10 1 1 0 0 1
refdes=D1
T 6600 87650 5 10 1 1 0 0 1
value=1N914
}
