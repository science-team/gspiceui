v 20150930 2
C 40000 40000 0 0 0 title-A4.sym
C 46900 44900 1 270 0 resistor-2.sym
{
T 46800 44500 5 10 1 1 0 6 1
refdes=Re
T 46800 44400 5 10 1 1 180 0 1
value=1.0K
}
C 46900 46800 1 270 0 resistor-2.sym
{
T 47200 46400 5 10 1 1 0 0 1
refdes=Rout
T 47200 46200 5 10 1 1 0 0 1
value=2.0K
}
C 46350 43700 1 0 0 gnd-1.sym
C 46400 45600 1 180 0 voltage-2.sym
{
T 45800 45700 5 10 1 1 0 0 1
refdes=Vin
T 45900 45700 5 10 0 1 90 0 1
value=AC 1V
}
N 45500 45100 45500 45400 4
N 45500 44200 45500 44000 4
N 45500 44000 47500 44000 4
N 47000 44900 47500 44900 4
N 47000 46900 47000 46800 4
T 45400 40850 9 16 1 0 0 0 1
Common Emitter Transistor Amplifier
C 45300 45100 1 270 0 voltage-3.sym
{
T 45200 44700 5 10 1 1 0 6 1
refdes=Vbias
T 45200 44500 5 10 1 1 0 6 1
value=DC 3V
}
T 44900 42400 9 10 1 0 0 0 2
This amplifier has a theoretical gain of 44dB
and 3dB bandwidth of approx. 13kHz to 31MHz.
T 48800 40400 9 10 1 0 0 0 1
2015 - 04 - 16
T 48800 40100 9 10 1 0 0 0 1
Mike Waters
T 44900 40400 9 10 1 0 0 0 1
bjt-amp-ce-2.sch
T 45200 40100 9 10 1 0 0 0 1
1
T 46500 40100 9 10 1 0 0 0 1
1
C 47700 44000 1 90 0 capacitor-1.sym
{
T 47000 44200 5 10 0 0 90 0 1
device=CAPACITOR
T 47600 44600 5 10 1 1 0 0 1
refdes=Ce
T 46800 44200 5 10 0 0 90 0 1
symversion=0.1
T 47600 44300 5 10 1 1 180 6 1
value=1.0uF
}
C 46800 46900 1 0 0 vcc-1.sym
C 42700 44300 1 0 0 gnd-1.sym
C 42600 45800 1 0 0 vcc-1.sym
C 43100 44600 1 0 1 vdc-1.sym
{
T 42400 45250 5 10 0 1 0 6 1
refdes=V1
T 42400 45450 5 10 0 0 0 6 1
device=VOLTAGE_SOURCE
T 42400 45650 5 10 0 0 0 6 1
footprint=none
T 42400 45150 5 10 1 1 0 6 1
value=DC 12V
}
C 46400 44900 1 0 0 npn-3.sym
{
T 47300 45400 5 10 0 0 0 0 1
device=NPN_TRANSISTOR
T 47300 45500 5 10 1 1 0 0 1
refdes=Q1
T 47300 45200 5 10 1 1 0 0 1
value=BC548
}
T 40500 40500 9 10 1 0 0 0 3
This schematic can be simulated
using NG-Spice but not GNU-Cap
(last tested 2015-03-17).
C 40500 41400 1 0 0 spice-model-1.sym
{
T 40600 42100 5 10 0 1 0 0 1
device=model
T 40600 42000 5 10 1 1 0 0 1
refdes=A1
T 41800 41700 5 10 1 1 0 0 1
model-name=BC548
T 41000 41500 5 10 1 1 0 0 1
file=../../lib/models/bc548.mod
}
