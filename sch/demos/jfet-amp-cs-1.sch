v 20150930 2
C 40000 40000 0 0 0 title-A4.sym
C 43400 45000 1 270 0 resistor-2.sym
{
T 43750 44600 5 10 0 0 270 0 1
device=RESISTOR
T 43700 44600 5 10 1 1 0 0 1
refdes=Rg
T 43700 44400 5 10 1 1 0 0 1
value=1Meg
}
C 45000 46800 1 270 0 resistor-2.sym
{
T 45350 46400 5 10 0 0 270 0 1
device=RESISTOR
T 45300 46500 5 10 1 1 180 6 1
refdes=Rd
T 45300 46200 5 10 1 1 0 0 1
value=3.3K
}
C 45200 44900 1 90 1 resistor-2.sym
{
T 44850 44500 5 10 0 0 270 2 1
device=RESISTOR
T 45300 44500 5 10 1 1 0 0 1
refdes=Rs
T 45300 44300 5 10 1 1 0 0 1
value=470
}
C 42200 45000 1 0 0 capacitor-1.sym
{
T 42400 45700 5 10 0 0 0 0 1
device=CAPACITOR
T 42300 45300 5 10 1 1 0 0 1
refdes=C1
T 42400 45900 5 10 0 0 0 0 1
symversion=0.1
T 42800 45300 5 10 1 1 0 0 1
value=1nF
}
C 44900 46800 1 0 0 vcc-1.sym
C 48100 45800 1 0 0 vcc-1.sym
C 43900 43700 1 0 0 gnd-1.sym
C 48200 44300 1 0 0 gnd-1.sym
C 40500 40300 1 0 0 spice-model-1.sym
{
T 40600 41000 5 10 0 1 0 0 1
device=model
T 40600 40900 5 10 1 1 0 0 1
refdes=A1
T 41800 40600 5 10 1 1 0 0 1
model-name=2N3819
T 41000 40400 5 10 1 1 0 0 1
file=../../lib/models/2n3819.mod
}
C 48000 44600 1 0 0 vdc-1.sym
{
T 48700 45250 5 10 1 1 0 0 1
refdes=Vcc
T 48700 45450 5 10 0 0 0 0 1
device=VOLTAGE_SOURCE
T 48700 45650 5 10 0 0 0 0 1
footprint=none
T 48700 45050 5 10 1 1 0 0 1
value=DC 20V
}
N 41900 44000 46000 44000 4
N 41900 45200 42200 45200 4
N 43500 45000 43500 45200 4
N 43500 44100 43500 44000 4
T 45700 40900 9 14 1 0 0 0 1
Single Stage N-Channel JFET Amplifier
T 45400 40100 9 10 1 0 0 0 1
1
T 46200 40100 9 10 1 0 0 0 1
1
T 48600 40400 9 10 1 0 0 0 1
2016 - 06 - 26
T 48600 40100 9 10 1 0 0 0 1
Mike Waters
T 44800 40400 9 10 1 0 0 0 1
jfet-amp-cs-1.sch
C 46200 44000 1 90 0 capacitor-1.sym
{
T 45500 44200 5 10 0 0 90 0 1
device=CAPACITOR
T 46100 44600 5 10 1 1 0 0 1
refdes=C2
T 45300 44200 5 10 0 0 90 0 1
symversion=0.1
T 46100 44200 5 10 1 1 0 0 1
value=470uF
}
N 45100 44900 46000 44900 4
C 44600 45000 1 0 0 spice-njfet-1.sym
{
T 45400 45400 5 10 0 0 0 0 1
device=FET_TRANSISTOR
T 45300 45400 5 10 1 1 0 0 1
refdes=J1
T 45200 45200 5 10 1 1 0 0 1
value=2N3819
}
N 45100 44900 45100 45000 4
N 45100 45900 45100 45700 4
T 46500 42400 9 10 1 0 0 0 4
The bandwidth as determined using
NG-Spice is 158Hz to 102MHz. The
in band gain is 22.3dB. The quiescent
drain current is 3.1mA.
T 40500 41700 9 10 1 0 0 0 3
This schematic can be simulated
using NG-Spice but not GNU-Cap
(last tested 2016-06-26).
C 42100 45000 1 90 1 voltage-1.sym
{
T 41600 44900 5 10 0 0 270 2 1
device=VOLTAGE_SOURCE
T 41600 44500 5 10 1 1 0 6 1
refdes=Vin
T 42100 45000 5 10 0 0 270 2 1
value=DC 0.0V AC 1.0V
}
N 41900 44100 41900 44000 4
N 41900 45000 41900 45200 4
C 43700 45100 1 0 0 resistor-2.sym
{
T 44100 45450 5 10 0 0 0 0 1
device=RESISTOR
T 44100 45500 5 10 1 1 180 0 1
refdes=R1
T 44400 45500 5 10 1 1 180 0 1
value=1
}
N 43100 45200 43700 45200 4
T 46600 47000 9 10 1 0 0 0 3
R1 is included so that gate current can be measured.
Short out C1 to investigate the DC bias characteristics
of the FET.
N 42200 44900 43100 44700 4
N 43100 45200 43100 44900 4
N 42200 44900 42200 45200 4
