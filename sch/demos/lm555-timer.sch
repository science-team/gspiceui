v 20150930 2
C 40000 40000 0 0 0 title-A4.sym
C 50400 45100 1 90 1 capacitor-1.sym
{
T 49700 44900 5 10 0 0 270 2 1
device=CAPACITOR
T 50300 44800 5 10 1 1 0 0 1
refdes=C2
T 49500 44900 5 10 0 0 270 2 1
symversion=0.1
T 50300 44500 5 10 1 1 180 6 1
value=10nF
}
C 51100 45500 1 90 1 capacitor-1.sym
{
T 50400 45300 5 10 0 0 270 2 1
device=CAPACITOR
T 51000 45200 5 10 1 1 0 0 1
refdes=C1
T 50200 45300 5 10 0 0 270 2 1
symversion=0.1
T 51000 44900 5 10 1 1 180 6 1
value=10nF
}
C 48100 41700 1 0 0 gnd-1.sym
N 50000 46400 50900 46400 4
N 49100 46800 49100 47300 4
N 48300 46800 48300 47300 4
N 48300 47300 50900 47300 4
N 47500 45500 47400 45500 4
N 47400 47900 47400 45500 4
N 49800 45500 51300 45500 4
N 51300 47900 47400 47900 4
{
T 49600 47700 5 10 1 1 0 0 1
netname=Trigger
}
N 50200 45100 49800 45100 4
N 47500 44200 47500 44400 4
C 50800 43900 1 0 0 gnd-1.sym
C 49700 42800 1 0 0 gnd-1.sym
T 45500 40900 9 14 1 0 0 0 1
LM555 Astable Square Waveform Generator
T 44700 40400 9 10 1 0 0 0 1
timer-lm555.sch
T 45300 40100 9 10 1 0 0 0 1
1
T 46300 40100 9 10 1 0 0 0 1
1
T 48600 40400 9 10 1 0 0 0 1
2018 - 11 - 18
T 48600 40100 9 10 1 0 0 0 1
Jason Uher & Mike Waters
C 47900 42000 1 0 0 vdc-1.sym
{
T 47700 42650 5 10 1 1 0 6 1
refdes=V1
T 48600 42850 5 10 0 0 0 0 1
device=VOLTAGE_SOURCE
T 48600 43050 5 10 0 0 0 0 1
footprint=none
T 47800 42350 5 10 1 1 0 6 1
value=DC 12V
}
C 48000 43200 1 0 0 vcc-1.sym
C 48500 47300 1 0 0 vcc-1.sym
N 51300 47900 51300 45500 4
N 50000 46400 50000 45900 4
N 50000 45900 49800 45900 4
C 47400 43900 1 0 0 gnd-1.sym
C 47500 44000 1 0 0 lm555-1.sym
{
T 49300 44000 5 10 1 1 0 0 1
refdes=X1
T 47500 44000 5 10 0 0 0 0 1
value=LM555
}
N 49800 44400 49800 44000 4
N 50200 44200 50900 44200 4
N 50900 44600 50900 44200 4
T 40200 45800 9 10 1 0 0 0 12
This example can be a bit tricky to simulate.
The following combination has worked :

    Sim. Eng.   :  GNU-Cap
    Probe         :  Rout
    Analysis     :  Transient
    Start Time  :  0
    End Time   :  500usec
    Step Inc.    :  1 usec
    Signal Src  :  None
    Init. Cond.  :  Cold or Warm

T 40300 41900 9 10 1 0 0 0 1
GNU-Cap Osc Freq  =  9.9 KHz
T 40300 42100 9 10 1 0 0 0 1
Calculated Osc Freq = 10.2 KHz
T 40300 42300 9 10 1 0 0 0 1
Data sheet Osc Freq = 11 KHz
T 40200 40300 9 10 1 0 0 0 5
Note : This LM555 SPICE model seems to
           work better at lower frequencies. As
           the frequency increases >50KHz
           oscillator frequencies are
           significantly under estimated.
C 48900 41600 1 0 0 spice-model-1.sym
{
T 49000 42300 5 10 0 1 0 0 1
device=model
T 49000 42200 5 10 1 1 0 0 1
refdes=A1
T 50100 41900 5 10 1 1 0 0 1
model-name=LM555
T 49400 41700 5 10 1 1 0 0 1
file=../../lib/models/lm555.ckt
}
C 51000 46400 1 90 0 resistor-2.sym
{
T 50650 46800 5 10 0 0 90 0 1
device=RESISTOR
T 50700 46900 5 10 1 1 0 6 1
refdes=RA
T 50700 46800 5 10 1 1 180 0 1
value=4.7K
}
C 51000 45500 1 90 0 resistor-2.sym
{
T 50650 45900 5 10 0 0 90 0 1
device=RESISTOR
T 50700 46000 5 10 1 1 0 6 1
refdes=RB
T 50700 45900 5 10 1 1 180 0 1
value=4.7K
}
C 49700 44000 1 270 0 resistor-2.sym
{
T 50050 43600 5 10 0 0 270 0 1
device=RESISTOR
T 50000 43600 5 10 1 1 0 0 1
refdes=Rout
T 50000 43500 5 10 1 1 180 6 1
value=10K
}
T 40300 43000 9 10 1 0 0 0 1
Osc  Freq   = 1.44 / (C1 (RA + 2 x RB))
T 40300 42800 9 10 1 0 0 0 1
Duty Cycle = RB / (RA + 2 x RB)
