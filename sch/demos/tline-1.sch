v 20130925 2
C 40000 40000 0 0 0 title-A4.sym
T 44700 40400 9 10 1 0 0 0 1
tline-1.sch
T 45500 40100 9 10 1 0 0 0 1
1
T 46200 40100 9 10 1 0 0 0 1
1
T 48600 40400 9 10 1 0 0 0 1
2015 - 02 - 13
T 48600 40100 9 10 1 0 0 0 1
Mike Waters
C 44750 46100 1 180 0 resistor-2.sym
{
T 44250 46200 5 10 1 1 0 0 1
refdes=Rs
T 44250 45700 5 10 1 1 0 0 1
value=50
}
C 43650 46000 1 270 0 voltage-2.sym
{
T 43350 45450 5 10 1 1 0 0 1
refdes=Vs
T 43750 45300 5 10 1 1 180 0 1
value=AC 1V
}
C 47650 46000 1 90 1 resistor-2.sym
{
T 47950 45600 5 10 1 1 0 6 1
refdes=Ro
T 47950 45350 5 10 1 1 0 6 1
value=50
}
N 43850 45100 45150 45100 4
C 44400 44800 1 0 0 gnd-1.sym
C 44750 45600 1 0 0 tline.sym
{
T 45650 46000 5 10 0 1 0 0 1
device=T-Line
T 45700 45550 5 10 1 1 0 0 1
refdes=TL1
T 45200 46300 5 10 1 1 0 0 1
value=Z0=50 TD=10nS
}
N 46550 45100 46550 45600 4
N 47550 45100 46550 45100 4
N 45150 45100 45150 45600 4
C 46950 44800 1 0 0 gnd-1.sym
T 45900 40900 9 16 1 0 0 0 1
Loss-less Transmission Line
T 43300 42600 9 10 1 0 0 0 8
For RG58 is a standard 50 Ohm coaxial cable
with a velocity constant of 0.66.

For a transmission delay of 10nS the actual
cable is :
   L = TD x c x v
      = 10e-9 x 3e8 x 0.66
      = 1.98 m
N 47550 46000 46950 46000 4
