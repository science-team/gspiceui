//**************************************************************************************************
//                                          HelpTasks.hpp                                          *
//                                         ---------------                                         *
// Description : This is a helper class for FrmMain, it handles most of the required help          *
//               operations.                                                                       *
// Started     : 2005-06-03                                                                        *
// Last Update : 2018-10-16                                                                        *
// Copyright   : (C) 2005-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef HELPTASKS_HPP
#define HELPTASKS_HPP

// Application includes

#include "TypeDefs.hpp"
#include "Config.hpp"

class FrmMain;

// wxWidgets includes

#include <wx/config.h>
#include <wx/filefn.h>
#include <wx/html/htmlwin.h>
#ifdef __WXMAC__
  #include <wx/image.h>
#endif

//**************************************************************************************************

class HelpTasks : public wxFrame
{
  private :

    FrmMain     * m_poFrmMain;       // Pointer to application main frame
    wxHtmlWindow  m_oHtmlWin;        // The HTML viewer window
    wxString      m_rosInstallPath;  // The root path of the gSpiceUI installation

    // Object initialization functions
    void  Initialize  ( void ); // This function calls the following init. fns
    void  InitInstPath( void );
    void  InitToolBar ( void );
    void  InitHtmlWin ( void );
    void  InitPosnSize( void );

  public :

    explicit  HelpTasks( FrmMain * poFrmMain );
             ~HelpTasks( );

    void  SetAppFrm( FrmMain * poFrmMain ) { m_poFrmMain = poFrmMain; }
    void  SetTitle ( void );

    void  ManualUser   ( void );
    void  ManualNgSpice( void ) {}
    void  ManualGnuCap ( void ) {}
    void  About        ( void );

    // Event handlers
    void  OnToolForwd( wxCommandEvent & roEvtCmd   );
    void  OnToolBckwd( wxCommandEvent & roEvtCmd   );
    void  OnToolClose( wxCommandEvent & roEvtCmd   );
    void  OnFrmClose ( wxCloseEvent   & roEvtClose );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum eFrmItemID
    {
      ID_TOOLBAR = 1,
      ID_TBR_FORWD,
      ID_TBR_BCKWD,
      ID_TBR_CLOSE,

      ID_UNUSED,      // Assigned to controls for which events are not used

      ID_FST = ID_TOOLBAR,
      ID_LST = ID_TBR_CLOSE
    };

    // Leave this as the last line as private access is envoked by this macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // HELPTASKS_HPP
