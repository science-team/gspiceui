//**************************************************************************************************
//                                          DlgPrefs.cpp                                           *
//                                         --------------                                          *
// Started     : 2006-10-17                                                                        *
// Last Update : 2018-11-14                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "DlgPrefs.hpp"

//**************************************************************************************************
// Implement an event table.

wxBEGIN_EVENT_TABLE( DlgPrefs, wxDialog )

  EVT_BUTTON( DlgPrefs::ID_BTN_OK    , DlgPrefs::OnBtnOk     )
  EVT_BUTTON( DlgPrefs::ID_BTN_CANCEL, DlgPrefs::OnBtnCancel )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.
//
// Argument List :
//   poWin - A pointer to the dialog parent window

DlgPrefs::DlgPrefs( wxWindow * poWin ) :
                    wxDialog( poWin, -1, wxT(""), wxDefaultPosition, wxDefaultSize,
                              wxDEFAULT_DIALOG_STYLE, wxDialogNameStr )
{
  // Construct the display objects
  Initialize( );

  // Clear object attributes
  bClear( );
}

//**************************************************************************************************
// Destructor.

DlgPrefs::~DlgPrefs( )
{
}

//**************************************************************************************************
// Initialize object attributes.

void  DlgPrefs::Initialize( void )
{
  SetTitle( wxT(" Preferences") );

  // Call all the initialization functions
  Create( );
  ToolTips( );

  // Layout the of the display objects
  DoLayout( );
}

//**************************************************************************************************
// Create the display objects.

void  DlgPrefs::Create( void )
{
  wxPanel * poPnlPrefs, * poPnlBtns;
//  wxPanel * poPnlLHS, * poPnlRHS;
  wxSize    oSizeCbx;
  int       iWdName=170, iWdValue=130;

  // Create the various underlying panel objects
  poPnlPrefs = new wxPanel( this );
//  poPnlLHS   = new wxPanel( this );
//  poPnlRHS   = new wxPanel( this );
  poPnlBtns  = new wxPanel( this );

  // Create the waveform viewer choice control
  m_oChoEdaSuite.bCreate( poPnlPrefs, ID_CHO_EDASUITE    , iWdName, iWdValue );
  m_oChoEdaSuite.m_oLblName.SetLabel( wxT("EDA Tool Suite") );
  m_oChoEdaSuite.m_oChoice.Append( rosEnumEdaToStr( eEDA_LEPTON ) );
  m_oChoEdaSuite.m_oChoice.Append( rosEnumEdaToStr( eEDA_GEDA   ) );

  // Create the waveform viewer choice control
  m_oChoDataViewer.bCreate( poPnlPrefs, ID_CHO_DATAVIEWER, iWdName, iWdValue );
  m_oChoDataViewer.m_oLblName.SetLabel( wxT("Waveform data viewer") );
  m_oChoDataViewer.m_oChoice.Append( rosEnumVwrToStr( eVIEW_GAW   ) );
  m_oChoDataViewer.m_oChoice.Append( rosEnumVwrToStr( eVIEW_GWAVE ) );

  // Create the temporary file management strategy choice control
  m_oChoTmpFileMgt.bCreate( poPnlPrefs, ID_CHO_TMPFILEMGT, iWdName, iWdValue );
  m_oChoTmpFileMgt.m_oLblName.SetLabel( wxT("Temporary files") );
  m_oChoTmpFileMgt.m_oChoice.Append( wxT("Delete") );
  m_oChoTmpFileMgt.m_oChoice.Append( wxT("Prompt") );
  m_oChoTmpFileMgt.m_oChoice.Append( wxT("Keep")   );

  // Create the main frame layout choice control
  m_oChoFrmLayout.bCreate( poPnlPrefs, ID_CHO_FRMLAYOUT  , iWdName, iWdValue );
  m_oChoFrmLayout.m_oLblName.SetLabel( wxT("Main frame layout") );
  m_oChoFrmLayout.m_oChoice.Append( wxT("Long Probes")  );
  m_oChoFrmLayout.m_oChoice.Append( wxT("Wide Console") );

  // Create the phase / angle units choice control
  m_oChoPhaseUnits.bCreate( poPnlPrefs, ID_CHO_PHASEUNITS, iWdName, iWdValue );
  m_oChoPhaseUnits.m_oLblName.SetLabel( wxT("Phase / angle units") );
  m_oChoPhaseUnits.m_oChoice.Append( wxT("Degrees") );
  m_oChoPhaseUnits.m_oChoice.Append( wxT("Radians") );

  // Create the results precision choice control
  m_oChoPrecision.bCreate( poPnlPrefs, ID_CHO_PRECISION  , iWdName, iWdValue );
  m_oChoPrecision.m_oLblName.SetLabel( wxT("Results precision") );
  m_oChoPrecision.m_oChoice.Append( wxT("%3.1E") );
  m_oChoPrecision.m_oChoice.Append( wxT("%4.2E") );
  m_oChoPrecision.m_oChoice.Append( wxT("%5.3E") );
  m_oChoPrecision.m_oChoice.Append( wxT("%6.4E") );
  m_oChoPrecision.m_oChoice.Append( wxT("%7.5E") );
  m_oChoPrecision.m_oChoice.Append( wxT("%8.6E") );

  // Create the Guile procedure choice control
  m_oChoGuileProc.bCreate( poPnlPrefs, ID_CHO_GUILEPROC  , iWdName, iWdValue );
  m_oChoGuileProc.m_oLblName.SetLabel( wxT("gnetlist Guile backend") );
  m_oChoGuileProc.m_oChoice.Append( PrcGNetList::roasGetGuileProcs( ) );
  m_oChoGuileProc.m_oChoice.Delete( 0 ); // The first item is an empty string

  // Create the wxNotebook maximum line PnlValue control
  m_oPnlNbkMaxLns.bCreate( poPnlPrefs, ID_VAL_NBKMAXLNS  , iWdName, iWdValue-45, 45,
                           wxDefaultPosition, PnlValue::eSHOW_LBL );
  m_oPnlNbkMaxLns.bSetName( wxT("Max text control size") );
  m_oPnlNbkMaxLns.bSetValueType( eVALUE_INT );
  m_oPnlNbkMaxLns.bSetDefValue( TXT_LNSDEF );
  m_oPnlNbkMaxLns.bSetSpnRange( TXT_LNSMIN, TXT_LNSMAX );
  m_oPnlNbkMaxLns.bSetSpnIncSz( 10, 10000 );
  m_oPnlNbkMaxLns.bSetUnits( wxT("Lines") );

  // Create the spin control period PnlValue control
  m_oPnlSpnPeriod.bCreate( poPnlPrefs, ID_VAL_SPINPERIOD , iWdName, iWdValue-45, 45,
                           wxDefaultPosition, PnlValue::eSHOW_LBL );
  m_oPnlSpnPeriod.bSetName( wxT("Spin control period") );
  m_oPnlSpnPeriod.bSetValueType( eVALUE_INT );
  m_oPnlSpnPeriod.bSetDefValue( SPN_PERIOD_DEF );
  m_oPnlSpnPeriod.bSetSpnRange( SPN_PERIOD_MIN, SPN_PERIOD_MAX );
  m_oPnlSpnPeriod.bSetSpnIncSz( 1, 10 );
  m_oPnlSpnPeriod.bSetUnits( wxT("mSec") );

  // Create the spin control period PnlValue control
  m_oPnlToolTipDly.bCreate( poPnlPrefs, ID_VAL_TOOLTIPDLY, iWdName, iWdValue-45, 45,
                            wxDefaultPosition, PnlValue::eSHOW_LBL );
  m_oPnlToolTipDly.bSetName( wxT("Tool tip delay") );
  m_oPnlToolTipDly.bSetValueType( eVALUE_INT );
  m_oPnlToolTipDly.bSetDefValue( CFG_DEF_TOOLTIPDLY );
  m_oPnlToolTipDly.bSetSpnRange( 0, 2000 );
  m_oPnlToolTipDly.bSetSpnIncSz( 100, 100 );
  m_oPnlToolTipDly.bSetUnits( wxT("mSec") );

  // Set height of check box controls according to size of PnlValue object
  oSizeCbx = m_oPnlNbkMaxLns.GetSize( );
  oSizeCbx.SetWidth( 250 );

  // Create the tool tips check box
#if wxCHECK_VERSION( 3,0,0 )
  m_oCbxToolTips   .Create( poPnlPrefs, ID_CBX_TOOLTIPS   , wxT("Show tool tips \t\t\t"),
                            wxDefaultPosition, oSizeCbx   , wxALIGN_RIGHT );
#else
  m_oCbxToolTips   .Create( poPnlPrefs, ID_CBX_TOOLTIPS   , wxT("Show tool tips \t\t\t\t"),
                            wxDefaultPosition, oSizeCbx   , wxALIGN_RIGHT );
#endif

  // Create the synchronize sweep sources check box
  m_oCbxSyncSigSrcs.Create( poPnlPrefs, ID_CBX_SYNCSIGSRCS, wxT("Sync. signal sources \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  m_oCbxSyncTemps  .Create( poPnlPrefs, ID_CBX_SYNCTEMPS  , wxT("Sync. temperatures \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the synchronize sweep sources check box
  m_oCbxKeepNetLst .Create( poPnlPrefs, ID_CBX_KEEPNETLST , wxT("Keep the netlist file \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the include model files check box
  m_oCbxVerboseMode.Create( poPnlPrefs, ID_CBX_VERBOSEMODE, wxT("gnetlist verbose mode \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the include model files check box
  m_oCbxIncludeMode.Create( poPnlPrefs, ID_CBX_INCLUDEMODE, wxT("Include model defs. \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the include model files check box
  m_oCbxEmbedMode  .Create( poPnlPrefs, ID_CBX_EMBEDMODE  , wxT("Embed include files \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the include model files check box
  m_oCbxNoMungeMode.Create( poPnlPrefs, ID_CBX_NOMUNGEMODE, wxT("Fix component prefixes \t\t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the automatic reload of schematic file/s check box
  m_oCbxAutoRegen  .Create( poPnlPrefs, ID_CBX_AUTOREGEN  , wxT("Auto. regenerate netlist \t"),
                            wxDefaultPosition, oSizeCbx, wxALIGN_RIGHT );

  // Create the application path text controls
//  m_oTxtGschem .bCreate( poPnlRight, ID_TXT_GSCHEM , 80, 250 );
//  m_oTxtGnetlst.bCreate( poPnlRight, ID_TXT_GNETLST, 80, 250 );
//  m_oTxtNgspice.bCreate( poPnlRight, ID_TXT_NGSPICE, 80, 250 );
//  m_oTxtGnucap .bCreate( poPnlRight, ID_TXT_GNUCAP , 80, 250 );
//  m_oTxtGaw    .bCreate( poPnlRight, ID_TXT_GAW    , 80, 250 );
//  m_oTxtGwave  .bCreate( poPnlRight, ID_TXT_GWAVE  , 80, 250 );
//  m_oTxtCalc   .bCreate( poPnlRight, ID_TXT_CALC   , 80, 250 );
//  m_oTxtGschem .m_oLblName.SetLabel( wxT("Gschem")     );
//  m_oTxtGnetlst.m_oLblName.SetLabel( wxT("Gnetlist")   );
//  m_oTxtNgspice.m_oLblName.SetLabel( wxT("NG-Spice")   );
//  m_oTxtGnucap .m_oLblName.SetLabel( wxT("GNU-Cap")    );
//  m_oTxtGaw    .m_oLblName.SetLabel( wxT("Gaw")        );
//  m_oTxtGwave  .m_oLblName.SetLabel( wxT("Gwave")      );
//  m_oTxtCalc   .m_oLblName.SetLabel( wxT("Calculator") );

  // Create the buttons
  m_oBtnOk    .Create( poPnlBtns, ID_BTN_OK,     wxT("OK") );
  m_oBtnCancel.Create( poPnlBtns, ID_BTN_CANCEL, wxT("Cancel") );
}

//**************************************************************************************************
// Initialize the tool tips.

void  DlgPrefs::ToolTips( void )
{
  m_oChoEdaSuite   .SetToolTip( wxT(" The EDA Tool Suite ") );
  m_oChoDataViewer .SetToolTip( wxT(" The waveform data viewing utility ") );
  m_oChoTmpFileMgt .SetToolTip( wxT(" The temporary file management strategy ") );
  m_oChoFrmLayout  .SetToolTip( wxT(" The main frame layout ") );
  m_oChoPhaseUnits .SetToolTip( wxT(" The phase / angle units ") );
  m_oChoPrecision  .SetToolTip( wxT(" The precision of the analysis results ") );
  m_oChoGuileProc  .SetToolTip( wxT(" The gnetlist Guile procedure for backend processing ") );
  m_oPnlNbkMaxLns  .SetToolTip( wxT(" The maximum number of lines that can be displayed in a text control ") );
  m_oPnlSpnPeriod  .SetToolTip( wxT(" The period between successive spin button control updates ") );
  m_oPnlToolTipDly .SetToolTip( wxT(" The delay before a tool tip is displayed ") );
  m_oCbxToolTips   .SetToolTip( wxT(" Enable tool tips ") );
  m_oCbxSyncSigSrcs.SetToolTip( wxT(" Synchronize signal sources between analysis pages " ) );
  m_oCbxSyncTemps  .SetToolTip( wxT(" Synchronize temperature values between analysis pages ") );
  m_oCbxKeepNetLst .SetToolTip( wxT(" Exclude the netlist file from the list of temporary files (which may be deleted) ") );
  m_oCbxVerboseMode.SetToolTip( wxT(" Enable gnetlist verbose mode ") );
  m_oCbxIncludeMode.SetToolTip( wxT(" Include model file contents in netlist file (gnetlist include_mode) ") );
  m_oCbxEmbedMode  .SetToolTip( wxT(" Embed file contents in netlist file when .INCLUDE encountered (gnetlist embedd_mode) ") );
  m_oCbxNoMungeMode.SetToolTip( wxT(" Automatically test and fix incorrect component label prefixes (gnetlist nomunge_mode) ") );
  m_oCbxAutoRegen  .SetToolTip( wxT(" Automatically regenerate the netlist file if the schematic file/s are newer ") );

//m_oTxtGschem     .SetToolTip( wxT(" The path to the binary for gschem (schematic capture application) ") );
//m_oTxtGnetlst    .SetToolTip( wxT(" The path to the binary for gnetlist (schematic to netlist converter) ") );
//m_oTxtNgspice    .SetToolTip( wxT(" The path to the binary for NG-Spice (simulation engine) ") );
//m_oTxtGnucap     .SetToolTip( wxT(" The path to the binary for GNU-Cap (simulation engine) ") );
//m_oTxtGaw        .SetToolTip( wxT(" The path to the binary for Gaw (waveform viewer) ") );
//m_oTxtGwave      .SetToolTip( wxT(" The path to the binary for Gwave (waveform viewer) ") );
//m_oTxtCalc       .SetToolTip( wxT(" The path to the binary for the calculator utility ") );
}

//**************************************************************************************************
// Layout the display objects within the dialog.

void  DlgPrefs::DoLayout( void )
{
  wxBoxSizer  * poSzrDlg;
  wxPanel     * poPnlPrefs, * poPnlBtns;
  wxBoxSizer  * poSzrPrefs, * poSzrBtns;
  wxSizerFlags  oFlags;

  // Get pointers to the various panels
  poPnlPrefs = (wxPanel *) m_oPnlNbkMaxLns .GetParent( );
  poPnlBtns  = (wxPanel *) m_oBtnOk        .GetParent( );

  // Create sizers to associate with the panels
  poSzrDlg   = new wxBoxSizer      ( wxVERTICAL   );
  poSzrPrefs = new wxStaticBoxSizer( wxVERTICAL, poPnlPrefs, wxT("") );
  poSzrBtns  = new wxBoxSizer      ( wxHORIZONTAL );

  // Set the sizers to the panels
              SetSizer( poSzrDlg   );
  poPnlPrefs->SetSizer( poSzrPrefs );
  poPnlBtns ->SetSizer( poSzrBtns  );

  // Layout the preferences controls
  oFlags.Align( wxALIGN_LEFT );
  oFlags.Border( wxLEFT | wxRIGHT | wxTOP, 10 );
  poSzrPrefs->Add( &m_oChoEdaSuite   , oFlags );
  oFlags.Border( wxLEFT              ,     10 );
  poSzrPrefs->Add( &m_oChoDataViewer , oFlags );
  poSzrPrefs->Add( &m_oChoTmpFileMgt , oFlags );
  poSzrPrefs->Add( &m_oChoFrmLayout  , oFlags );
  poSzrPrefs->Add( &m_oChoPhaseUnits , oFlags );
  poSzrPrefs->Add( &m_oChoPrecision  , oFlags );
  poSzrPrefs->Add( &m_oChoGuileProc  , oFlags );
  poSzrPrefs->Add( &m_oPnlNbkMaxLns  , oFlags );
  poSzrPrefs->Add( &m_oPnlSpnPeriod  , oFlags );
  poSzrPrefs->Add( &m_oPnlToolTipDly , oFlags );
  oFlags.Border( wxLEFT              ,      7 );
  poSzrPrefs->Add( &m_oCbxToolTips   , oFlags );
  poSzrPrefs->Add( &m_oCbxSyncSigSrcs, oFlags );
  poSzrPrefs->Add( &m_oCbxSyncTemps  , oFlags );
  poSzrPrefs->Add( &m_oCbxKeepNetLst , oFlags );
  poSzrPrefs->Add( &m_oCbxVerboseMode, oFlags );
  poSzrPrefs->Add( &m_oCbxIncludeMode, oFlags );
  poSzrPrefs->Add( &m_oCbxEmbedMode  , oFlags );
  poSzrPrefs->Add( &m_oCbxNoMungeMode, oFlags );
  oFlags.Border( wxLEFT | wxBOTTOM   ,      7 );
  poSzrPrefs->Add( &m_oCbxAutoRegen  , oFlags );

  // Layout the buttons
  oFlags.Border( wxTOP | wxBOTTOM, 10 );
  oFlags.Align( wxALIGN_RIGHT );
  poSzrBtns->Add( &m_oBtnOk    , oFlags );
  poSzrBtns->AddSpacer( 10 );
  oFlags.Align( wxALIGN_LEFT );
  poSzrBtns->Add( &m_oBtnCancel, oFlags );

  // Layout the underlying dialog
  oFlags.Border( wxALL,    15 );
  poSzrDlg->Add( poPnlPrefs, oFlags );
  oFlags.Align( wxALIGN_CENTER );
  oFlags.Border( wxBOTTOM, 15 );
  poSzrDlg->Add( poPnlBtns , oFlags );

  // Set dialogues minimum size and initial size as calculated by the sizer
  poSzrPrefs->SetSizeHints( poPnlPrefs );
  poSzrBtns ->SetSizeHints( poPnlBtns  );
  poSzrDlg  ->SetSizeHints( this       );
}

//**************************************************************************************************
// Load the values into the display controls from the configuration file.

void  DlgPrefs::Load( void )
{
  SetEvtHandlerEnabled( false );

  m_oChoEdaSuite   .m_oChoice.SetSelection( (int) g_oConfig.eGetEdaSuite   ( )     );
  m_oChoDataViewer .m_oChoice.SetSelection( (int) g_oConfig.eGetDataViewer ( )     );
  m_oChoTmpFileMgt .m_oChoice.SetSelection( (int) g_oConfig.eGetTmpFileMgt ( )     );
  m_oChoFrmLayout  .m_oChoice.SetSelection( (int) g_oConfig.uiGetFrmLayout ( )     );
  m_oChoPhaseUnits .m_oChoice.SetSelection( (int) g_oConfig.eGetPhaseUnits ( )     );
  m_oChoPrecision  .m_oChoice.SetSelection( (int) g_oConfig.uiGetPrecision ( ) - 1 );
  m_oChoGuileProc  .m_oChoice.SetStringSelection( g_oConfig.rosGetGuileProc( )     );

  m_oPnlNbkMaxLns  .bSetValue( (long)   g_oConfig.uiGetNbkMaxLns ( ) );
  m_oPnlSpnPeriod  .bSetValue( (double) g_oConfig.uiGetSpnPeriod ( ), -3 );
  m_oPnlToolTipDly .bSetValue( (double) g_oConfig.uiGetToolTipDly( ), -3 );

  m_oCbxToolTips   .SetValue(   g_oConfig.bGetToolTips   ( ) );
  m_oCbxSyncSigSrcs.SetValue(   g_oConfig.bGetSyncSigSrcs( ) );
  m_oCbxSyncTemps  .SetValue(   g_oConfig.bGetSyncTemps  ( ) );
  m_oCbxKeepNetLst .SetValue(   g_oConfig.bGetKeepNetLst ( ) );
  m_oCbxVerboseMode.SetValue(   g_oConfig.bGetVerboseMode( ) );
  m_oCbxIncludeMode.SetValue(   g_oConfig.bGetIncludeMode( ) );
  m_oCbxEmbedMode  .SetValue(   g_oConfig.bGetEmbedMode  ( ) );
  m_oCbxNoMungeMode.SetValue( ! g_oConfig.bGetNoMungeMode( ) );
  m_oCbxAutoRegen  .SetValue(   g_oConfig.bGetAutoRegen  ( ) );

  SetEvtHandlerEnabled( true );
}

//**************************************************************************************************
// Save the values in the display controls in the configuration file.

void  DlgPrefs::Save( void )
{
  SetEvtHandlerEnabled( false );

  g_oConfig.bSetEdaSuite   ( (eTypeEDA)        m_oChoEdaSuite  .m_oChoice.GetSelection( ) );
  g_oConfig.bSetDataViewer ( (eTypeViewer)     m_oChoDataViewer.m_oChoice.GetSelection( ) );
  g_oConfig.bSetTmpFileMgt ( (eTypeTmpFileMgt) m_oChoTmpFileMgt.m_oChoice.GetSelection( ) );
  g_oConfig.bSetFrmLayout  (                   m_oChoFrmLayout .m_oChoice.GetSelection( ) );
  g_oConfig.bSetPhaseUnits ( (eTypeUnits)      m_oChoPhaseUnits.m_oChoice.GetSelection( ) );
  g_oConfig.bSetPrecision  (                   m_oChoPrecision .m_oChoice.GetSelection( ) + 1 );
  g_oConfig.bSetGuileProc  (                   m_oChoGuileProc .m_oChoice.GetStringSelection( ) );

  g_oConfig.bSetNbkMaxLns  ( (uint)           m_oPnlNbkMaxLns .liGetValue( )  );
  g_oConfig.bSetSpnPeriod  ( (uint) (1000.0 * m_oPnlSpnPeriod .dfGetValue( )) );
  g_oConfig.bSetToolTipDly ( (uint) (1000.0 * m_oPnlToolTipDly.dfGetValue( )) );

  g_oConfig.bSetToolTips   (   m_oCbxToolTips   .IsChecked( ) );
  g_oConfig.bSetSyncSigSrcs(   m_oCbxSyncSigSrcs.IsChecked( ) );
  g_oConfig.bSetSyncTemps  (   m_oCbxSyncTemps  .IsChecked( ) );
  g_oConfig.bSetKeepNetLst (   m_oCbxKeepNetLst .IsChecked( ) );
  g_oConfig.bSetVerboseMode(   m_oCbxVerboseMode.IsChecked( ) );
  g_oConfig.bSetIncludeMode(   m_oCbxIncludeMode.IsChecked( ) );
  g_oConfig.bSetEmbedMode  (   m_oCbxEmbedMode  .IsChecked( ) );
  g_oConfig.bSetNoMungeMode( ! m_oCbxNoMungeMode.IsChecked( ) );
  g_oConfig.bSetAutoRegen  (   m_oCbxAutoRegen  .IsChecked( ) );

  g_oConfig.bFlush( );

  SetEvtHandlerEnabled( true );
}

//**************************************************************************************************
// Reset all dialog settings to defaults.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  DlgPrefs::bClear( void )
{
  Load( );

  return( true );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Ok button event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event (not used)

void  DlgPrefs::OnBtnOk( wxCommandEvent & roEvtCmd )
{
  Save( );

  EndModal( wxID_OK );
}

//**************************************************************************************************
// Cancel button event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event (not used)

void  DlgPrefs::OnBtnCancel( wxCommandEvent & roEvtCmd )
{
  Load( );

  EndModal( wxID_CANCEL );
}

//**************************************************************************************************
