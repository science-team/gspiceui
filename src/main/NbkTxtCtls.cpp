//**************************************************************************************************
//                                         NbkTxtCtls.cpp                                          *
//                                        ----------------                                         *
// Started     : 2005-06-14                                                                        *
// Last Update : 2015-04-01                                                                        *
// Copyright   : (C) 2005-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "NbkTxtCtls.hpp"

//**************************************************************************************************
// Constructor.

NbkTxtCtls::NbkTxtCtls( void ) : wxNotebook( )
{
}

//**************************************************************************************************
// Destructor.

NbkTxtCtls::~NbkTxtCtls( )
{
}

//**************************************************************************************************
// Create an instantance of this object.
//
// Argument List :
//   poWin  - The parent window
//   oWinID - The window identifier
//   roPosn - The position
//   roSize - The size
//
// Return Values :
//   true  - Success
//   false - Failure

bool  NbkTxtCtls::bCreate( wxWindow * poWin, wxWindowID oWinID )
{
  bool      bRtn=true;
  wxString  os1;

  // Check if the object has already been created
  if( bIsCreated( ) )             return( true );

  // Create the notebook object to hold the text controls
  if( ! Create( poWin, oWinID ) ) return( false );

  // Allocate memory for the text controls
  TextCtrl * poTxcConsole = new TextCtrl;
  TextCtrl * poTxcNetList = new TextCtrl;
  TextCtrl * poTxcSimultn = new TextCtrl;
  TextCtrl * poTxcNgSpice = new TextCtrl;
  TextCtrl * poTxcGnuCap  = new TextCtrl;

  // Create the text controls
  if( ! poTxcConsole->bCreate( this ) ) bRtn = false;
  if( ! poTxcNetList->bCreate( this ) ) bRtn = false;
  if( ! poTxcSimultn->bCreate( this ) ) bRtn = false;
  if( ! poTxcNgSpice->bCreate( this ) ) bRtn = false;
  if( ! poTxcGnuCap ->bCreate( this ) ) bRtn = false;

  // Initialize the text controls
  os1 = wxT("No console I/O to display");
  if( ! poTxcConsole->bSetInitMsg( os1 ) ) bRtn = false;
  os1 = wxT("No netlist currently loaded");
  if( ! poTxcNetList->bSetInitMsg( os1 ) ) bRtn = false;
  os1 = wxT("No simulation currently defined");
  if( ! poTxcSimultn->bSetInitMsg( os1 ) ) bRtn = false;
  os1 = wxT("No NG-Spice simulation results to display");
  if( ! poTxcNgSpice->bSetInitMsg( os1 ) ) bRtn = false;
  os1 = wxT("No GNU-Cap simulation results to display");
  if( ! poTxcGnuCap ->bSetInitMsg( os1 ) ) bRtn = false;

  // Define tool tips for the wxNotebook and each page
  poTxcConsole->SetToolTip( wxT("Console I/O (non-editable)") );
  poTxcNetList->SetToolTip( wxT("Netlist file (non-editable)") );
  poTxcSimultn->SetToolTip( wxT("Simulation file (editable)") );
  poTxcNgSpice->SetToolTip( wxT("NG-Spice simulation results (non-editable)") );
  poTxcGnuCap ->SetToolTip( wxT("GNU-Cap simulation results (non-editable)") );

  // Add the display objects to the wxNotebook (wxNotebook will delete them)
  AddPage( poTxcConsole, wxT("   Console   ")  );
  AddPage( poTxcNetList, wxT("   NetList    ") );
  AddPage( poTxcSimultn, wxT(" Simulation ")   );
  AddPage( poTxcNgSpice, wxT("  NG-Spice   ")  );
  AddPage( poTxcGnuCap , wxT("  GNU-Cap   ")   );

  bInitialize( );

  return( bRtn );
}

//**************************************************************************************************
// Clear one or all text controls associated with the notebook.
//
// Argument List :
//   ePage - The enumerated page identifier
//
// Return Values :
//   true  - Success
//   false - Failure

bool  NbkTxtCtls::bClear( ePageType ePage )
{
  size_t  szt1;

  // Check that the object has been created
  if( ! bIsCreated( ) ) return( false );

  // Attempt the clear operation
  switch( ePage )
  {
    case ePAGE_CONSOLE :  // Clear a particular page
    case ePAGE_NETLIST :
    case ePAGE_SIMULTN :
    case ePAGE_NGSPICE :
    case ePAGE_GNUCAP  :
    case ePAGE_CURRENT :
      if( ! poGetPage( ePage )->bClear( ) )
        return( false );
      break;

    case ePAGE_ALL :      // Clear all pages
      for( szt1=0; szt1<GetPageCount( ); szt1++ )
        if( ! poGetPage( (ePageType) szt1 )->bClear( ) )
          return( false );
      break;

     default :            // Invalid page identifier
       return( false );
  }

  return( true );
}

//**************************************************************************************************
// Initialize one or all text controls associated with the notebook.
//
// Argument List :
//   ePage - The enumerated page identifier
//
// Return Values :
//   true  - Success
//   false - Failure

bool  NbkTxtCtls::bInitialize( ePageType ePage )
{
  size_t  szt1;

  // Check that the object has been created
  if( ! bIsCreated( ) ) return( false );

  // Attempt the initialize operation
  switch( ePage )
  {
    case ePAGE_CONSOLE :  // Initialize a particular page
    case ePAGE_NETLIST :
    case ePAGE_SIMULTN :
    case ePAGE_NGSPICE :
    case ePAGE_GNUCAP  :
    case ePAGE_CURRENT :
      if( ! poGetPage( ePage )->bInitialize( ) )
        return( false );
      break;

    case ePAGE_ALL :      // Initialize all pages
      for( szt1=0; szt1<GetPageCount( ); szt1++ )
        if( ! poGetPage( (ePageType) szt1 )->bInitialize( ) )
          return( false );
      break;

     default :            // Invalid page identifier
       return( false );
  }

  return( true );
}

//**************************************************************************************************
// Set the page to be displayed.
//
// Argument List :
//   ePage - The enumerated page identifier
//
// Return Values :
//   true  - Success
//   false - Failure

bool  NbkTxtCtls::bSetPage( NbkTxtCtls::ePageType ePage )
{
  // Check if the object has already been created
  if( ! bIsCreated( ) )                        return( false );

  // Check that the page identifier is valid
  if( GetPageCount( ) < ((size_t) ePage + 1) ) return( false );

  SetSelection( (size_t) ePage );

  return( true );
}

//**************************************************************************************************
// Set the insert and show positions in the currently displayed page.
//
// Argument List :
//   liPosn - The position
//
// Return Values :
//   true  - Success
//   false - Failure

bool  NbkTxtCtls::bSetPosn( long liPosn )
{
  TextCtrl * poTxtCtl;

  // Check if the object has already been created
  if( ! bIsCreated( ) )  return( false );

  // Get a pointer to the currently displayed page
  poTxtCtl = static_cast<TextCtrl *>( GetCurrentPage( ) );

  if( poTxtCtl == NULL ) return( false );

  // Set the position
  if( liPosn >= 0 ) poTxtCtl->SetInsertionPoint( liPosn );
  else              poTxtCtl->SetInsertionPointEnd( );

  return( true );
}

//**************************************************************************************************
// Get the enumerated identifier for the currently displayed notebook page.
//
// Return Values :
//   Success - The enumerated page identifier
//   Failure - ePAGE_NONE

NbkTxtCtls::ePageType  NbkTxtCtls::eGetPage( void )
{
  ePageType  ePage;

  // Check if the object has already been created
  if( ! bIsCreated( ) ) return( ePAGE_NONE );

  // Get the enumerated identifier for the currently displayed page
  switch( GetSelection( ) )
  {
    case 0  : ePage = ePAGE_CONSOLE; break;
    case 1  : ePage = ePAGE_NETLIST; break;
    case 2  : ePage = ePAGE_SIMULTN; break;
    case 3  : ePage = ePAGE_NGSPICE; break;
    case 4  : ePage = ePAGE_GNUCAP;  break;
    default : ePage = ePAGE_NONE;
  }

  return( ePage );
}

//**************************************************************************************************
// Get a pointer to the text control currently being displayed.
//
// Argument List :
//   ePage - The enumerated page identifier
//
// Return Values :
//   Success - A pointer to the TextCtrl object
//   Failure - NULL

TextCtrl * NbkTxtCtls::poGetPage( NbkTxtCtls::ePageType ePage )
{
  // Check if the object has already been created
  if( ! bIsCreated( ) )                        return( NULL );

  // Check that the page identifier is valid
  if( ePage == ePAGE_CURRENT ) ePage = eGetPage( );
  if( GetPageCount( ) < ((size_t) ePage + 1) ) return( NULL );

  // Return a pointer to the requested page
  return( static_cast<TextCtrl *>( GetPage( (size_t) ePage ) ) );
}

//**************************************************************************************************
