//**************************************************************************************************
//                                         NbkTxtCtls.hpp                                          *
//                                        ----------------                                         *
// Description : This class extends wxNotebook, it contains all text control objects which appear  *
//               at the bottom of the GUI.                                                         *
// Started     : 2005-06-14                                                                        *
// Last Update : 2018-10-08                                                                        *
// Copyright   : (C) 2005-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef NBKTXTCTLS_HPP
#define NBKTXTCTLS_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/TextCtrl.hpp"

// wxWidgets Includes

#include <wx/notebook.h>

// Local Constant Declarations

#ifndef LAYOUT_MNGR
  #define  NBKTXTCTLS_WIDTH   572
  #define  NBKTXTCTLS_HEIGHT  179
#endif // LAYOUT_MNGR

//**************************************************************************************************

class NbkTxtCtls : public wxNotebook
{
  public :

    // The various page identifiers
    enum ePageType
    {
      ePAGE_CONSOLE = 0,
      ePAGE_NETLIST,
      ePAGE_SIMULTN,
      ePAGE_NGSPICE,
      ePAGE_GNUCAP,

      ePAGE_ALL,
      ePAGE_CURRENT,
      ePAGE_NONE,

      ePAGE_FST = ePAGE_CONSOLE,
      ePAGE_LST = ePAGE_NGSPICE
    };

          NbkTxtCtls( void );
         ~NbkTxtCtls( );

    bool  bCreate   ( wxWindow * poWin, wxWindowID oWinID );
    bool  bIsCreated( void ) { return( GetParent( )!=NULL ? true : false ); }

    bool  bClear     ( ePageType ePage=ePAGE_ALL );
    bool  bInitialize( ePageType ePage=ePAGE_ALL );

    bool  bSetPage( ePageType ePage );
    bool  bSetPosn( long liPosn );

    ePageType   eGetPage( void );
    TextCtrl * poGetPage( ePageType ePage=ePAGE_CURRENT );

    bool  bSetLinesMax( int iLines ) { return( TextCtrl::bSetLinesMax( iLines ) ); }
    bool  bSetLinesDsp( int iLines ) { return( TextCtrl::bSetLinesDsp( iLines ) ); }
};

//**************************************************************************************************

#endif // NBKTXTCTLS_HPP
