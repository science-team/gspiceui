//**************************************************************************************************
//                                          TypeDefs.hpp                                           *
//                                         --------------                                          *
// Description : This header file mostly contains enumerated type definitions which may be used    *
//               anywhere in the application.                                                      *
// Started     : 2007-09-06                                                                        *
// Last Update : 2018-10-26                                                                        *
// Copyright   : (C) 2007-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef TYPEDEFS_HPP
#define TYPEDEFS_HPP

// System Includes

#include <iostream>  // Standard C++ iostream objects eg. cin, cout, cerr
#include <cfloat>    // Defines limits for float types
#include <climits>   // Define basic type limits

// wxWidgets Includes

#include <wx/wx.h>
#include <wx/defs.h>
#include <wx/filename.h>

// External Variables

extern  bool           g_bDebug;   // Declared in App_gSpiceUI.hpp
extern  class  Config  g_oConfig;  // Declared in App_gSpiceUI.hpp

//**************************************************************************************************
// Temporary macro definitions to facilitate transition from wxWidgets library version 2.x to 3.x

#ifndef wxIMPLEMENT_APP
  #define  wxIMPLEMENT_APP(X)        IMPLEMENT_APP(X)
#endif

#ifndef wxBEGIN_EVENT_TABLE
  #define  wxBEGIN_EVENT_TABLE(X,Y)  BEGIN_EVENT_TABLE(X,Y)
#endif

#ifndef wxEND_EVENT_TABLE
  #define  wxEND_EVENT_TABLE( )      END_EVENT_TABLE( )
#endif

#ifndef wxDECLARE_EVENT_TABLE
  #define  wxDECLARE_EVENT_TABLE( )  DECLARE_EVENT_TABLE( )
#endif

//**************************************************************************************************
// Type definitions

#ifndef ulong
  typedef  unsigned long  ulong;
#endif
#ifndef uint
  typedef  unsigned int   uint;
#endif
#ifndef uchar
  typedef  unsigned char  uchar;
#endif

typedef  wxStaticText   wxLabel;

//**************************************************************************************************
// Operating System specific macro declarations

#if defined( __WXMSW__ ) || defined( __WXOSX__ )
  #define  EXP10(X)   pow(10.0,X)
  #define  EXP10F(X)  powf(10.0,X)
#else
  #define  EXP10(X)   exp10(X)
  #define  EXP10F(X)  exp10f(X)
#endif

//**************************************************************************************************
// Useful #defines

#define  GUI_CTRL_HT  26         // The height of the PnlValue control in pixels

// The following #define values where determined using the test utility test_CnvtType
#define  NOVAL_UINT   UINT_MAX   // UINT_MAX  =  4294967295
#define  NOVAL_ULNG   ULONG_MAX  // ULONG_MAX =  18446744073709551615
#define  NOVAL_INT    INT_MIN    // INT_MIN   = -2147483648
                                 // INT_MAX   =  2147483647
#define  NOVAL_LNG    LONG_MIN   // LONG_MIN  = -9223372036854775808
                                 // LONG_MAX  =  9223372036854775807
#define  NOVAL_FLT    FLT_MIN    // FLT_MIN   = 1.17549e-38
                                 // FLT_MAX   = 3.40282e+38
#define  NOVAL_DBL    DBL_MIN    // DBL_MIN   = 2.22507e-308
                                 // DBL_MAX   = 1.79769e+308

//**************************************************************************************************
// Fonts

#define  FONT_NORM   wxFont( 9, wxFONTFAMILY_SWISS,  wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL )
#define  FONT_BOLD   wxFont( 9, wxFONTFAMILY_SWISS,  wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD   )
#define  FONT_SLANT  wxFont( 9, wxFONTFAMILY_SWISS,  wxFONTSTYLE_SLANT,  wxFONTWEIGHT_LIGHT  )
#define  FONT_MONO   wxFont( 8, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL )

//**************************************************************************************************
// Binary or executable file names

#ifndef __WXMSW__
  #define  BIN_LNETLIST  wxT("lepton-netlist")    // Lepton EDA netlist program
  #define  BIN_GNETLIST  wxT("gnetlist")          // gEDA       netlist program
  #define  BIN_LSCHEM    wxT("lepton-schematic")  // Lepton EDA schematic capture program
  #define  BIN_GSCHEM    wxT("gschem")            // gEDA       schematic capture program
  #define  BIN_GWAVE     wxT("gwave2")
  #define  BIN_GAW       wxT("gaw")
  #define  BIN_GNUCAP    wxT("gnucap")
  #define  BIN_NGSPICE   wxT("ngspice")
#else
  #define  BIN_LNETLIST  wxT("lepton-netlist.exe")
  #define  BIN_GNETLIST  wxT("gnetlist.exe")
  #define  BIN_LSCHEM    wxT("lepton-schematic.exe")
  #define  BIN_GSCHEM    wxT("gschem.exe")
  #define  BIN_GWAVE     wxT("gwave2.exe")
  #define  BIN_GAW       wxT("gaw.exe")
  #define  BIN_GNUCAP    wxT("gnucap.exe")
  #define  BIN_NGSPICE   wxT("ngspice.exe")
#endif

//**************************************************************************************************
// Enumerated types

// Enumerated type for the various simulator engine types
enum  eTypeSimEng
{
  eSIMR_NGSPICE,  // NG-Spice
  eSIMR_GNUCAP,   // GNU-Cap

  eSIMR_NONE      // No simulation engine specified
};

// Enumerated type for the various EDA suites
enum  eTypeEDA
{
  eEDA_LEPTON,    // Lepton EDA
  eEDA_GEDA,      // GEDA GAF

  eEDA_NONE       // No simulation engine specified
};

// Enumerated type for the various waveform viewers types
enum  eTypeViewer
{
  eVIEW_GAW,      // Gaw
  eVIEW_GWAVE,    // GWave
//eVIEW_GNUPLOT,  // GNU Plot

  eVIEW_NONE      // No waveform viewer specified
};

// Enumerated type for the various command types which may be performed
enum  eTypeCmd
{
  eCMD_OP,        // Quiescent operating point analysis
  eCMD_DC,        // DC analysis
  eCMD_AC,        // AC analysis
  eCMD_TR,        // Transient analysis
  eCMD_FO,        // Fourier analysis
  eCMD_DI,        // Distortion analysis
  eCMD_NO,        // Noise analysis
  eCMD_PZ,        // Pole-zero analysis
  eCMD_SE,        // Sensitivity analysis
  eCMD_TF,        // Transfer function analysis

  eCMD_OPT,       // OPTIONS command
  eCMD_IC,        // Initial conditions command
  eCMD_PR,        // PRINT command
  eCMD_GEN,       // GENERATOR command (GNU-Cap only)

  eCMD_NONE       // No analysis specified
};

// Enumerated type for the various components
enum  eTypeCpnt
{
  eCPNT_CAP,      // Capacitor
  eCPNT_RES,      // Resistor
  eCPNT_IND,      // Inductor
  eCPNT_CIND,     // Coupled (Mutual) Inductors
  eCPNT_DIODE,    // Diode
  eCPNT_BJT,      // BJT (Bipolar Junction Transistor)
  eCPNT_JFET,     // JFET (Junction Field-Effect Transistor)
  eCPNT_MOSFET,   // MOSFET (Metal-Oxide Semiconductor Field-Effect Transistor)
  eCPNT_MESFET,   // MESFET (Metal–Semiconductor Field Effect Transistor)
  eCPNT_VCVS,     // Voltage Controlled Voltage Source
  eCPNT_CCCS,     // Current Controlled Current Source
  eCPNT_VCCS,     // Voltage Controlled Current Source
  eCPNT_CCVS,     // Current Controlled Voltage Source
  eCPNT_TLINE,    // Lossless Transmission Line
  eCPNT_LTRA,     // Lossy Transmission Line (LTRA)
  eCPNT_URC,      // Uniform Distributed RC Transmission Line (URC)
  eCPNT_TXL,      // Single Lossy Transmission Line (TXL)
  eCPNT_CPL,      // Coupled Multi-conductor Transmission Line (CPL)
  eCPNT_ICS,      // Independent Current Source
  eCPNT_IVS,      // Independent Voltage Source
  eCPNT_NLDS,     // Non-Linear Dependent Source
  eCPNT_NLDCS,    // Non-Linear Dependent Current Source
  eCPNT_NLDVS,    // Non-Linear Dependent Voltage Source
  eCPNT_CCSW,     // Current Controlled Switch
  eCPNT_VCSW,     // Voltage Controlled Switch
  eCPNT_SUBCKT,   // Sub-circuit
  eCPNT_LOGIC,    // Logic Device
  eCPNT_STJ,      // Super-conducting Tunnel Junction

  eCPNT_NONE      // No component specified
};

// The various variable types
enum eTypeValue
{
  eVALUE_BIN,     // Binary
  eVALUE_OCT,     // Octal
  eVALUE_HEX,     // Hexadecimal
  eVALUE_INT,     // Integer
  eVALUE_FLT,     // Floating point
  eVALUE_SCI,     // Floating point (scientific  notation)
  eVALUE_ENG,     // Floating point (engineering notation)

  eVALUE_NONE     // No value type specified
};

// Enumerated type for the various types of units
enum eTypeUnits
{
  eUNITS_CAP,     // Capacitance
  eUNITS_IND,     // Inductance
  eUNITS_RES,     // Resistance
  eUNITS_COND,    // Conductance
  eUNITS_VOLT,    // Voltage
  eUNITS_CURR,    // Current
  eUNITS_TIME,    // Time
  eUNITS_FREQ,    // Frequency
  eUNITS_CHRG,    // Charge
  eUNITS_PHAD,    // Phase / angle in Degree
  eUNITS_PHAR,    // Phase / angle in Radian
  eUNITS_TMPC,    // Temperature in Celcius
  eUNITS_TMPF,    // Temperature in Fahrenheit
  eUNITS_PCT,     // Percentage
  eUNITS_EXP,     // Dimensionless (append an exponent)

  eUNITS_NONE     // No units specified
};

// Enumerated type for the various parameters which may be determined by analysis
enum  eTypeParam
{
  ePARAM_VLT,     // Node or component voltage
  ePARAM_CUR,     // Node or component current
  ePARAM_PWR,     // Node or component power
  ePARAM_RES,     // Input and output resistance

  ePARAM_NONE     // No parameter specified
};

// Enumerated type for the various sub-parameters which may be determined (AC analysis only)
enum  eTypeCpxPt
{
  eCPXPT_MAG,     // Magnitude      of the complex node or component parameter
  eCPXPT_PHASE,   // Phase          of the complex node or component parameter
  eCPXPT_REAL,    // Real part      of the complex node or component parameter
  eCPXPT_IMAG,    // Imaginary part of the complex node or component parameter
  eCPXPT_MAGDB,   // Convert the magnitude to dBV

  eCPXPT_NONE     // No sub-parameter specified
};

// Enumerated type for the various step scale types
enum  eTypeScale
{
  eSCALE_LIN,     // Linear
  eSCALE_LOG,     // Logarithmic
  eSCALE_DEC,     // Decimal
  eSCALE_OCT,     // Octal

  eSCALE_NONE     // No step scale specified
};

// Enumerated type for the initial conditions for a transient analysis
enum  eTypeInitC
{
  eINITC_WARM,    // Warm start
  eINITC_UICS,    // Use initial conditions
  eINITC_COLD,    // Cold start

  eINITC_NONE     // No initial conditions specified
};

// Enumerated type for the temporary file management strategy
enum eTypeTmpFileMgt
{
  eTFM_DELETE,    // Automatically delete temporary files
  eTFM_PROMPT,    // Prompt the user before deleting temporary files
  eTFM_KEEP,      // Don't delete temporary files

  eTFM_NONE       // No temporary file management strategy specified
};

//**************************************************************************************************
// Declare new array types (which are based on wxWidgets types)

WX_DECLARE_OBJARRAY( wxFileName, ArrayFileName );

//**************************************************************************************************
// Function prototypes

wxString   & rosEnumEngToStr( eTypeSimEng eSimEng );
wxString   & rosEnumEdaToStr( eTypeEDA    eEDA    );
wxString   & rosEnumVwrToStr( eTypeViewer eViewer );
wxString   & rosEnumCmdToStr( eTypeCmd    eCmd    );

eTypeSimEng  eStrToEnumEng( const wxString & rosSimEng );
eTypeEDA     eStrToEnumEda( const wxString & rosEDA    );
eTypeViewer  eStrToEnumVwr( const wxString & rosViewer );
eTypeCmd     eStrToEnumCmd( const wxString & rosCmd    );

//**************************************************************************************************

#endif // TYPEDEFS_HPP
