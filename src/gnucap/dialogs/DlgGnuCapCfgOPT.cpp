//**************************************************************************************************
//                                      DlgGnuCapCfgOPT.cpp                                         *
//                                     ---------------------                                       *
// Started     : 2006-09-14                                                                        *
// Last Update : 2018-11-04                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "DlgGnuCapCfgOPT.hpp"

//**************************************************************************************************
// Implement an event table.

wxBEGIN_EVENT_TABLE( DlgGnuCapCfgOPT, wxDialog )

  EVT_BUTTON( DlgGnuCapCfgOPT::ID_BTN_OK      , DlgGnuCapCfgOPT::OnBtnOk       )
  EVT_BUTTON( DlgGnuCapCfgOPT::ID_BTN_DEFAULTS, DlgGnuCapCfgOPT::OnBtnDefaults )
  EVT_BUTTON( DlgGnuCapCfgOPT::ID_BTN_CANCEL  , DlgGnuCapCfgOPT::OnBtnCancel   )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.
//
// Argument List :
//   poWin - A pointer to the dialog parent window

DlgGnuCapCfgOPT::DlgGnuCapCfgOPT( wxWindow * poWin ) :
                                  wxDialog( poWin, -1, wxT(""), wxDefaultPosition, wxDefaultSize,
                                            wxDEFAULT_DIALOG_STYLE, wxDialogNameStr )
{
  SetTitle( wxT(" GNU-Cap OPTIONS Command Setup") );
  Initialize( );
  bClear( );
}

//**************************************************************************************************
// Destructor.

DlgGnuCapCfgOPT::~DlgGnuCapCfgOPT( )
{
}

//**************************************************************************************************
// Initialize object attributes.

void  DlgGnuCapCfgOPT::Initialize( void )
{
  // Call all the initialization functions
  Create( );
  ToolTips( );

  // Layout the of the display objects
  DoLayout( );
}

//**************************************************************************************************
// Create the display objects.

void  DlgGnuCapCfgOPT::Create( void )
{
  wxPanel * poPnlLHS, * poPnlMID, * poPnlRHS, * poPnlRHS1, * poPnlBtns;
  wxSize    oSizeCbx;
  int       iWdName=85, iWdValue=95, iWdUnits=80;

  // Create the necessary panel objects
  poPnlLHS  = new wxPanel( this     );
  poPnlMID  = new wxPanel( this     );
  poPnlRHS  = new wxPanel( this     );
  poPnlRHS1 = new wxPanel( poPnlRHS );
  poPnlBtns = new wxPanel( poPnlRHS );

  // Create the PnlValue controls
  m_oPnlABSTOL  .bCreate( poPnlLHS  ,ID_PNL_ABSTOL  , iWdName, iWdValue, iWdUnits );
  m_oPnlCHGTOL  .bCreate( poPnlLHS  ,ID_PNL_CHGTOL  , iWdName, iWdValue, iWdUnits );
  m_oPnlRELTOL  .bCreate( poPnlLHS  ,ID_PNL_RELTOL  , iWdName, iWdValue, iWdUnits );
  m_oPnlTRTOL   .bCreate( poPnlLHS  ,ID_PNL_TRTOL   , iWdName, iWdValue, iWdUnits );
  m_oPnlVNTOL   .bCreate( poPnlLHS  ,ID_PNL_VNTOL   , iWdName, iWdValue, iWdUnits );
  m_oPnlDAMPST  .bCreate( poPnlLHS  ,ID_PNL_DAMPST  , iWdName, iWdValue, iWdUnits );
  m_oPnlDAMPMAX .bCreate( poPnlLHS  ,ID_PNL_DAMPMAX , iWdName, iWdValue, iWdUnits );
  m_oPnlDAMPMIN .bCreate( poPnlLHS  ,ID_PNL_DAMPMIN , iWdName, iWdValue, iWdUnits );
  m_oPnlDEFL    .bCreate( poPnlLHS  ,ID_PNL_DEFL    , iWdName, iWdValue, iWdUnits );
  m_oPnlDEFW    .bCreate( poPnlLHS  ,ID_PNL_DEFW    , iWdName, iWdValue, iWdUnits );
  m_oPnlDEFAD   .bCreate( poPnlLHS  ,ID_PNL_DEFAD   , iWdName, iWdValue, iWdUnits );
  m_oPnlDEFAS   .bCreate( poPnlLHS  ,ID_PNL_DEFAS   , iWdName, iWdValue, iWdUnits );
  m_oPnlDTMIN   .bCreate( poPnlLHS  ,ID_PNL_DTMIN   , iWdName, iWdValue, iWdUnits );
  m_oPnlDTRATIO .bCreate( poPnlLHS  ,ID_PNL_DTRATIO , iWdName, iWdValue, iWdUnits );
  m_oPnlGMIN    .bCreate( poPnlLHS  ,ID_PNL_GMIN    , iWdName, iWdValue, iWdUnits );
  m_oPnlROUND   .bCreate( poPnlLHS  ,ID_PNL_ROUND   , iWdName, iWdValue, iWdUnits );
  m_oPnlSHORT   .bCreate( poPnlLHS  ,ID_PNL_SHORT   , iWdName, iWdValue, iWdUnits );
  m_oPnlTEMP    .bCreate( poPnlMID  ,ID_PNL_TEMP    , iWdName, iWdValue, iWdUnits );
  m_oPnlTNOM    .bCreate( poPnlMID  ,ID_PNL_TNOM    , iWdName, iWdValue, iWdUnits );
  m_oPnlTRREJECT.bCreate( poPnlMID  ,ID_PNL_TRREJECT, iWdName, iWdValue, iWdUnits );
  m_oPnlTRSTEPG .bCreate( poPnlMID  ,ID_PNL_TRSTEPG , iWdName, iWdValue, iWdUnits );
  m_oPnlTRSTEPH .bCreate( poPnlMID  ,ID_PNL_TRSTEPH , iWdName, iWdValue, iWdUnits );
  m_oPnlTRSTEPS .bCreate( poPnlMID  ,ID_PNL_TRSTEPS , iWdName, iWdValue, iWdUnits );
  m_oPnlFLOOR   .bCreate( poPnlMID  ,ID_PNL_FLOOR   , iWdName, iWdValue, iWdUnits );
  m_oPnlVFLOOR  .bCreate( poPnlMID  ,ID_PNL_VFLOOR  , iWdName, iWdValue, iWdUnits );
  m_oPnlVMAX    .bCreate( poPnlMID  ,ID_PNL_VMAX    , iWdName, iWdValue, iWdUnits );
  m_oPnlVMIN    .bCreate( poPnlMID  ,ID_PNL_VMIN    , iWdName, iWdValue, iWdUnits );
  m_oPnlITL1    .bCreate( poPnlMID  ,ID_PNL_ITL1    , iWdName, iWdValue, iWdUnits );
  m_oPnlITL3    .bCreate( poPnlMID  ,ID_PNL_ITL3    , iWdName, iWdValue, iWdUnits );
  m_oPnlITL4    .bCreate( poPnlMID  ,ID_PNL_ITL4    , iWdName, iWdValue, iWdUnits );
  m_oPnlITL7    .bCreate( poPnlMID  ,ID_PNL_ITL7    , iWdName, iWdValue, iWdUnits );
  m_oPnlITL8    .bCreate( poPnlMID  ,ID_PNL_ITL8    , iWdName, iWdValue, iWdUnits );
  m_oPnlITERMIN .bCreate( poPnlMID  ,ID_PNL_ITERMIN , iWdName, iWdValue, iWdUnits );
  m_oPnlHARMS   .bCreate( poPnlMID  ,ID_PNL_HARMS   , iWdName, iWdValue, iWdUnits );
  m_oPnlRECURS  .bCreate( poPnlRHS1 ,ID_PNL_RECURS  , iWdName, iWdValue, iWdUnits );
  m_oPnlTRANSITS.bCreate( poPnlRHS1 ,ID_PNL_TRANSITS, iWdName, iWdValue, iWdUnits );

  m_oPnlABSTOL  .bSetName( wxT("ABSTOL")    );
  m_oPnlCHGTOL  .bSetName( wxT("CHGTOL")    );
  m_oPnlDAMPMAX .bSetName( wxT("DAMPMAX")   );
  m_oPnlDAMPMIN .bSetName( wxT("DAMPMIN")   );
  m_oPnlDAMPST  .bSetName( wxT("DAMPST")    );
  m_oPnlDEFL    .bSetName( wxT("DEFL")      );
  m_oPnlDEFW    .bSetName( wxT("DEFW")      );
  m_oPnlDEFAD   .bSetName( wxT("DEFAD")     );
  m_oPnlDEFAS   .bSetName( wxT("DEFAS")     );
  m_oPnlDTMIN   .bSetName( wxT("DTMIN")     );
  m_oPnlDTRATIO .bSetName( wxT("DTRATIO")   );
  m_oPnlFLOOR   .bSetName( wxT("FLOOR")     );
  m_oPnlGMIN    .bSetName( wxT("GMIN")      );
  m_oPnlHARMS   .bSetName( wxT("HARMONICS") );
  m_oPnlITERMIN .bSetName( wxT("ITERMIN")   );
  m_oPnlITL1    .bSetName( wxT("ITL1")      );
  m_oPnlITL3    .bSetName( wxT("ITL3")      );
  m_oPnlITL4    .bSetName( wxT("ITL4")      );
  m_oPnlITL7    .bSetName( wxT("ITL7")      );
  m_oPnlITL8    .bSetName( wxT("ITL8")      );
  m_oPnlRECURS  .bSetName( wxT("RECURS")    );
  m_oPnlRELTOL  .bSetName( wxT("RELTOL")    );
  m_oPnlROUND   .bSetName( wxT("ROUND")     );
  m_oPnlSHORT   .bSetName( wxT("SHORT")     );
  m_oPnlTEMP    .bSetName( wxT("TEMP")      );
  m_oPnlTNOM    .bSetName( wxT("TNOM")      );
  m_oPnlTRANSITS.bSetName( wxT("TRANSITS")  );
  m_oPnlTRREJECT.bSetName( wxT("TRREJECT")  );
  m_oPnlTRSTEPG .bSetName( wxT("TRSTEPG")   );
  m_oPnlTRSTEPH .bSetName( wxT("TRSTEPH")   );
  m_oPnlTRSTEPS .bSetName( wxT("TRSTEPS")   );
  m_oPnlTRTOL   .bSetName( wxT("TRTOL")     );
  m_oPnlVFLOOR  .bSetName( wxT("VFLOOR")    );
  m_oPnlVMAX    .bSetName( wxT("VMAX")      );
  m_oPnlVMIN    .bSetName( wxT("VMIN")      );
  m_oPnlVNTOL   .bSetName( wxT("VNTOL")     );

  m_oPnlABSTOL  .bSetUnitsType( eUNITS_CURR );
  m_oPnlCHGTOL  .bSetUnitsType( eUNITS_CHRG );
  m_oPnlDTMIN   .bSetUnitsType( eUNITS_TIME );
  m_oPnlDTRATIO .bSetUnitsType( eUNITS_EXP  );
  m_oPnlGMIN    .bSetUnitsType( eUNITS_COND );
  m_oPnlROUND   .bSetUnitsType( eUNITS_EXP  );
  m_oPnlSHORT   .bSetUnitsType( eUNITS_RES  );
  m_oPnlTEMP    .bSetUnitsType( eUNITS_TMPC );
  m_oPnlTNOM    .bSetUnitsType( eUNITS_TMPC );
  m_oPnlVFLOOR  .bSetUnitsType( eUNITS_VOLT );
  m_oPnlVMIN    .bSetUnitsType( eUNITS_VOLT );
  m_oPnlVMAX    .bSetUnitsType( eUNITS_VOLT );
  m_oPnlVNTOL   .bSetUnitsType( eUNITS_VOLT );

  m_oPnlDAMPMAX .bSetValueType( eVALUE_FLT );
  m_oPnlDAMPMIN .bSetValueType( eVALUE_FLT );
  m_oPnlDAMPST  .bSetValueType( eVALUE_INT );
  m_oPnlDEFL    .bSetValueType( eVALUE_FLT );
  m_oPnlDEFW    .bSetValueType( eVALUE_FLT );
  m_oPnlDEFAD   .bSetValueType( eVALUE_SCI );
  m_oPnlDEFAS   .bSetValueType( eVALUE_SCI );
  m_oPnlFLOOR   .bSetValueType( eVALUE_SCI );
  m_oPnlHARMS   .bSetValueType( eVALUE_INT );
  m_oPnlITERMIN .bSetValueType( eVALUE_INT );
  m_oPnlITL1    .bSetValueType( eVALUE_INT );
  m_oPnlITL3    .bSetValueType( eVALUE_INT );
  m_oPnlITL4    .bSetValueType( eVALUE_INT );
  m_oPnlITL7    .bSetValueType( eVALUE_INT );
  m_oPnlITL8    .bSetValueType( eVALUE_INT );
  m_oPnlRECURS  .bSetValueType( eVALUE_INT );
  m_oPnlRELTOL  .bSetValueType( eVALUE_FLT );
  m_oPnlTRANSITS.bSetValueType( eVALUE_INT );
  m_oPnlTRREJECT.bSetValueType( eVALUE_FLT );
  m_oPnlTRSTEPG .bSetValueType( eVALUE_SCI );
  m_oPnlTRSTEPH .bSetValueType( eVALUE_SCI );
  m_oPnlTRSTEPS .bSetValueType( eVALUE_FLT );
  m_oPnlTRTOL   .bSetValueType( eVALUE_FLT );

  m_oPnlDAMPMAX .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlDAMPMIN .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlDAMPST  .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlDEFL    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlDEFW    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlDEFAD   .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlDEFAS   .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlFLOOR   .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlHARMS   .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlITERMIN .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlITL1    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlITL3    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlITL4    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlITL7    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlITL8    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlRECURS  .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlRELTOL  .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTEMP    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTNOM    .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTRANSITS.bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTRREJECT.bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTRSTEPG .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTRSTEPH .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTRSTEPS .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlTRTOL   .bShowUnits( PnlValue::eSHOW_LBL );

  m_oPnlDEFL    .bSetUnits( wxT("uMetre") );
  m_oPnlDEFW    .bSetUnits( wxT("uMetre") );
  m_oPnlDEFAD   .bSetUnits( wxT("Metre²") );
  m_oPnlDEFAS   .bSetUnits( wxT("Metre²") );
  m_oPnlRELTOL  .bSetUnits( wxT("%")      );

  m_oPnlDEFAD   .bSetSpnRange( 1.0E-12, 1.0E-06 );
  m_oPnlDEFAS   .bSetSpnRange( 1.0E-12, 1.0E-06 );
  m_oPnlFLOOR   .bSetSpnRange( 1.0E-23, 1.0E-18 );
  m_oPnlTRSTEPG .bSetSpnRange( 1.0E+00, 1.0E+99 );
  m_oPnlTRSTEPH .bSetSpnRange( 1.0E+00, 1.0E+99 );
  m_oPnlDEFL    .bSetSpnRange( 0.0    , 10000.0 ); m_oPnlDEFL    .bSetSpnIncSz( 1.0 , 1000.0 );
  m_oPnlDEFW    .bSetSpnRange( 0.0    , 10000.0 ); m_oPnlDEFW    .bSetSpnIncSz( 1.0 , 1000.0 );
  m_oPnlTRREJECT.bSetSpnRange( 0.01   ,   100.0 ); m_oPnlTRREJECT.bSetSpnIncSz( 0.01,   10.0 );
  m_oPnlTRSTEPS .bSetSpnRange( 0.01   ,   100.0 ); m_oPnlTRSTEPS .bSetSpnIncSz( 0.01,   10.0 );
  m_oPnlDAMPMAX .bSetSpnRange( 0.0    ,     1.0 ); m_oPnlDAMPMAX .bSetSpnIncSz( 0.01,    0.1 );
  m_oPnlDAMPMIN .bSetSpnRange( 0.0    ,     1.0 ); m_oPnlDAMPMIN .bSetSpnIncSz( 0.01,    0.1 );
  m_oPnlRELTOL  .bSetSpnRange( 0.01   ,   100.0 ); m_oPnlRELTOL  .bSetSpnIncSz( 0.01,   10.0 );
  m_oPnlTRTOL   .bSetSpnRange( 0.01   ,   100.0 ); m_oPnlTRTOL   .bSetSpnIncSz( 0.01,   10.0 );
  m_oPnlDAMPST  .bSetSpnRange( 0      ,    37   ); m_oPnlDAMPST  .bSetSpnIncSz( 1   ,    1   );
  m_oPnlHARMS   .bSetSpnRange( 1      ,   100   ); m_oPnlHARMS   .bSetSpnIncSz( 1   ,   10   );
  m_oPnlITL1    .bSetSpnRange( 0      ,  1000   ); m_oPnlITL1    .bSetSpnIncSz( 1   ,  100   );
  m_oPnlITL3    .bSetSpnRange( 0      ,  1000   ); m_oPnlITL3    .bSetSpnIncSz( 1   ,  100   );
  m_oPnlITL4    .bSetSpnRange( 0      ,  1000   ); m_oPnlITL4    .bSetSpnIncSz( 1   ,  100   );
  m_oPnlITL7    .bSetSpnRange( 0      ,  1000   ); m_oPnlITL7    .bSetSpnIncSz( 1   ,  100   );
  m_oPnlITL8    .bSetSpnRange( 0      ,  1000   ); m_oPnlITL8    .bSetSpnIncSz( 1   ,  100   );
  m_oPnlITERMIN .bSetSpnRange( 0      ,  1000   ); m_oPnlITERMIN .bSetSpnIncSz( 1   ,  100   );
  m_oPnlRECURS  .bSetSpnRange( 0      ,  1000   ); m_oPnlRECURS  .bSetSpnIncSz( 1   ,  100   );
  m_oPnlTRANSITS.bSetSpnRange( 1      ,   100   ); m_oPnlTRANSITS.bSetSpnIncSz( 1   ,   10   );

  // Create the choice box controls
  m_oChoMETHOD.bCreate( poPnlRHS1, ID_CHO_METHOD, iWdName, iWdValue );
  m_oChoMETHOD.m_oLblName.SetLabel( wxT("METHOD") );
  m_oChoMETHOD.m_oChoice.Append( wxT("Euler")     );
  m_oChoMETHOD.m_oChoice.Append( wxT("EulerOnly") );
  m_oChoMETHOD.m_oChoice.Append( wxT("Trap")      );
  m_oChoMETHOD.m_oChoice.Append( wxT("TrapOnly")  );

  m_oChoMODE  .bCreate( poPnlRHS1, ID_CHO_MODE, iWdName, iWdValue );
  m_oChoMODE  .m_oLblName.SetLabel( wxT("MODE") );
  m_oChoMODE  .m_oChoice.Append( wxT("Analog")  );
  m_oChoMODE  .m_oChoice.Append( wxT("Digital") );
  m_oChoMODE  .m_oChoice.Append( wxT("Mixed")   );

  m_oChoORDER .bCreate( poPnlRHS1, ID_CHO_ORDER, iWdName, iWdValue );
  m_oChoORDER .m_oLblName.SetLabel( wxT("ORDER") );
  m_oChoORDER .m_oChoice.Append( wxT("Forward") );
  m_oChoORDER .m_oChoice.Append( wxT("Reverse") );
  m_oChoORDER .m_oChoice.Append( wxT("Auto")    );

  // Set height of check box controls according to size of PnlValue object
  oSizeCbx.SetWidth ( 250 );
  oSizeCbx.SetHeight( GUI_CTRL_HT+3 );

  // Create the check box controls
  m_oCbxBYPASS  .Create( poPnlRHS1, ID_CBX_BYPASS  , wxT("BYPASS \t\t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxCSTRAY  .Create( poPnlRHS1, ID_CBX_CSTRAY  , wxT("CSTRAY   \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxFBBYPASS.Create( poPnlRHS1, ID_CBX_FBBYPASS, wxT("FBBYPASS \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxINCMODE .Create( poPnlRHS1, ID_CBX_INCMODE , wxT("INCMODE  \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxLUBYPASS.Create( poPnlRHS1, ID_CBX_LUBYPASS, wxT("LUBYPASS \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxOPTS    .Create( poPnlRHS1, ID_CBX_OPTS    , wxT("OPTS   \t\t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxQUITCONV.Create( poPnlRHS1, ID_CBX_QUITCONV, wxT("QUITCONV \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxRSTRAY  .Create( poPnlRHS1, ID_CBX_RSTRAY  , wxT("RSTRAY   \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );
  m_oCbxTRACEL  .Create( poPnlRHS1, ID_CBX_TRACEL  , wxT("TRACEL   \t    "), wxDefaultPosition,
                         oSizeCbx, wxALIGN_RIGHT );

  // Create the buttons
  m_oBtnOk      .Create( poPnlBtns, ID_BTN_OK      , wxT("OK")       );
  m_oBtnDefaults.Create( poPnlBtns, ID_BTN_DEFAULTS, wxT("Defaults") );
  m_oBtnCancel  .Create( poPnlBtns, ID_BTN_CANCEL  , wxT("Cancel")   );
}

//**************************************************************************************************
// Initialize the tool tips.

void  DlgGnuCapCfgOPT::ToolTips( void )
{
  // Define tool tips for each control
  m_oPnlABSTOL  .SetToolTip( wxT(" Absolute current error tolerance ") );
  m_oPnlCHGTOL  .SetToolTip( wxT(" Charge tolerance ") );
  m_oPnlDAMPMAX .SetToolTip( wxT(" Normal Newton damping factor (when all is well) ") );
  m_oPnlDAMPMIN .SetToolTip( wxT(" Newton damping factor when there's a problem ") );
  m_oPnlDAMPST  .SetToolTip( wxT(" Damping strategy ") );
  m_oPnlDEFL    .SetToolTip( wxT(" MOSFET default channel length ") );
  m_oPnlDEFW    .SetToolTip( wxT(" MOSFET default channel width ") );
  m_oPnlDEFAD   .SetToolTip( wxT(" MOSFET default drain diffusion area ") );
  m_oPnlDEFAS   .SetToolTip( wxT(" MOSFET default source diffusion area ") );
  m_oPnlDTMIN   .SetToolTip( wxT(" Minimum time step ") );
  m_oPnlDTRATIO .SetToolTip( wxT(" Ratio between the minimum and maximum time step ") );
  m_oPnlFLOOR   .SetToolTip( wxT(" Effective zero value ") );
  m_oPnlGMIN    .SetToolTip( wxT(" Minimum allowable conductance ") );
  m_oPnlHARMS   .SetToolTip( wxT(" Harmonics in Fourier analysis ") );
  m_oPnlITERMIN .SetToolTip( wxT(" Number of extra iterations after convergence ") );
  m_oPnlITL1    .SetToolTip( wxT(" DC iteration limit ") );
  m_oPnlITL3    .SetToolTip( wxT(" Transient lower iteration limit ") );
  m_oPnlITL4    .SetToolTip( wxT(" Transient upper iteration limit ") );
  m_oPnlITL7    .SetToolTip( wxT(" Worst case analysis iteration limit ") );
  m_oPnlITL8    .SetToolTip( wxT(" Convergence diagnostic iteration threshold ") );
  m_oPnlRECURS  .SetToolTip( wxT(" Limit recursion levels in expression evaluation ") );
  m_oPnlRELTOL  .SetToolTip( wxT(" Relative error tolerance ") );
  m_oPnlROUND   .SetToolTip( wxT(" Numeric rounding tolerance ") );
  m_oPnlSHORT   .SetToolTip( wxT(" Resistance of voltage source or short ") );
  m_oPnlTEMP    .SetToolTip( wxT(" Ambient / operating temperature of the circuit when simulated ") );
  m_oPnlTNOM    .SetToolTip( wxT(" Nominal temperature at which device parameters were measured ") );
  m_oPnlTRANSITS.SetToolTip( wxT(" Mixed mode transition count ") );
  m_oPnlTRREJECT.SetToolTip( wxT(" Transient error rejection threshold ") );
  m_oPnlTRSTEPG .SetToolTip( wxT(" Maximum internal step size growth in Transient analysis ") );
  m_oPnlTRSTEPH .SetToolTip( wxT(" Maximum internal step size growth in TR analysis when iteration exceeds it13 but still converges ") );
  m_oPnlTRSTEPS .SetToolTip( wxT(" Amount to shrink step size when convergence fails in TR anaylis ") );
  m_oPnlTRTOL   .SetToolTip( wxT(" Transient analysis error tolerance ") );
  m_oPnlVFLOOR  .SetToolTip( wxT(" Effective zero value for voltage probes ") );
  m_oPnlVMAX    .SetToolTip( wxT(" Positive node voltage limit ") );
  m_oPnlVMIN    .SetToolTip( wxT(" Negative node voltage limit ") );
  m_oPnlVNTOL   .SetToolTip( wxT(" Absolute voltage error tolerance ") );

  m_oCbxBYPASS  .SetToolTip( wxT(" Bypass model evaluation if appropriate ") );
  m_oCbxCSTRAY  .SetToolTip( wxT(" Include capacitance in device models ") );
  m_oCbxFBBYPASS.SetToolTip( wxT(" Skip matrix solution the last iteration ") );
  m_oCbxINCMODE .SetToolTip( wxT(" Incrementally update the matrix ") );
  m_oCbxLUBYPASS.SetToolTip( wxT(" Bypass parts of LU decomposition if appropriate ") );
  m_oCbxOPTS    .SetToolTip( wxT(" Enable printing of option values ") );
  m_oCbxQUITCONV.SetToolTip( wxT(" Quit on convergence failure in TR analysis ") );
  m_oCbxRSTRAY  .SetToolTip( wxT(" Include series resistance in device models ") );
  m_oCbxTRACEL  .SetToolTip( wxT(" Use a queue to only load changed elements to the matrix ") );

  m_oChoMETHOD.GetParent( )->SetToolTip( wxT(" Numerical integration method ") );
  m_oChoMODE  .GetParent( )->SetToolTip( wxT(" Simulation mode selection ") );
  m_oChoORDER .GetParent( )->SetToolTip( wxT(" Equation ordering ") );
}

//**************************************************************************************************
// Layout the dialog display objects.

void  DlgGnuCapCfgOPT::DoLayout( void )
{
  wxBoxSizer  * poSzrDlg;
  wxPanel     * poPnlLHS, * poPnlMID, * poPnlRHS, * poPnlRHS1, * poPnlBtns;
  wxBoxSizer  * poSzrLHS, * poSzrMID, * poSzrRHS, * poSzrRHS1, * poSzrBtns;
  wxSizerFlags  oFlags;

  // Get pointers to the various panels
  poPnlLHS  = (wxPanel *) m_oPnlABSTOL.GetParent( );
  poPnlMID  = (wxPanel *) m_oPnlTEMP  .GetParent( );
  poPnlRHS1 = (wxPanel *) m_oCbxBYPASS.GetParent( );
  poPnlBtns = (wxPanel *) m_oBtnOk    .GetParent( );
  poPnlRHS  = (wxPanel *) poPnlBtns  ->GetParent( );

  // Create sizers to associate with the panels
  poSzrDlg  = new wxBoxSizer      ( wxHORIZONTAL );
  poSzrLHS  = new wxStaticBoxSizer( wxVERTICAL, poPnlLHS  );
  poSzrMID  = new wxStaticBoxSizer( wxVERTICAL, poPnlMID  );
  poSzrRHS1 = new wxStaticBoxSizer( wxVERTICAL, poPnlRHS1 );
  poSzrBtns = new wxBoxSizer      ( wxHORIZONTAL );
  poSzrRHS  = new wxBoxSizer      ( wxVERTICAL );

  // Set the sizers to the panels
             SetSizer( poSzrDlg );
  poPnlLHS ->SetSizer( poSzrLHS );
  poPnlMID ->SetSizer( poSzrMID );
  poPnlRHS ->SetSizer( poSzrRHS );
  poPnlRHS1->SetSizer( poSzrRHS1 );
  poPnlBtns->SetSizer( poSzrBtns );

  // Layout the left hand panel
  oFlags.Align( wxALIGN_LEFT );
  oFlags.Border( wxTOP | wxLEFT | wxRIGHT, 10 );
  poSzrLHS->Add(  &m_oPnlABSTOL  , oFlags );
  oFlags.Border( wxLEFT                  , 10 );
  poSzrLHS->Add(  &m_oPnlCHGTOL  , oFlags );
  poSzrLHS->Add(  &m_oPnlRELTOL  , oFlags );
  poSzrLHS->Add(  &m_oPnlTRTOL   , oFlags );
  poSzrLHS->Add(  &m_oPnlVNTOL   , oFlags );
  poSzrLHS->Add(  &m_oPnlDAMPST  , oFlags );
  poSzrLHS->Add(  &m_oPnlDAMPMAX , oFlags );
  poSzrLHS->Add(  &m_oPnlDAMPMIN , oFlags );
  poSzrLHS->Add(  &m_oPnlDEFL    , oFlags );
  poSzrLHS->Add(  &m_oPnlDEFW    , oFlags );
  poSzrLHS->Add(  &m_oPnlDEFAD   , oFlags );
  poSzrLHS->Add(  &m_oPnlDEFAS   , oFlags );
  poSzrLHS->Add(  &m_oPnlDTMIN   , oFlags );
  poSzrLHS->Add(  &m_oPnlDTRATIO , oFlags );
  poSzrLHS->Add(  &m_oPnlGMIN    , oFlags );
  poSzrLHS->Add(  &m_oPnlROUND   , oFlags );
  oFlags.Border( wxLEFT | wxBOTTOM       , 10 );
  poSzrLHS->Add(  &m_oPnlSHORT   , oFlags );

  // Layout the middle panel
  oFlags.Border( wxTOP | wxLEFT | wxRIGHT, 10 );
  poSzrMID->Add(  &m_oPnlTEMP    , oFlags );
  oFlags.Border( wxLEFT                  , 10 );
  poSzrMID->Add(  &m_oPnlTNOM    , oFlags );
  poSzrMID->Add(  &m_oPnlTRREJECT, oFlags );
  poSzrMID->Add(  &m_oPnlTRSTEPG , oFlags );
  poSzrMID->Add(  &m_oPnlTRSTEPH , oFlags );
  poSzrMID->Add(  &m_oPnlTRSTEPS , oFlags );
  poSzrMID->Add(  &m_oPnlFLOOR   , oFlags );
  poSzrMID->Add(  &m_oPnlVFLOOR  , oFlags );
  poSzrMID->Add(  &m_oPnlVMAX    , oFlags );
  poSzrMID->Add(  &m_oPnlVMIN    , oFlags );
  poSzrMID->Add(  &m_oPnlITL1    , oFlags );
  poSzrMID->Add(  &m_oPnlITL3    , oFlags );
  poSzrMID->Add(  &m_oPnlITL4    , oFlags );
  poSzrMID->Add(  &m_oPnlITL7    , oFlags );
  poSzrMID->Add(  &m_oPnlITL8    , oFlags );
  poSzrMID->Add(  &m_oPnlITERMIN , oFlags );
  oFlags.Border( wxLEFT | wxBOTTOM       , 10 );
  poSzrMID->Add( &m_oPnlHARMS    , oFlags );

  // Layout the right hand options panel
  oFlags.Border( wxTOP | wxLEFT | wxRIGHT, 10 );
  poSzrRHS1->Add( &m_oPnlRECURS  , oFlags );
  oFlags.Border( wxLEFT                  , 10 );
  poSzrRHS1->Add( &m_oPnlTRANSITS, oFlags );
  poSzrRHS1->Add( &m_oChoMETHOD  , oFlags );
  poSzrRHS1->Add( &m_oChoMODE    , oFlags );
  poSzrRHS1->Add( &m_oChoORDER   , oFlags );
  oFlags.Border( wxLEFT                  ,  7 );
  poSzrRHS1->Add( &m_oCbxBYPASS  , oFlags );
  poSzrRHS1->Add( &m_oCbxCSTRAY  , oFlags );
  poSzrRHS1->Add( &m_oCbxFBBYPASS, oFlags );
  poSzrRHS1->Add( &m_oCbxINCMODE , oFlags );
  poSzrRHS1->Add( &m_oCbxLUBYPASS, oFlags );
  poSzrRHS1->Add( &m_oCbxOPTS    , oFlags );
  poSzrRHS1->Add( &m_oCbxQUITCONV, oFlags );
  poSzrRHS1->Add( &m_oCbxRSTRAY  , oFlags );
  oFlags.Border( wxLEFT | wxBOTTOM       ,  7 );
  poSzrRHS1->Add( &m_oCbxTRACEL  , oFlags );

  // Layout the buttons panel
  oFlags.Align( wxALIGN_RIGHT  );
  oFlags.Border( wxTOP | wxBOTTOM        , 12 );
  poSzrBtns->Add( &m_oBtnOk      , oFlags );
  poSzrBtns->AddSpacer( 10 );
  oFlags.Align( wxALIGN_CENTER );
  poSzrBtns->Add( &m_oBtnDefaults, oFlags );
  poSzrBtns->AddSpacer( 10 );
  oFlags.Align( wxALIGN_LEFT   );
  poSzrBtns->Add( &m_oBtnCancel  , oFlags );

  // Layout the right hand panel
  oFlags.Align( wxALIGN_CENTER );
  oFlags.Border( wxALL                   ,  0 );
  poSzrRHS->Add( poPnlRHS1       , oFlags );
  oFlags.Border( wxTOP                   , 35 );
  poSzrRHS->Add( poPnlBtns       , oFlags );

  // Layout the overall dialog
  oFlags.Border( wxALL                   , 15 );
  poSzrDlg->Add( poPnlLHS        , oFlags );
  oFlags.Border( wxTOP | wxBOTTOM        , 15 );
  poSzrDlg->Add( poPnlMID        , oFlags );
  oFlags.Border( wxALL                   , 15 );
  poSzrDlg->Add( poPnlRHS        , oFlags );

  // Set dialogues minimum size and initial size as calculated by the sizer
  poSzrLHS ->SetSizeHints( poPnlLHS  );
  poSzrMID ->SetSizeHints( poPnlMID  );
  poSzrRHS1->SetSizeHints( poPnlRHS1 );
  poSzrBtns->SetSizeHints( poPnlBtns );
  poSzrRHS ->SetSizeHints( poPnlRHS  );
  poSzrDlg ->SetSizeHints( this      );
}

//**************************************************************************************************
// Get a pointer to a value panel associated with a control ID number.
//
// Argument List :
//   iPnlID - The control ID number
//
// Return Values :
//   Success - A pointer to the value panel
//   Failure - NULL

PnlValue * DlgGnuCapCfgOPT::poGetPanel( int iPnlID )
{
  switch( iPnlID )
  {
    case ID_PNL_ABSTOL   : return( &m_oPnlABSTOL   );
    case ID_PNL_CHGTOL   : return( &m_oPnlCHGTOL   );
    case ID_PNL_DAMPMAX  : return( &m_oPnlDAMPMAX  );
    case ID_PNL_DAMPMIN  : return( &m_oPnlDAMPMIN  );
    case ID_PNL_DAMPST   : return( &m_oPnlDAMPST   );
    case ID_PNL_DEFL     : return( &m_oPnlDEFL     );
    case ID_PNL_DEFW     : return( &m_oPnlDEFW     );
    case ID_PNL_DEFAD    : return( &m_oPnlDEFAD    );
    case ID_PNL_DEFAS    : return( &m_oPnlDEFAS    );
    case ID_PNL_DTMIN    : return( &m_oPnlDTMIN    );
    case ID_PNL_DTRATIO  : return( &m_oPnlDTRATIO  );
    case ID_PNL_FLOOR    : return( &m_oPnlFLOOR    );
    case ID_PNL_GMIN     : return( &m_oPnlGMIN     );
    case ID_PNL_HARMS    : return( &m_oPnlHARMS    );
    case ID_PNL_ITERMIN  : return( &m_oPnlITERMIN  );
    case ID_PNL_ITL1     : return( &m_oPnlITL1     );
    case ID_PNL_ITL3     : return( &m_oPnlITL3     );
    case ID_PNL_ITL4     : return( &m_oPnlITL4     );
    case ID_PNL_ITL7     : return( &m_oPnlITL7     );
    case ID_PNL_ITL8     : return( &m_oPnlITL8     );
    case ID_PNL_RECURS   : return( &m_oPnlRECURS   );
    case ID_PNL_RELTOL   : return( &m_oPnlRELTOL   );
    case ID_PNL_ROUND    : return( &m_oPnlROUND    );
    case ID_PNL_SHORT    : return( &m_oPnlSHORT    );
    case ID_PNL_TEMP     : return( &m_oPnlTEMP     );
    case ID_PNL_TNOM     : return( &m_oPnlTNOM     );
    case ID_PNL_TRANSITS : return( &m_oPnlTRANSITS );
    case ID_PNL_TRREJECT : return( &m_oPnlTRREJECT );
    case ID_PNL_TRSTEPG  : return( &m_oPnlTRSTEPG  );
    case ID_PNL_TRSTEPH  : return( &m_oPnlTRSTEPH  );
    case ID_PNL_TRSTEPS  : return( &m_oPnlTRSTEPS  );
    case ID_PNL_TRTOL    : return( &m_oPnlTRTOL    );
    case ID_PNL_VFLOOR   : return( &m_oPnlVFLOOR   );
    case ID_PNL_VMAX     : return( &m_oPnlVMAX     );
    case ID_PNL_VMIN     : return( &m_oPnlVMIN     );
    case ID_PNL_VNTOL    : return( &m_oPnlVNTOL    );

    default              : return( NULL );
  }
}

//**************************************************************************************************
// Set the values in the value panel controls.
//
// Argument List :
//   rosCmdOPT - A reference to an OPTION command object

void  DlgGnuCapCfgOPT::SetValues( CmdGnuCapOPT & roCmdOPT )
{
  wxString  os1;
  uint      ui1;

  SetEvtHandlerEnabled( false );

  m_oPnlABSTOL  .bSetValue( roCmdOPT.m_osABSTOL   );
  m_oPnlCHGTOL  .bSetValue( roCmdOPT.m_osCHGTOL   );
  m_oPnlDAMPMAX .bSetValue( roCmdOPT.m_osDAMPMAX  );
  m_oPnlDAMPMIN .bSetValue( roCmdOPT.m_osDAMPMIN  );
  m_oPnlDAMPST  .bSetValue( roCmdOPT.m_osDAMPST   );
  m_oPnlDEFL    .bSetValue( roCmdOPT.m_osDEFL     );
  m_oPnlDEFW    .bSetValue( roCmdOPT.m_osDEFW     );
  m_oPnlDEFAD   .bSetValue( roCmdOPT.m_osDEFAD    );
  m_oPnlDEFAS   .bSetValue( roCmdOPT.m_osDEFAS    );
  m_oPnlDTMIN   .bSetValue( roCmdOPT.m_osDTMIN    );
  m_oPnlDTRATIO .bSetValue( roCmdOPT.m_osDTRATIO  );
  m_oPnlFLOOR   .bSetValue( roCmdOPT.m_osFLOOR    );
  m_oPnlGMIN    .bSetValue( roCmdOPT.m_osGMIN     );
  m_oPnlHARMS   .bSetValue( roCmdOPT.m_osHARMS    );
  m_oPnlITERMIN .bSetValue( roCmdOPT.m_osITERMIN  );
  m_oPnlITL1    .bSetValue( roCmdOPT.m_osITL1     );
  m_oPnlITL3    .bSetValue( roCmdOPT.m_osITL3     );
  m_oPnlITL4    .bSetValue( roCmdOPT.m_osITL4     );
  m_oPnlITL7    .bSetValue( roCmdOPT.m_osITL7     );
  m_oPnlITL8    .bSetValue( roCmdOPT.m_osITL8     );
  m_oPnlRECURS  .bSetValue( roCmdOPT.m_osRECURS   );
  m_oPnlRELTOL  .bSetValue( roCmdOPT.m_osRELTOL   );
  m_oPnlROUND   .bSetValue( roCmdOPT.m_osROUND    );
  m_oPnlSHORT   .bSetValue( roCmdOPT.m_osSHORT    );
  m_oPnlTEMP    .bSetValue( roCmdOPT.m_osTEMP     );
  m_oPnlTNOM    .bSetValue( roCmdOPT.m_osTNOM     );
  m_oPnlTRANSITS.bSetValue( roCmdOPT.m_osTRANSITS );
  m_oPnlTRREJECT.bSetValue( roCmdOPT.m_osTRREJECT );
  m_oPnlTRSTEPG .bSetValue( roCmdOPT.m_osTRSTEPG  );
  m_oPnlTRSTEPH .bSetValue( roCmdOPT.m_osTRSTEPH  );
  m_oPnlTRSTEPS .bSetValue( roCmdOPT.m_osTRSTEPS  );
  m_oPnlTRTOL   .bSetValue( roCmdOPT.m_osTRTOL    );
  m_oPnlVFLOOR  .bSetValue( roCmdOPT.m_osVFLOOR   );
  m_oPnlVMAX    .bSetValue( roCmdOPT.m_osVMAX     );
  m_oPnlVMIN    .bSetValue( roCmdOPT.m_osVMIN     );
  m_oPnlVNTOL   .bSetValue( roCmdOPT.m_osVNTOL    );

  m_oCbxBYPASS  . SetValue( roCmdOPT.m_bBYPASS   );
  m_oCbxCSTRAY  . SetValue( roCmdOPT.m_bCSTRAY   );
  m_oCbxFBBYPASS. SetValue( roCmdOPT.m_bFBBYPASS );
  m_oCbxINCMODE . SetValue( roCmdOPT.m_bINCMODE  );
  m_oCbxLUBYPASS. SetValue( roCmdOPT.m_bLUBYPASS );
  m_oCbxOPTS    . SetValue( roCmdOPT.m_bOPTS     );
  m_oCbxQUITCONV. SetValue( roCmdOPT.m_bQUITCONV );
  m_oCbxRSTRAY  . SetValue( roCmdOPT.m_bRSTRAY   );
  m_oCbxTRACEL  . SetValue( roCmdOPT.m_bTRACEL   );

  for( ui1=0; ui1<m_oChoMETHOD.m_oChoice.GetCount( ); ui1++ )
  {
    os1 = m_oChoMETHOD.m_oChoice.GetString( ui1 );
    if( os1.IsSameAs( roCmdOPT.m_osMETHOD, false ) )
      { m_oChoMETHOD.m_oChoice.SetSelection( ui1 ); break; }
  }
  for( ui1=0; ui1<m_oChoMODE.m_oChoice.GetCount( )  ; ui1++ )
  {
    os1 = m_oChoMODE.m_oChoice.GetString( ui1 );
    if( os1.IsSameAs( roCmdOPT.m_osMODE, false ) )
      { m_oChoMODE.m_oChoice.SetSelection( ui1 ); break; }
  }
  for( ui1=0; ui1<m_oChoORDER.m_oChoice.GetCount( ) ; ui1++ )
  {
    os1 = m_oChoORDER.m_oChoice.GetString( ui1 );
    if( os1.IsSameAs( roCmdOPT.m_osORDER, false ) )
      { m_oChoORDER.m_oChoice.SetSelection( ui1 ); break; }
  }

  SetEvtHandlerEnabled( true );
}

//**************************************************************************************************
// Get the values from the value panel controls.
//
// Argument List :
//   rosCmdOPT - A reference to an OPTION command object

void  DlgGnuCapCfgOPT::GetValues( CmdGnuCapOPT & roCmdOPT )
{
  SetEvtHandlerEnabled( false );

  roCmdOPT.m_osABSTOL   = m_oPnlABSTOL  .rosGetValue( );
  roCmdOPT.m_osCHGTOL   = m_oPnlCHGTOL  .rosGetValue( );
  roCmdOPT.m_osDAMPMAX  = m_oPnlDAMPMAX .rosGetValue( );
  roCmdOPT.m_osDAMPMIN  = m_oPnlDAMPMIN .rosGetValue( );
  roCmdOPT.m_osDAMPST   = m_oPnlDAMPST  .rosGetValue( );
  roCmdOPT.m_osDEFL     = m_oPnlDEFL    .rosGetValue( );
  roCmdOPT.m_osDEFW     = m_oPnlDEFW    .rosGetValue( );
  roCmdOPT.m_osDEFAD    = m_oPnlDEFAD   .rosGetValue( );
  roCmdOPT.m_osDEFAS    = m_oPnlDEFAS   .rosGetValue( );
  roCmdOPT.m_osDTMIN    = m_oPnlDTMIN   .rosGetValue( );
  roCmdOPT.m_osDTRATIO  = m_oPnlDTRATIO .rosGetValue( );
  roCmdOPT.m_osFLOOR    = m_oPnlFLOOR   .rosGetValue( );
  roCmdOPT.m_osGMIN     = m_oPnlGMIN    .rosGetValue( );
  roCmdOPT.m_osHARMS    = m_oPnlHARMS   .rosGetValue( );
  roCmdOPT.m_osITERMIN  = m_oPnlITERMIN .rosGetValue( );
  roCmdOPT.m_osITL1     = m_oPnlITL1    .rosGetValue( );
  roCmdOPT.m_osITL3     = m_oPnlITL3    .rosGetValue( );
  roCmdOPT.m_osITL4     = m_oPnlITL4    .rosGetValue( );
  roCmdOPT.m_osITL7     = m_oPnlITL7    .rosGetValue( );
  roCmdOPT.m_osITL8     = m_oPnlITL8    .rosGetValue( );
  roCmdOPT.m_osRECURS   = m_oPnlRECURS  .rosGetValue( );
  roCmdOPT.m_osRELTOL   = m_oPnlRELTOL  .rosGetValue( );
  roCmdOPT.m_osROUND    = m_oPnlROUND   .rosGetValue( );
  roCmdOPT.m_osSHORT    = m_oPnlSHORT   .rosGetValue( );
  roCmdOPT.m_osTEMP     = m_oPnlTEMP    .rosGetValue( );
  roCmdOPT.m_osTNOM     = m_oPnlTNOM    .rosGetValue( );
  roCmdOPT.m_osTRANSITS = m_oPnlTRANSITS.rosGetValue( );
  roCmdOPT.m_osTRREJECT = m_oPnlTRREJECT.rosGetValue( );
  roCmdOPT.m_osTRSTEPG  = m_oPnlTRSTEPG .rosGetValue( );
  roCmdOPT.m_osTRSTEPH  = m_oPnlTRSTEPH .rosGetValue( );
  roCmdOPT.m_osTRSTEPS  = m_oPnlTRSTEPS .rosGetValue( );
  roCmdOPT.m_osTRTOL    = m_oPnlTRTOL   .rosGetValue( );
  roCmdOPT.m_osVFLOOR   = m_oPnlVFLOOR  .rosGetValue( );
  roCmdOPT.m_osVMAX     = m_oPnlVMAX    .rosGetValue( );
  roCmdOPT.m_osVMIN     = m_oPnlVMIN    .rosGetValue( );
  roCmdOPT.m_osVNTOL    = m_oPnlVNTOL   .rosGetValue( );

  roCmdOPT.m_bBYPASS    = m_oCbxBYPASS  .GetValue( );
  roCmdOPT.m_bCSTRAY    = m_oCbxCSTRAY  .GetValue( );
  roCmdOPT.m_bFBBYPASS  = m_oCbxFBBYPASS.GetValue( );
  roCmdOPT.m_bINCMODE   = m_oCbxINCMODE .GetValue( );
  roCmdOPT.m_bLUBYPASS  = m_oCbxLUBYPASS.GetValue( );
  roCmdOPT.m_bOPTS      = m_oCbxOPTS    .GetValue( );
  roCmdOPT.m_bQUITCONV  = m_oCbxQUITCONV.GetValue( );
  roCmdOPT.m_bRSTRAY    = m_oCbxRSTRAY  .GetValue( );
  roCmdOPT.m_bTRACEL    = m_oCbxTRACEL  .GetValue( );

  roCmdOPT.m_osMETHOD   = m_oChoMETHOD.m_oChoice.GetStringSelection( ).Upper( );
  roCmdOPT.m_osMODE     = m_oChoMODE  .m_oChoice.GetStringSelection( ).Upper( );
  roCmdOPT.m_osORDER    = m_oChoORDER .m_oChoice.GetStringSelection( ).Upper( );

  if( g_oConfig.eGetPhaseUnits( ) == eUNITS_PHAD ) roCmdOPT.m_osPHASE = wxT("DEGREES");
  else                                             roCmdOPT.m_osPHASE = wxT("RADIANS");

  roCmdOPT.bFormat( );

  SetEvtHandlerEnabled( true );
}

//**************************************************************************************************
// Set the values in the value panel controls.
//
// Argument List :
//   roCmdOPT - A reference to an OPTION command object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  DlgGnuCapCfgOPT::bSetValues( CmdGnuCapOPT & roCmdOPT )
{
  SetValues( roCmdOPT );
  m_oCmdOPT = roCmdOPT;

  return( true );
}

//**************************************************************************************************
// Get the values from the value panel controls.
//
// Argument List :
//   roCmdOPT - A reference to an OPTION command object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  DlgGnuCapCfgOPT::bGetValues( CmdGnuCapOPT & roCmdOPT )
{
  GetValues( roCmdOPT );

  return( true );
}

//**************************************************************************************************
// Reset all dialog settings to defaults.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  DlgGnuCapCfgOPT::bClear( void )
{
  m_oCmdOPT.bSetDefaults( );

  SetValues( m_oCmdOPT );

  return( true );
}

//**************************************************************************************************
// Set a value panel control's value.
//
// Argument List :
//   iPnlID   - Value panel control identifier
//   rosValue - The value to set
//
// Return Values :
//   true  - Success
//   false - Failure

bool  DlgGnuCapCfgOPT::bSetValue( int iPnlID, const wxString & rosValue )
{
  PnlValue * poPnlValue;

  // Get a pointer to the appropriate value panel
  poPnlValue = poGetPanel( iPnlID );
  if( poPnlValue == NULL )                  return( false );

  // Set the panel value
  if( ! poPnlValue->bSetValue( rosValue ) ) return( false );

  // Create the OPTIONS command string
  m_oCmdOPT.bFormat( );

  return( true );
}

//**************************************************************************************************
// Get a value panel control's value.
//
// Argument List :
//   iPnlID - Value panel control identifier
//
// Return Values :
//   Success - The panel value
//   Failure - 0.0

const wxString & DlgGnuCapCfgOPT::rosGetValue( int iPnlID )
{
  static  wxString   osEmpty;
          PnlValue * poPnlValue;

  poPnlValue = poGetPanel( iPnlID );
  if( poPnlValue == NULL ) return( osEmpty );

  return( poPnlValue->rosGetValue( ) );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Ok button event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event (not used)

void  DlgGnuCapCfgOPT::OnBtnOk( wxCommandEvent & roEvtCmd )
{
  GetValues( m_oCmdOPT );
  EndModal( wxID_OK );
}

//**************************************************************************************************
// Defaults button event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event (not used)

void  DlgGnuCapCfgOPT::OnBtnDefaults( wxCommandEvent & roEvtCmd )
{
  CmdGnuCapOPT  oCmdOPT;

  oCmdOPT.bSetDefaults( );
  SetValues( oCmdOPT );
}

//**************************************************************************************************
// Cancel button event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event (not used)

void  DlgGnuCapCfgOPT::OnBtnCancel( wxCommandEvent & roEvtCmd )
{
  SetValues( m_oCmdOPT );
  EndModal( wxID_CANCEL );
}

//**************************************************************************************************
