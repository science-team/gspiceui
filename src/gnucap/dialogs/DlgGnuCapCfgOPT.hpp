//**************************************************************************************************
//                                      DlgGnuCapCfgOPT.hpp                                        *
//                                     ---------------------                                       *
// Description : This dialogue is used to enter values associated with the GNU-Cap OPTIONS         *
//               command.                                                                          *
// Started     : 2006-09-14                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef DLGGNUCAPCFGOPT_HPP
#define DLGGNUCAPCFGOPT_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "utility/PnlValue.hpp"
#include "utility/PnlLblCho.hpp"
#include "gnucap/commands/CmdGnuCapOPT.hpp"
#include "base/NbkSimEngBase.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class DlgGnuCapCfgOPT : public wxDialog
{
  private :

    // Value panel controls (for float values)
    PnlValue      m_oPnlABSTOL;
    PnlValue      m_oPnlCHGTOL;
    PnlValue      m_oPnlDAMPMAX;
    PnlValue      m_oPnlDAMPMIN;
    PnlValue      m_oPnlDEFL;
    PnlValue      m_oPnlDEFW;
    PnlValue      m_oPnlDEFAD;
    PnlValue      m_oPnlDEFAS;
    PnlValue      m_oPnlDTMIN;
    PnlValue      m_oPnlDTRATIO;
    PnlValue      m_oPnlFLOOR;
    PnlValue      m_oPnlGMIN;
    PnlValue      m_oPnlRELTOL;
    PnlValue      m_oPnlROUND;
    PnlValue      m_oPnlSHORT;
    PnlValue      m_oPnlTEMP;
    PnlValue      m_oPnlTNOM;
    PnlValue      m_oPnlTRREJECT;
    PnlValue      m_oPnlTRSTEPG;
    PnlValue      m_oPnlTRSTEPH;
    PnlValue      m_oPnlTRSTEPS;
    PnlValue      m_oPnlTRTOL;
    PnlValue      m_oPnlVFLOOR;
    PnlValue      m_oPnlVMAX;
    PnlValue      m_oPnlVMIN;
    PnlValue      m_oPnlVNTOL;

    // Value panel controls (for integer values)
    PnlValue      m_oPnlDAMPST;
    PnlValue      m_oPnlHARMS;
    PnlValue      m_oPnlITL1;
    PnlValue      m_oPnlITL3;
    PnlValue      m_oPnlITL4;
    PnlValue      m_oPnlITL7;
    PnlValue      m_oPnlITL8;
    PnlValue      m_oPnlITERMIN;
    PnlValue      m_oPnlRECURS;
    PnlValue      m_oPnlTRANSITS;

    // Value panel controls (for string values)
    PnlLblCho     m_oChoMETHOD;
    PnlLblCho     m_oChoMODE;
    PnlLblCho     m_oChoORDER;

    // Value panel controls (for boolean values)
    wxCheckBox    m_oCbxBYPASS;
    wxCheckBox    m_oCbxCSTRAY;
    wxCheckBox    m_oCbxFBBYPASS;
    wxCheckBox    m_oCbxINCMODE;
    wxCheckBox    m_oCbxLUBYPASS;
    wxCheckBox    m_oCbxOPTS;
    wxCheckBox    m_oCbxQUITCONV;
    wxCheckBox    m_oCbxRSTRAY;
    wxCheckBox    m_oCbxTRACEL;

    // Button controls
    wxButton      m_oBtnOk;
    wxButton      m_oBtnCancel;
    wxButton      m_oBtnDefaults;

    // OPTIONS command object
    CmdGnuCapOPT  m_oCmdOPT;

    // Object initialization functions
    void  Initialize( void );
    void  Create    ( void );
    void  ToolTips  ( void );
    void  DoLayout  ( void );

    PnlValue * poGetPanel( int iPnlID );

    void  SetValues( CmdGnuCapOPT & roCmdOPT );
    void  GetValues( CmdGnuCapOPT & roCmdOPT );

  public :

    explicit  DlgGnuCapCfgOPT( wxWindow * poWin );
             ~DlgGnuCapCfgOPT( );

    bool  bClear( void );

    bool  bSetValues( CmdGnuCapOPT & roCmdOPT );
    bool  bGetValues( CmdGnuCapOPT & roCmdOPT );

          bool         bSetValue( int iPnlID, const wxString & rosValue );
    const wxString & rosGetValue( int iPnlID );

    // Event handlers
    void  OnBtnOk      ( wxCommandEvent & roEvtCmd );
    void  OnBtnDefaults( wxCommandEvent & roEvtCmd );
    void  OnBtnCancel  ( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum eDlgItemID
    {
      ID_PNL_ABSTOL = 0,
      ID_PNL_CHGTOL  ,
      ID_PNL_DAMPMAX ,
      ID_PNL_DAMPMIN ,
      ID_PNL_DAMPST  ,
      ID_PNL_DEFL    ,
      ID_PNL_DEFW    ,
      ID_PNL_DEFAD   ,
      ID_PNL_DEFAS   ,
      ID_PNL_DTMIN   ,
      ID_PNL_DTRATIO ,
      ID_PNL_FLOOR   ,
      ID_PNL_GMIN    ,
      ID_PNL_HARMS   ,
      ID_PNL_ITERMIN ,
      ID_PNL_ITL1    ,
      ID_PNL_ITL3    ,
      ID_PNL_ITL4    ,
      ID_PNL_ITL7    ,
      ID_PNL_ITL8    ,
      ID_PNL_RECURS  ,
      ID_PNL_RELTOL  ,
      ID_PNL_ROUND   ,
      ID_PNL_SHORT   ,
      ID_PNL_TEMP    ,
      ID_PNL_TNOM    ,
      ID_PNL_TRANSITS,
      ID_PNL_TRREJECT,
      ID_PNL_TRSTEPG ,
      ID_PNL_TRSTEPH ,
      ID_PNL_TRSTEPS ,
      ID_PNL_TRTOL   ,
      ID_PNL_VFLOOR  ,
      ID_PNL_VMAX    ,
      ID_PNL_VMIN    ,
      ID_PNL_VNTOL   ,

      ID_CHO_METHOD  ,
      ID_CHO_MODE    ,
      ID_CHO_ORDER   ,

      ID_CBX_BYPASS  ,
      ID_CBX_CSTRAY  ,
      ID_CBX_FBBYPASS,
      ID_CBX_INCMODE ,
      ID_CBX_LUBYPASS,
      ID_CBX_OPTS    ,
      ID_CBX_QUITCONV,
      ID_CBX_RSTRAY  ,
      ID_CBX_TRACEL  ,

      ID_BTN_OK      ,
      ID_BTN_DEFAULTS,
      ID_BTN_CANCEL  ,

      ID_UNUSED      ,  // Assigned to controls for which events are not used

      ID_FST = ID_PNL_ABSTOL,
      ID_LST = ID_BTN_CANCEL
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // DLGGNUCAPCFGOPT_HPP
