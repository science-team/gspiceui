//**************************************************************************************************
//                                        DlgGnuCapCfgGEN.hpp                                      *
//                                       ------------------                                        *
// Description : This dialogue is used to enter values associated with a GNU-Cap generator command *
//                line. It is used for Transient and Fourier analysis.                             *
// Started     : 2004-02-27                                                                        *
// Last Update : 2018-11-03                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef DLGGCPCFGGEN_HPP
#define DLGGCPCFGGEN_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/PnlValue.hpp"
#include "gnucap/commands/CmdGnuCapGEN.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class DlgGnuCapCfgGEN : public wxDialog
{
  private :

    // Value panel controls for overall source characteristics
    PnlValue  m_oPnlAmplitude;
    PnlValue  m_oPnlOffset;

    // Value panel controls for a sinusoidal source
    PnlValue  m_oPnlSinFreq;
    PnlValue  m_oPnlSinPhase;

    // Value panel controls for a pulse source
    PnlValue  m_oPnlPulInitV;
    PnlValue  m_oPnlPulMin;
    PnlValue  m_oPnlPulMax;
    PnlValue  m_oPnlPulDelay;
    PnlValue  m_oPnlPulRise;
    PnlValue  m_oPnlPulWidth;
    PnlValue  m_oPnlPulFall;
    PnlValue  m_oPnlPulPeriod;

    // Button controls
    wxButton  m_oBtnOk;
    wxButton  m_oBtnClear;
    wxButton  m_oBtnCancel;

    // Generator command object
    CmdGnuCapGEN  m_oCmdGEN;

    // Object initialization functions
    void  Initialize( void );
    void  Create    ( void );
    void  ToolTips  ( void );
    void  DoLayout  ( void );

    PnlValue * poGetPanel( int iPnlID );

    void  SetValues( CmdGnuCapGEN & roCmdGEN );
    void  GetValues( CmdGnuCapGEN & roCmdGEN );

  public :

    explicit  DlgGnuCapCfgGEN( wxWindow * poWin );
             ~DlgGnuCapCfgGEN( );

    bool  bClear( void );

    bool  bSetValues( CmdGnuCapGEN & roCmdGEN );
    bool  bGetValues( CmdGnuCapGEN & roCmdGEN );

          bool         bSetValue( int iPnlID, const wxString & rosValue );
    const wxString & rosGetValue( int iPnlID );

    // Event handlers
    void  OnBtnOk    ( wxCommandEvent & roEvtCmd );
    void  OnBtnClear ( wxCommandEvent & roEvtCmd );
    void  OnBtnCancel( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum eDlgItemID
    {
      ID_PNL_AMPLITUDE = 0,
      ID_PNL_OFFSET,
      ID_PNL_SIN_FREQ,
      ID_PNL_SIN_PHASE,
      ID_PNL_PUL_INITV,
      ID_PNL_PUL_MIN,
      ID_PNL_PUL_MAX,
      ID_PNL_PUL_DELAY,
      ID_PNL_PUL_RISE,
      ID_PNL_PUL_WIDTH,
      ID_PNL_PUL_FALL,
      ID_PNL_PUL_PERIOD,

      ID_BTN_OK,
      ID_BTN_CLEAR,
      ID_BTN_CANCEL,

      ID_UNUSED,      // Assigned to controls for which events are not used

      ID_PNL_FST = ID_PNL_AMPLITUDE,
      ID_PNL_LST = ID_PNL_PUL_PERIOD,

      ID_FST = ID_PNL_AMPLITUDE,
      ID_LST = ID_BTN_CANCEL
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // DLGGCPCFGGEN_HPP
