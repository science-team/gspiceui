//**************************************************************************************************
//                                         PnlGnuCapTR.hpp                                         *
//                                        -----------------                                        *
// Description : This class derives from the PnlAnaBase base class and provides a GUI for the user *
//               to configure a Transient Analysis for the GNU-Cap electronic circuit simulation   *
//               engine.                                                                           *
// Started     : 2004-02-22                                                                        *
// Last Update : 2018-10-22                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLGNUCAPTR_HPP
#define PNLGNUCAPTR_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/PnlAnaBase.hpp"
#include "netlist/SimnGnuCap.hpp"
#include "gnucap/dialogs/DlgGnuCapCfgGEN.hpp"

//**************************************************************************************************

class PnlGnuCapTR : public PnlAnaBase
{
  private :

    // Generator configuration
    DlgGnuCapCfgGEN   m_oDlgCfgGEN;

    void  Create      ( void );
    void  CreateSigSrc( void ) override;

  public :

    explicit  PnlGnuCapTR( wxWindow * poWin );
             ~PnlGnuCapTR( );

    bool  bClear( void ) override;

    bool  bLoad( SimnGnuCap & roSimn );
    bool  bSave( SimnGnuCap & roSimn );

    // Event handlers
    void  OnBtnSetup( wxCommandEvent & roEvtCmd );

    // Declare friend classes
    friend  class  NbkGnuCap;

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_BTN_SETUP = PnlAnaBase::ID_LST+1,

      ID_UNUSED,      // Assigned to controls for which events are not used

      ID_FST = ID_BTN_SETUP,
      ID_LST = ID_BTN_SETUP
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLGNUCAPTR_HPP
