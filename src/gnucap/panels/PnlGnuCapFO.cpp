//**************************************************************************************************
//                                         PnlGnuCapFO.cpp                                         *
//                                        -----------------                                        *
// Started     : 2004-04-26                                                                        *
// Last Update : 2018-10-05                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "PnlGnuCapFO.hpp"

//**************************************************************************************************
// Implement an event table.

wxBEGIN_EVENT_TABLE( PnlGnuCapFO, PnlAnaBase )

  EVT_BUTTON( PnlGnuCapFO::ID_BTN_SETUP, PnlGnuCapFO::OnBtnSetup )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.

PnlGnuCapFO::PnlGnuCapFO( wxWindow * poWin ) : PnlAnaBase( poWin ), m_oDlgCfgGEN( poWin )
{
  bSetAnalysType( eCMD_FO );  // Set the analysis panel type
  Create( );                  // Create the analysis panel
  bClear( );                  // Clear all object attributes
}

//**************************************************************************************************
// Destructor.

PnlGnuCapFO::~PnlGnuCapFO( )
{
}

//**************************************************************************************************
// Create the display objects.

void  PnlGnuCapFO::Create( void )
{
  PnlAnaBase::CreateBase( );     // Create the base controls
  PnlAnaBase::CreateInitC( );    // Create the initial condtns controls
  PnlAnaBase::CreateTemp( );     // Create the analysis temp. controls
  PnlAnaBase::CreateSigSrc( );   // Create the input signal source controls
              CreateSigSrc( );   // Create the additional input signal source controls
              CreateOutputs( );  // Create the output parameters

#ifdef LAYOUT_MNGR
  PnlAnaBase::DoLayout( );     // Layout the panel's GUI objects
#endif // LAYOUT_MNGR

  // Set the frequency sweep parameter labels
#ifdef LAYOUT_MNGR
  bSetSwpParsLbl( wxT("Frequency Sweep") );
#else
  m_oSbxSwpPars.SetLabel( wxT(" Frequency Sweep ") );
#endif // LAYOUT_MNGR
  m_oPnlStart  .bSetName( wxT("Start Frequency") );
  m_oPnlStop   .bSetName( wxT("Stop Frequency") );
  m_oPnlStep   .bSetName( wxT("Step Increment") );

  // Set sweep parameter units
  m_oPnlStart.bSetUnitsType( eUNITS_FREQ );
  m_oPnlStop .bSetUnitsType( eUNITS_FREQ );
  m_oPnlStep .bSetUnitsType( eUNITS_FREQ );
}

//**************************************************************************************************
// Create the additional source component display objects.

void  PnlGnuCapFO::CreateSigSrc( void )
{
  wxPanel * poPnl;

  poPnl = static_cast< wxPanel * >( m_oPnlStart.GetParent( ) );

#ifdef LAYOUT_MNGR
  m_oBtnSetup.Create( poPnl, ID_BTN_SETUP, wxT("Setup ...") );
#else
  m_oSbxSigSrc.SetSize( wxSize( 255, 60 ) );
  m_oBtnSetup.Create( poPnl, ID_BTN_SETUP, wxT("Setup ..."),
                      wxPoint( 143, 200 ), wxSize( 105, GUI_CTRL_HT ) );
#endif // LAYOUT_MNGR
}

//**************************************************************************************************
// Create the parameters to included in the results file output.

void  PnlGnuCapFO::CreateOutputs( void )
{
  // Create and add output parameters
  m_oSbxOutputs.Create( this, ID_UNUSED,     wxT(" Outputs "),
                        wxPoint( 455,  5 ), wxSize( 110, 122 ) );
  m_oCbxMag    .Create( this, ID_CBX_MAG,    wxT("Magnitude"),
                        wxPoint( 461, 24 ) );
  m_oCbxPhase  .Create( this, ID_CBX_PHASE,  wxT("Phase"),
                        wxPoint( 461, 49 ) );
  m_oCbxMagDb  .Create( this, ID_CBX_MAGDB,  wxT("Mag. in dB"),
                        wxPoint( 461, 74 ) );
  m_oCbxMagRel .Create( this, ID_CBX_MAGREL, wxT("Mag. is Rel."),
                        wxPoint( 461, 99 ) );

  m_oSbxOutputs.SetOwnFont( FONT_SLANT );
}

//**************************************************************************************************
// ???

void  PnlGnuCapFO::GetConfig( void )
{
  wxConfig * poConfig;
  long       li1;

  // Set the output parameters to be extracted
  poConfig = (wxConfig *) wxConfig::Get( );
  if( poConfig == NULL ) return;

  poConfig->SetPath( wxT("/GNU-Cap") );

  poConfig->Read( wxT("FO_Mag") , &li1, (long) 1 );
  m_oCbxMag   .SetValue( li1 != 0 ? true : false );

  poConfig->Read( wxT("FO_Phase") , &li1, (long) 0 );
  m_oCbxPhase .SetValue( li1 != 0 ? true : false );

  poConfig->Read( wxT("FO_MagRel"), &li1, (long) 1 );
  m_oCbxMagRel.SetValue( li1 != 0 ? true : false );

  poConfig->Read( wxT("FO_MagDB") , &li1, (long) 0 );
  m_oCbxMagDb .SetValue( li1 != 0 ? true : false );
}

//**************************************************************************************************
// ???

void  PnlGnuCapFO::SetConfig( void )
{
  wxConfig * poConfig;

  poConfig = (wxConfig *) wxConfig::Get( );
  if( poConfig == NULL ) return;

  poConfig->SetPath( wxT("/GNU-Cap") );

  poConfig->Write( wxT("FO_Mag")   , m_oCbxMag   .GetValue( ) );
  poConfig->Write( wxT("FO_Phase") , m_oCbxPhase .GetValue( ) );
  poConfig->Write( wxT("FO_MagRel"), m_oCbxMagRel.GetValue( ) );
  poConfig->Write( wxT("FO_MagDB") , m_oCbxMagDb .GetValue( ) );
}

//**************************************************************************************************
// Clear the object attributes.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlGnuCapFO::bClear( void )
{
  // Clear the base class
  PnlAnaBase::bClear( );

  // Set default sweep values
  m_oPnlStart.bSetValue( (float)   1.0 );
  m_oPnlStop .bSetValue( (float) 100.0 );
  m_oPnlStep .bSetValue( (float)  10.0 );
  m_oPnlStart.bSetUnits( wxT("kHz") );
  m_oPnlStop .bSetUnits( wxT("kHz") );
  m_oPnlStep .bSetUnits( wxT("kHz") );

  // Set default initial condition values
  bSetInitC( eINITC_WARM );

  // Set input source default values
  m_oChoSrcName.Clear( );
  m_oChoSrcName.Append( wxT("None") );
  m_oChoSrcName.SetSelection( 0 );

  // Clear the signal generator configuration dialog
  m_oDlgCfgGEN.bClear( );

  // Set parameters check box default values
  m_oCbxVoltage.SetValue( true );
  m_oCbxCurrent.SetValue( false );
  m_oCbxPower  .SetValue( false );
  m_oCbxResist .SetValue( false );

  // Set the output parameter check box default values
  m_oCbxMag   .SetValue( true  );
  m_oCbxPhase .SetValue( false );
  m_oCbxMagDb .SetValue( true  );
  m_oCbxMagRel.SetValue( false );

  // Set default temperature value
  m_oPnlTemp.bSetValue( 27.0 );

  return( true );
}

//**************************************************************************************************
// Load information from a simulation object.
//
// Argument List :
//   roSimn - A SimnGnuCap object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlGnuCapFO::bLoad( SimnGnuCap & roSimn )
{
  bool  bRtn=true;

  // Load the components into the signal source choice box
  PnlAnaBase::LoadSrcNames( roSimn.m_oaCpnts, wxT("VI") );

  // Go no further if the FO command isn't valid
  if( ! roSimn.m_oCmdFO.bIsValid( ) )                        return( false );

  // Set the sweep source (a sweep source is not mandatory for a FO analysis)
  PnlAnaBase::bSetSrcCpnt( roSimn.m_oCpntSigSrc );

  // Set the sweep values
  if( ! m_oPnlStart.bSetValue( roSimn.m_oCmdFO.m_osStart ) ) bRtn = false;
  if( ! m_oPnlStop .bSetValue( roSimn.m_oCmdFO.m_osStop  ) ) bRtn = false;
  if( ! m_oPnlStep .bSetValue( roSimn.m_oCmdFO.m_osStep  ) ) bRtn = false;

  // Set the initial conditions
  if( roSimn.m_oCmdFO.m_eInitC != eINITC_NONE )
    m_oRbxSweep.SetSelection( roSimn.m_oCmdFO.m_eInitC );

  // Load the generator setup dialog
  if( ! m_oDlgCfgGEN.bSetValues( roSimn.m_oCmdGEN ) )        bRtn = false;

  // Set the parameters to derive
  m_oCbxVoltage.SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_VLT ] );
  m_oCbxCurrent.SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_CUR ] );
  m_oCbxPower  .SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_PWR ] );
  m_oCbxResist .SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_RES ] );

  // Set the output parameters to be extracted
  GetConfig( );

  // Set the analysis temperature
  if( ! m_oPnlTemp.bSetValue( roSimn.m_oCmdFO.m_osTempC ) )  bRtn = false;

  return( bRtn );
}

//**************************************************************************************************
// Save information to a simulation object.
//
// Argument List :
//   roSimn - A SimnGnuCap object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlGnuCapFO::bSave( SimnGnuCap & roSimn )
{
  wxString  os1;

  m_osErrMsg.Empty( );

  // Set the analysis type
  roSimn.m_oCmdPR.bSetAnaType( eCMD_FO );

  // Set the sweep values
  roSimn.m_oCmdFO.m_osStart = m_oPnlStart.rosGetValue( );
  roSimn.m_oCmdFO.m_osStop  = m_oPnlStop .rosGetValue( );
  roSimn.m_oCmdFO.m_osStep  = m_oPnlStep .rosGetValue( );

  // Set the initial conditions
  roSimn.m_oCmdFO.m_eInitC = (eTypeInitC) m_oRbxSweep.GetSelection( );

  // Set the component to be used as a signal source
  if( m_oChoSrcName.GetStringSelection( ) != wxT("None") )
  {
    // Get the unmodified signal source component, set it as the signal source
    os1 = m_oChoSrcName.GetStringSelection( );
    (Component &) roSimn.m_oCpntSigSrc = roSimn.roGetCpnt( os1 );
    roSimn.m_oCpntSigSrc.bSetValue( wxT("GENERATOR(1)") );
    // Get the GENERATOR command from the Generator Setup dialog
    m_oDlgCfgGEN.bGetValues( roSimn.m_oCmdGEN );
    if( ! roSimn.m_oCmdGEN.bIsValid( ) )
    {
      os1 = wxT("The signal source ")
          + roSimn.m_oCmdGEN.rosGetErrMsg( ) + wxT(".   ");
      SetErrMsg( os1 );
    }
  }
  else
  {
    roSimn.m_oCpntSigSrc.bClear( );
    roSimn.m_oCmdGEN    .bSetDefaults( );
  }

  // Store the parameters to derive
  roSimn.m_oCmdPR.m_bParams[ ePARAM_VLT ] = m_oCbxVoltage.GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_CUR ] = m_oCbxCurrent.GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_PWR ] = m_oCbxPower  .GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_RES ] = m_oCbxResist .GetValue( );

  // Store the output parameters to be extracted
  if( m_oCbxMag.IsChecked( ) || m_oCbxPhase.IsChecked( ) ) SetConfig( );
  else SetErrMsg( wxT("An Outputs value hasn't been specified.") );

  // Set the analysis temperature
  roSimn.m_oCmdFO.m_osTempC = m_oPnlTemp.rosGetValue( );

  // Create the command strings
  roSimn.m_oCmdFO.bFormat( );
  roSimn.m_oCmdPR.bFormat( );

  // Check for errors
  if( ! roSimn.m_oCmdFO.bIsValid( ) )
    SetErrMsg( roSimn.m_oCmdFO.rosGetErrMsg( ) );
  if( ! roSimn.m_oCmdPR.bIsValid( ) )
    SetErrMsg( roSimn.m_oCmdPR.rosGetErrMsg( ) );

  return( bIsOk( ) );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Generator component setup button control event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event

void  PnlGnuCapFO::OnBtnSetup( wxCommandEvent & roEvtCmd )
{
  m_oDlgCfgGEN.CenterOnParent( );
  m_oDlgCfgGEN.ShowModal( );
}

//**************************************************************************************************
