//**************************************************************************************************
//                                         PnlGnuCapDC.cpp                                         *
//                                        -----------------                                        *
// Started     : 2003-08-18                                                                        *
// Last Update : 2018-10-20                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "PnlGnuCapDC.hpp"

//**************************************************************************************************
// Implement an event table.

wxBEGIN_EVENT_TABLE( PnlGnuCapDC, PnlAnaBase )

  EVT_CHOICE  ( PnlAnaBase::ID_CHO_SRCNAME, PnlGnuCapDC::OnSrcName )
  EVT_RADIOBOX( PnlAnaBase::ID_RBX_SCALE  , PnlGnuCapDC::OnScale   )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.

PnlGnuCapDC::PnlGnuCapDC( wxWindow * poWin ) : PnlAnaBase( poWin )
{
  bSetAnalysType( eCMD_DC );  // Set the analysis panel type
  Create( );                  // Create the analysis panel
  InitSwpUnits( );            // Initialize the sweep units
  bClear( );                  // Clear all object attributes
}

//**************************************************************************************************
// Destructor.

PnlGnuCapDC::~PnlGnuCapDC( )
{
}

//**************************************************************************************************
// Create the display objects.

void  PnlGnuCapDC::Create( void )
{
  PnlAnaBase::CreateBase( );    // Create the base controls
  PnlAnaBase::CreateScale( );   // Create the scale controls
  PnlAnaBase::CreateTemp( );    // Create the analysis temperature controls
  PnlAnaBase::CreateSigSrc( );  // Create the input signal source controls

#ifdef LAYOUT_MNGR
  PnlAnaBase::DoLayout( );      // Layout the panel's GUI objects
#endif // LAYOUT_MNGR

  // The following block of code is required to circumvent a bug in wxWidgets. All declared wxChoice
  // controls attempt a clear( ) operation in their destructor whether or not thay have actually
  // been created. Causes a null pointer warning. ??? 2018-10-20
  m_oPnlSrcLvl.bCreate( m_oChoSrcName.GetParent( ), ID_PNL_SRCLVL, -1, 95, 82 );
  m_oPnlSrcLvl.Hide( );

  // Set the sweep parameters labels
#ifdef LAYOUT_MNGR
  bSetSwpParsLbl( wxT("DC Sweep") );
#else
  m_oSbxSwpPars.SetLabel( wxT(" DC Sweep ")  );
#endif // LAYOUT_MNGR
  m_oPnlStart  .bSetName( wxT("Start Value") );
  m_oPnlStop   .bSetName( wxT("Stop Value")  );

  // Create scale controls but disable the scale option GNU-Cap doesn't support
  m_oRbxSweep.Enable( eSCALE_OCT, false );
}

//**************************************************************************************************
// Initialize the sweep parameter units.

void  PnlGnuCapDC::InitSwpUnits( void )
{
  wxString    os1;
  eTypeUnits  eUnits;

  // Determine the sweep parameter units
  os1 = m_oChoSrcName.GetStringSelection( );
  eUnits = Component::eGetUnitsType( os1 );

  switch( eUnits )
  {
    case eUNITS_VOLT :  // Units of voltage
      os1 = wxT("Voltage");
      break;

    case eUNITS_CURR :  // Units of current
      os1 = wxT("Current");
      break;

    case eUNITS_CAP  :  // Units of capacitance
      os1 = wxT("Capacitance");
      break;

    case eUNITS_IND  :  // Units of inductance
      os1 = wxT("Inductance");
      break;

    case eUNITS_RES  :  // Units of resistance
      os1 = wxT("Resistance");
      break;

    default          :  // No units
      os1 = wxT("Value");
  }

  // Set the sweep parameter units
  m_oPnlStart.bSetName( wxString( wxT("Start ") ) + os1 );
  m_oPnlStop .bSetName( wxString( wxT("Stop ")  ) + os1 );
  m_oPnlStart.bSetUnitsType( eUnits );
  m_oPnlStop .bSetUnitsType( eUnits );
  m_oPnlStep .bSetUnitsType( eUnits );
}

//**************************************************************************************************
// Initialize the step scale.

void  PnlGnuCapDC::InitScale( void )
{
  switch( m_oRbxSweep.GetSelection( ) )
  {
    case eSCALE_LIN :  // Increment the step value by adding constant value
      m_oPnlStep.bSetName( wxT("Step Size") );
      m_oPnlStep.bSetValueType( eVALUE_FLT );
      m_oPnlStep.bShowUnits( PnlValue::eSHOW_CHO );
      m_oPnlStep.bSetSpnRange( 0.0 , 10000.0 );
      m_oPnlStep.bSetSpnIncSz( 0.01,  1000.0 );
      m_oPnlStep.bSetDefValue( 10.0 );
      break;

    case eSCALE_LOG :  // Increase the step by a constant multiplier
      m_oPnlStep.bSetName( wxT("Step Multiplier") );
      m_oPnlStep.bSetValueType( eVALUE_FLT );
      m_oPnlStep.bShowUnits( PnlValue::eSHOW_NONE );
      m_oPnlStep.bSetSpnRange( 0.0 , 10000.0 );
      m_oPnlStep.bSetSpnIncSz( 0.01,  1000.0 );
      m_oPnlStep.bSetDefValue( 1.1 );
      break;

    case eSCALE_DEC :  // Specify a fixed number of steps per decade
      m_oPnlStep.bSetName( wxT("Steps / Decade") );
      m_oPnlStep.bSetValueType( eVALUE_INT );
      m_oPnlStep.bShowUnits( PnlValue::eSHOW_NONE );
      m_oPnlStep.bSetSpnRange( 1, 10000 );
      m_oPnlStep.bSetSpnIncSz( 1,  1000 );
      m_oPnlStep.bSetDefValue( 10 );
      break;

    default :
      break;
  }
}

//**************************************************************************************************
// Clear the object attributes.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlGnuCapDC::bClear( void )
{
  // Clear the base class
  PnlAnaBase::bClear( );

  // Set default step scale type and sweep values
  m_oPnlStart.bSetValue( (float)   0.0 );
  m_oPnlStop .bSetValue( (float) 100.0 );
  m_oPnlStep .bSetValue( (float)  10.0 );

  // Set default scale value
  bSetScale( eSCALE_LIN );

  // Set input source default values
  m_oChoSrcName.Clear( );
  m_oChoSrcName.Append( wxT("None") );
  m_oChoSrcName.SetSelection( 0 );

  // Set parameters check box default values
  m_oCbxVoltage.SetValue( true );
  m_oCbxCurrent.SetValue( false );
  m_oCbxPower  .SetValue( false );
  m_oCbxResist .SetValue( false );

  // Set default temperature value
  m_oPnlTemp.bSetValue( 27.0 );

  return( true );
}

//**************************************************************************************************
// Load information from a simulation object.
//
// Argument List :
//   roSimn - A simulation object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlGnuCapDC::bLoad( SimnGnuCap & roSimn )
{
  bool  bRtn=true;

  // Load the components into the signal source choice box
  PnlAnaBase::LoadSrcNames( roSimn.m_oaCpnts, wxT("VIRLC") );

  // Go no further if the DC command isn't valid
  if( ! roSimn.m_oCmdDC.bIsValid( ) )                        return( false );

  // Set the source component label
  if( ! PnlAnaBase::bSetSrcCpnt( roSimn.m_oCpntSigSrc ) )    bRtn = false;
  InitSwpUnits( );

  // Set the step scale (do this before setting the sweep step)
  if( roSimn.m_oCmdDC.m_eScale != eSCALE_NONE )
  {
    m_oRbxSweep.SetSelection( roSimn.m_oCmdDC.m_eScale );
    InitScale( );
  }

  // Set the sweep values
  if( ! m_oPnlStart.bSetValue( roSimn.m_oCmdDC.m_osStart ) ) bRtn = false;
  if( ! m_oPnlStop .bSetValue( roSimn.m_oCmdDC.m_osStop  ) ) bRtn = false;
  if( ! m_oPnlStep .bSetValue( roSimn.m_oCmdDC.m_osStep  ) ) bRtn = false;

  // Set the parameters to derive
  m_oCbxVoltage.SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_VLT ] );
  m_oCbxCurrent.SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_CUR ] );
  m_oCbxPower  .SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_PWR ] );
  m_oCbxResist .SetValue( roSimn.m_oCmdPR.m_bParams[ ePARAM_RES ] );

  // Set the analysis temperature
  if( ! m_oPnlTemp.bSetValue( roSimn.m_oCmdDC.m_osTempC ) )  bRtn = false;

  return( bRtn );
}

//**************************************************************************************************
// Save information to a simulation object.
//
// Argument List :
//   roSimn - A simulation object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlGnuCapDC::bSave( SimnGnuCap & roSimn )
{
  m_osErrMsg.Empty( );

  // Set the analysis type
  roSimn.m_oCmdPR.bSetAnaType( eCMD_DC );

  // Set the sweep values
  roSimn.m_oCmdDC.m_osStart = m_oPnlStart.rosGetValue( );
  roSimn.m_oCmdDC.m_osStop  = m_oPnlStop .rosGetValue( );
  roSimn.m_oCmdDC.m_osStep  = m_oPnlStep .rosGetValue( );

  // Set the step scale
  roSimn.m_oCmdDC.m_eScale = (eTypeScale) m_oRbxSweep.GetSelection( );

  // Set the sweep source (this is compulsory for a DC analysis)
  if( m_oChoSrcName.GetStringSelection( ) != wxT("None") )
  {
    roSimn.m_oCmdDC.m_osSource = m_oChoSrcName.GetStringSelection( );
    roSimn.m_oCpntSigSrc = roSimn.roGetCpnt( roSimn.m_oCmdDC.m_osSource );
  }

  // Store the parameters to derive
  roSimn.m_oCmdPR.m_bParams[ ePARAM_VLT ] = m_oCbxVoltage.GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_CUR ] = m_oCbxCurrent.GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_PWR ] = m_oCbxPower  .GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_RES ] = m_oCbxResist .GetValue( );

  // Set the analysis temperature
  roSimn.m_oCmdDC.m_osTempC = m_oPnlTemp.rosGetValue( );

  // Create the command strings
  roSimn.m_oCmdDC.bFormat( );
  roSimn.m_oCmdPR.bFormat( );

  // Check for errors
  if( ! roSimn.m_oCmdDC.bIsValid( ) ) SetErrMsg( roSimn.m_oCmdDC.rosGetErrMsg( ) );
  if( ! roSimn.m_oCmdPR.bIsValid( ) ) SetErrMsg( roSimn.m_oCmdPR.rosGetErrMsg( ) );

  return( bIsOk( ) );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Step scale radio box event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event

void  PnlGnuCapDC::OnScale( wxCommandEvent & roEvtCmd )
{
  InitScale( );
}

//**************************************************************************************************
// Source component choice box event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event

void  PnlGnuCapDC::OnSrcName( wxCommandEvent & roEvtCmd )
{
  // Initialize the sweep parameter units
  InitSwpUnits( );
}

//**************************************************************************************************
