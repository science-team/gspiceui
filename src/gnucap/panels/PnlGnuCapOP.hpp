//**************************************************************************************************
//                                         PnlGnuCapOP.hpp                                         *
//                                        -----------------                                        *
// Description : This class derives from the PnlAnaBase base class and provides a GUI for the user *
//               to configure an Operating Point Analysis for GNU-Cap electronic circuit           *
//               simulation engine.                                                                *
// Started     : 2003-11-06                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLGNUCAPOP_HPP
#define PNLGNUCAPOP_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PnlAnaBase.hpp"
#include "netlist/SimnGnuCap.hpp"

//**************************************************************************************************

class PnlGnuCapOP : public PnlAnaBase
{
  private :

    void  Create      ( void );
    void  CreateSigSrc( void ) override;

    void  InitScale( void ) override;

  public :

    explicit  PnlGnuCapOP( wxWindow * poWin );
             ~PnlGnuCapOP( );

    bool  bClear( void ) override;

    bool  bLoad( SimnGnuCap & roSimn );
    bool  bSave( SimnGnuCap & roSimn );

    // Event handlers
    void  OnScale  ( wxCommandEvent & roEvtCmd );
    void  OnSrcName( wxCommandEvent & roEvtCmd );

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLGNUCAPOP_HPP
