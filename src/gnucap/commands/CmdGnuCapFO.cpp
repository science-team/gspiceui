//**************************************************************************************************
//                                         CmdGnuCapFO.cpp                                         *
//                                        -----------------                                        *
// Started     : 2008-03-11                                                                        *
// Last Update : 2018-09-25                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "CmdGnuCapFO.hpp"

//**************************************************************************************************
// Constructor.

CmdGnuCapFO::CmdGnuCapFO( void ) : CmdBase( )
{
  bSetDefaults( );
}

//**************************************************************************************************
// Destructor.

CmdGnuCapFO::~CmdGnuCapFO( )
{
}

//**************************************************************************************************
// Check that the object attributes are valid.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapFO::bValidate( void )
{
  double  df1, df2, df3;

  CmdBase::bValidate( );

  // Check the sweep start and stop values
  if( ! CnvtType::bStrToFlt( m_osStart, &df1 ) )
    SetErrMsg( wxT("Invalid sweep start value.") );
  if( ! CnvtType::bStrToFlt( m_osStop,  &df2 ) )
    SetErrMsg( wxT("Invalid sweep stop value.") );
  if( df2 < df1 )
    SetErrMsg( wxT("Start value greater than stop value.") );

  // Check the sweep step value and initial conditions
  if( ! CnvtType::bStrToFlt( m_osStep, &df3 ) )
    SetErrMsg( wxT("Invalid sweep step value.") );
  if( df3 <= 0.0 )
    SetErrMsg( wxT("Step value less than or equal to zero.") );
  switch( m_eInitC )
  {
    case eINITC_WARM :
    case eINITC_UICS :
    case eINITC_COLD : break;
    default          : SetErrMsg( wxT("Invalid initial conditions value.") );
  }

  // Set the analysis temperature
  if( ! CnvtType::bStrToFlt( m_osTempC, &df3 ) )
    SetErrMsg( wxT("Invalid analysis temperature value.") );

  return( bIsValid( ) );
}

//**************************************************************************************************
// Set the object attributes to they're default values.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapFO::bSetDefaults( void )
{
  CmdBase::bSetDefaults( );

  m_eSimEng  = eSIMR_GNUCAP;
  m_eCmdType = eCMD_FO;

  m_osStart  = wxT("1.0K");
  m_osStop   = wxT("100K");
  m_osStep   = wxT("10K");

  m_eInitC   = eINITC_WARM;

  m_osTempC  = wxT("27.0");

  return( true );
}

//**************************************************************************************************
// Parse the command string.
//
// Eg.s : .FO 1.00K 100.00K 10.00K TE 27.00 COLD BASIC
//        .FO 1.00K 100.00K 10.00K TE 27.00 BASIC
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapFO::bParse( void )
{
  wxStringTokenizer  ostk1;
  wxString           os1;
  double             df1;
  int                i1;

  // Clear the object attributes
  os1 = (wxString &) *this;
  bSetDefaults( );
  assign( os1 );

  // Tokenize the command string
  ostk1.SetString( *this );
  i1 = ostk1.CountTokens( );
  if( i1!=7 && i1!=8 )                                 return( bValidate( ) );

  // Check command type
  os1 = ostk1.GetNextToken( ).Left( 3 ).Upper( );
  if( os1 != wxT(".FO") )                              return( bValidate( ) );

  // Extract the start time
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStart = os1;
  else                                                 return( bValidate( ) );

  // Extract the stop time
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStop  = os1;
  else                                                 return( bValidate( ) );

  // Extract the step increment
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStep  = os1;
  else                                                 return( bValidate( ) );

  // Extract the analysis temperature
  os1 = ostk1.GetNextToken( );
  if( os1.Upper( ) == wxT("TE") )
  {
    os1 = ostk1.GetNextToken( );
    if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osTempC = os1;
    else                                               return( bValidate( ) );
  }

  // Extract the initial conditions
  if( ostk1.CountTokens( ) > 1 )
  {
    os1 = ostk1.GetNextToken( );
    os1.MakeUpper( );
    if(      os1 == wxT("COLD") ) m_eInitC = eINITC_COLD;
    else if( os1 == wxT("UIC") )  m_eInitC = eINITC_UICS;
    else                                               return( bValidate( ) );
  }
  else                            m_eInitC = eINITC_WARM;

  // Check that the last field is "BASIC"
  if( ostk1.GetNextToken( ).Upper( ) != wxT("BASIC") ) return( bValidate( ) );

  return( bValidate( ) );
}

//**************************************************************************************************
// Format the command string.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapFO::bFormat( void )
{
  wxString  osCmd, os1;

  // Set the command name
  osCmd = wxT(".FO");

  // Set sweep parameters
  osCmd << wxT(' ') << m_osStart;
  osCmd << wxT(' ') << m_osStop;
  osCmd << wxT(' ') << m_osStep;

  // Set analysis temperature
  osCmd << wxT(" TE ") << m_osTempC;

  // Set the initial conditions
  switch( m_eInitC )
  {
    case eINITC_COLD : osCmd << wxT(" COLD"); break;
    case eINITC_UICS : osCmd << wxT(" UICS"); break;
    case eINITC_WARM :                        break;
    default : break;
  }

  // Append format modifier
  osCmd << wxT(" BASIC");

  assign( osCmd );

  return( bValidate( ) );
}

//**************************************************************************************************
// Print the object attributes.
//
// Argument List :
//   rosPrefix - A prefix to every line displayed (usually just spaces)

void  CmdGnuCapFO::Print( const wxString & rosPrefix )
{
  CmdBase::Print( rosPrefix + wxT("CmdBase::") );

  std::cout << rosPrefix.mb_str( ) << "m_osStart : " << m_osStart.mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStop  : " << m_osStop .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStep  : " << m_osStep .mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_eInitC  : ";
  switch( m_eInitC )
  {
    case eINITC_WARM : std::cout << "eINITC_COLD\n"; break;
    case eINITC_UICS : std::cout << "eINITC_UICS\n"; break;
    case eINITC_COLD : std::cout << "eINITC_COLD\n"; break;
    case eINITC_NONE : std::cout << "eINITC_NONE\n"; break;
    default :          std::cout << "Invalid\n";     break;
  }

  std::cout << rosPrefix.mb_str( ) << "m_osTempC : " << m_osTempC.mb_str( ) << '\n';
}

//**************************************************************************************************
//                                          Test Utility                                           *
//**************************************************************************************************

#ifdef TEST_CMDGNUCAPFO

using  namespace  std;

// Function prototypes

void  Usage( char * psAppName );

//**************************************************************************************************

int  main( int argc, char * argv[ ] )
{
  wxString  osCmd;
  wxString  os1;

  // Validate the argument count passed to the application
  if( argc > 2 )           { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }

  // Process the command line arguments
  os1 = wxConvLibc.cMB2WC( argv[ 1 ] );
  if( argc > 1 )
  {
    if( os1 == wxT("-h") ) { Usage( argv[ 0 ] ); exit( EXIT_SUCCESS ); }
    else                   { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }
  }

  // Display the utility banner
  cout << "\n  Class CmdGnuCapFO Test Utility"
       << "\n    Version 1.02 (12/08/2011)\n";

  // Create a GNU-CAP Transient command object
  CmdGnuCapFO  tCmd_FO;

  // Use the following command example to check the formatter and the parser :
  osCmd = wxT(".FO 1.00K 100.00K 10.00K TE 30.00 COLD BASIC");

  // Set things up for a formatter test
  tCmd_FO.m_osStart = wxT("1.00K");
  tCmd_FO.m_osStop  = wxT("100.00K");
  tCmd_FO.m_osStep  = wxT("10.00K");
  tCmd_FO.m_osTempC = wxT("30.00");
  tCmd_FO.m_eInitC  = eINITC_COLD;
  cout << "\nRun Formatter    : " << ( tCmd_FO.bFormat( ) ? "OK" : "FAULT" );
  cout << "\nTest Cmd Format  : " << ( tCmd_FO == osCmd   ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\ntCmd_FO Contents : " << tCmd_FO.mb_str( ) << '\n';

  // Set things up for a parser test
  tCmd_FO.bSetString( osCmd );
  cout << "\nRun Parser       : " << ( tCmd_FO.bParse( ) ? "OK" : "FAULT" );
  tCmd_FO.bFormat( );
  cout << "\nTest Cmd Format  : " << ( tCmd_FO == osCmd  ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\ntCmd_FO Contents : " << tCmd_FO.mb_str( ) << '\n';

  cout << '\n';

  exit( EXIT_SUCCESS );
}

//**************************************************************************************************

void  Usage( char * psAppName )
{
  cout << "\nUsage   : " << psAppName << " [-OPTIONS]"
       << "\nOptions :"
       << "\n  -h : Print usage (this message)\n";
}

#endif // TEST_CMDGNUCAPFO

//**************************************************************************************************
