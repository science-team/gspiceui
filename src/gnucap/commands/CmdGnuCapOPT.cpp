//**************************************************************************************************
//                                         CmdGnuCapOPT.cpp                                        *
//                                        ------------------                                       *
// Started     : 2006-09-11                                                                        *
// Last Update : 2018-11-12                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "CmdGnuCapOPT.hpp"

//**************************************************************************************************
// Constructor.

CmdGnuCapOPT::CmdGnuCapOPT( void ) : CmdBase( )
{
  bSetDefaults( );
}

//**************************************************************************************************
// Destructor.

CmdGnuCapOPT::~CmdGnuCapOPT( )
{
}

//**************************************************************************************************
// Check that the object attributes are valid.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOPT::bValidate( void )
{
  wxString  os1=wxT("Invalid value for ");
  double    df1;
  long      li1;

  CmdBase::bValidate( );

  if( ! CnvtType::bStrToFlt( m_osABSTOL  , &df1 ) ) SetErrMsg( os1 + wxT("ABSTOL.")       );
  if( ! CnvtType::bStrToFlt( m_osCHGTOL  , &df1 ) ) SetErrMsg( os1 + wxT("CHGTOL.")       );
  if( ! CnvtType::bStrToFlt( m_osDAMPMAX , &df1 ) ) SetErrMsg( os1 + wxT("DAMPMAX.")      );
  if( ! CnvtType::bStrToFlt( m_osDAMPMIN , &df1 ) ) SetErrMsg( os1 + wxT("DAMPMIN.")      );
  if( ! CnvtType::bStrToFlt( m_osDEFAD   , &df1 ) ) SetErrMsg( os1 + wxT("DEFAD.")        );
  if( ! CnvtType::bStrToFlt( m_osDEFAS   , &df1 ) ) SetErrMsg( os1 + wxT("DEFAS.")        );
  if( ! CnvtType::bStrToFlt( m_osDEFL    , &df1 ) ) SetErrMsg( os1 + wxT("DEFL.")         );
  if( ! CnvtType::bStrToFlt( m_osDEFW    , &df1 ) ) SetErrMsg( os1 + wxT("DEFW.")         );
  if( ! CnvtType::bStrToFlt( m_osDTMIN   , &df1 ) ) SetErrMsg( os1 + wxT("DTMIN.")        );
  if( ! CnvtType::bStrToFlt( m_osDTRATIO , &df1 ) ) SetErrMsg( os1 + wxT("DTRATIO.")      );
  if( ! CnvtType::bStrToFlt( m_osFLOOR   , &df1 ) ) SetErrMsg( os1 + wxT("FLOOR.")        );
  if( ! CnvtType::bStrToFlt( m_osGMIN    , &df1 ) ) SetErrMsg( os1 + wxT("GMIN.")         );
  if( ! CnvtType::bStrToFlt( m_osRELTOL  , &df1 ) ) SetErrMsg( os1 + wxT("RELTOL.")       );
  if( ! CnvtType::bStrToFlt( m_osROUND   , &df1 ) ) SetErrMsg( os1 + wxT("ROUND.")        );
  if( ! CnvtType::bStrToFlt( m_osSHORT   , &df1 ) ) SetErrMsg( os1 + wxT("SHORT.")        );
  if( ! CnvtType::bStrToFlt( m_osTEMP    , &df1 ) ) SetErrMsg( os1 + wxT("TEMP.")         );
  if( ! CnvtType::bStrToFlt( m_osTNOM    , &df1 ) ) SetErrMsg( os1 + wxT("TNOM.")         );
  if( ! CnvtType::bStrToFlt( m_osTRREJECT, &df1 ) ) SetErrMsg( os1 + wxT("TRREJECT.")     );
  if( ! CnvtType::bStrToFlt( m_osTRSTEPG , &df1 ) ) SetErrMsg( os1 + wxT("TRSTEPGROW.")   );
  if( ! CnvtType::bStrToFlt( m_osTRSTEPH , &df1 ) ) SetErrMsg( os1 + wxT("TRSTEPHOLD.")   );
  if( ! CnvtType::bStrToFlt( m_osTRSTEPS , &df1 ) ) SetErrMsg( os1 + wxT("TRSTEPSHRINK.") );
  if( ! CnvtType::bStrToFlt( m_osTRTOL   , &df1 ) ) SetErrMsg( os1 + wxT("TRTOL.")        );
  if( ! CnvtType::bStrToFlt( m_osVFLOOR  , &df1 ) ) SetErrMsg( os1 + wxT("VFLOOR.")       );
  if( ! CnvtType::bStrToFlt( m_osVMAX    , &df1 ) ) SetErrMsg( os1 + wxT("VMAX.")         );
  if( ! CnvtType::bStrToFlt( m_osVMIN    , &df1 ) ) SetErrMsg( os1 + wxT("VMIN.")         );
  if( ! CnvtType::bStrToFlt( m_osVNTOL   , &df1 ) ) SetErrMsg( os1 + wxT("VNTOL.")        );

  if( ! CnvtType::bStrToInt( m_osDAMPST  , &li1 ) ) SetErrMsg( os1 + wxT("DAMPST.")       );
  if( ! CnvtType::bStrToInt( m_osHARMS   , &li1 ) ) SetErrMsg( os1 + wxT("HARMONICS.")    );
  if( ! CnvtType::bStrToInt( m_osITERMIN , &li1 ) ) SetErrMsg( os1 + wxT("ITERMIN.")      );
  if( ! CnvtType::bStrToInt( m_osITL1    , &li1 ) ) SetErrMsg( os1 + wxT("ITL1.")         );
  if( ! CnvtType::bStrToInt( m_osITL2    , &li1 ) ) SetErrMsg( os1 + wxT("ITL2.")         );
  if( ! CnvtType::bStrToInt( m_osITL3    , &li1 ) ) SetErrMsg( os1 + wxT("ITL3.")         );
  if( ! CnvtType::bStrToInt( m_osITL4    , &li1 ) ) SetErrMsg( os1 + wxT("ITL4.")         );
  if( ! CnvtType::bStrToInt( m_osITL5    , &li1 ) ) SetErrMsg( os1 + wxT("ITL5.")         );
  if( ! CnvtType::bStrToInt( m_osITL6    , &li1 ) ) SetErrMsg( os1 + wxT("ITL6.")         );
  if( ! CnvtType::bStrToInt( m_osITL7    , &li1 ) ) SetErrMsg( os1 + wxT("ITL7.")         );
  if( ! CnvtType::bStrToInt( m_osITL8    , &li1 ) ) SetErrMsg( os1 + wxT("ITL8.")         );
  if( ! CnvtType::bStrToInt( m_osNUMDGT  , &li1 ) ) SetErrMsg( os1 + wxT("NUMDGT.")       );
  if( ! CnvtType::bStrToInt( m_osRECURS  , &li1 ) ) SetErrMsg( os1 + wxT("RECURS.")       );
  if( ! CnvtType::bStrToInt( m_osTRANSITS, &li1 ) ) SetErrMsg( os1 + wxT("TRANSITS.")     );

  if( m_osMETHOD.IsEmpty( ) )                       SetErrMsg( os1 + wxT("METHOD.")       );
  if( m_osMODE  .IsEmpty( ) )                       SetErrMsg( os1 + wxT("MODE.")         );
  if( m_osORDER .IsEmpty( ) )                       SetErrMsg( os1 + wxT("ORDER.")        );
  if( m_osPHASE .IsEmpty( ) )                       SetErrMsg( os1 + wxT("PHASE.")        );

  return( bIsValid( ) );
}

//**************************************************************************************************
// Set the object attributes to they're default values.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOPT::bSetDefaults( void )
{
  CmdBase::bSetDefaults( );

  m_eSimEng    = eSIMR_GNUCAP;
  m_eCmdType   = eCMD_OPT;

  m_osABSTOL   = GCP_ABSTOL;
  m_osCHGTOL   = GCP_CHGTOL;
  m_osDAMPMAX  = GCP_DAMPMAX;
  m_osDAMPMIN  = GCP_DAMPMIN;
  m_osDAMPST   = GCP_DAMPST;
  m_osDEFAD    = GCP_DEFAD;
  m_osDEFAS    = GCP_DEFAS;
  m_osDEFL     = GCP_DEFL;
  m_osDEFW     = GCP_DEFW;
  m_osDTMIN    = GCP_DTMIN;
  m_osDTRATIO  = GCP_DTRATIO;
  m_osFLOOR    = GCP_FLOOR;
  m_osGMIN     = GCP_GMIN;
  m_osRELTOL   = GCP_RELTOL;
  m_osROUND    = GCP_ROUND;
  m_osSHORT    = GCP_SHORT;
  m_osTEMP     = GCP_TEMP;
  m_osTNOM     = GCP_TNOM;
  m_osTRREJECT = GCP_TRREJECT;
  m_osTRSTEPG  = GCP_TRSTEPG;
  m_osTRSTEPH  = GCP_TRSTEPH;
  m_osTRSTEPS  = GCP_TRSTEPS;
  m_osTRTOL    = GCP_TRTOL;
  m_osVFLOOR   = GCP_VFLOOR;
  m_osVMAX     = GCP_VMAX;
  m_osVMIN     = GCP_VMIN;
  m_osVNTOL    = GCP_VNTOL;

  m_osHARMS    = GCP_HARMS;
  m_osITL1     = GCP_ITL1;
  m_osITL2     = GCP_ITL2;
  m_osITL3     = GCP_ITL3;
  m_osITL4     = GCP_ITL4;
  m_osITL5     = GCP_ITL5;
  m_osITL6     = GCP_ITL6;
  m_osITL7     = GCP_ITL7;
  m_osITL8     = GCP_ITL8;
  m_osITERMIN  = GCP_ITERMIN;
  m_osNUMDGT   = GCP_NUMDGT;
  m_osRECURS   = GCP_RECURS;
  m_osTRANSITS = GCP_TRANSITS;

  m_osMETHOD   = GCP_METHOD;
  m_osMODE     = GCP_MODE;
  m_osORDER    = GCP_ORDER;
  m_osPHASE    = GCP_PHASE;

  m_bBYPASS    = GCP_BYPASS;
  m_bCSTRAY    = GCP_CSTRAY;
  m_bFBBYPASS  = GCP_FBBYPASS;
  m_bINCMODE   = GCP_INCMODE;
  m_bLUBYPASS  = GCP_LUBYPASS;
  m_bOPTS      = GCP_OPTS;
  m_bQUITCONV  = GCP_QUITCONV;
  m_bRSTRAY    = GCP_RSTRAY;
  m_bTRACEL    = GCP_TRACEL;

  return( true );
}

//**************************************************************************************************
// Parse the OPTIONS command string.
//
// Eg.s :
//   .OPTIONS NOOPTS
//   .OPTIONS ABSTOL=1.20p CHGTOL=12.00f DAMPMAX=900.00m DAMPMIN=600.00m DEFAD=11.00f DEFAS=11.00f
//            DEFL=120.00u DEFW=120.00u DTMIN=1.10p DTRATIO=1.10Giga FLOOR=1.10E-21 GMIN=1.20p
//            RELTOL=3.00m ROUND=1.0E-10 SHORT=110.00n TEMP=30.00 TNOM=35.00 TRREJECT=600.00m
//            TRSTEPGROW=3.00 TRSTEPHOLD=100.00 TRSTEPSHRINK=9.00 TRTOL=9.00 VFLOOR=1.10f VMAX=40.00
//            VMIN=-20.00 VNTOL=3.00u DAMPST=1 HARMONICS=10 ITL1=200 ITL2=70 ITL3=10 ITL4=30 ITL5=2
//            ITL6=2 ITL7=2 ITL8=200 ITERMIN=2 NUMDGT=6 RECURS=10 TRANSITS=3 NOBYP NOCSTRAY NOFBBYP
//            NOINC NOLUBYP NOOPTS QUITCONV RSTRAY NOTRACEL METHOD=EULER MODE=ANALOG ORDER=FORWARD
//            PHASE=RADIANS
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOPT::bParse( void )
{
  wxStringTokenizer  ostk1;
  wxString           os1, os2;
  size_t             sz1;

  // Clear the object attributes
  os1 = (wxString &) *this;
  bSetDefaults( );
  assign( os1 );

  // Tokenize the command string
  ostk1.SetString( *this );
  if( ostk1.CountTokens( ) < 2 )      return( bValidate( ) );

  // Check command type
  os1 = ostk1.GetNextToken( ).Left( 4 ).Upper( );
  if( os1 != wxT(".OPT") )            return( bValidate( ) );

  // Extract each parameter value
  while( ostk1.HasMoreTokens( ) )
  {
    // Extract the field name and the associated value
    os1 = ostk1.GetNextToken( );
    os2 = wxT("");
    if( ( sz1=os1.find( wxT("=") ) ) != wxString::npos )
    {
      os2 = os1.Right( os1.Length( )-sz1-1 );
      os1 = os1.Left( sz1 );
    }

    // Set the object attribute values
    if(      os1.StartsWith( wxT("ABSTOL")   ) ) m_osABSTOL   = os2;
    else if( os1.StartsWith( wxT("CHGTOL")   ) ) m_osCHGTOL   = os2;
    else if( os1.StartsWith( wxT("DAMPMAX")  ) ) m_osDAMPMAX  = os2;
    else if( os1.StartsWith( wxT("DAMPMIN")  ) ) m_osDAMPMIN  = os2;
    else if( os1.StartsWith( wxT("DEFAD")    ) ) m_osDEFAD    = os2;
    else if( os1.StartsWith( wxT("DEFAS")    ) ) m_osDEFAS    = os2;
    else if( os1.StartsWith( wxT("DEFL")     ) ) m_osDEFL     = os2;
    else if( os1.StartsWith( wxT("DEFW")     ) ) m_osDEFW     = os2;
    else if( os1.StartsWith( wxT("DTMIN")    ) ) m_osDTMIN    = os2;
    else if( os1.StartsWith( wxT("DTRATIO")  ) ) m_osDTRATIO  = os2;
    else if( os1.StartsWith( wxT("FLOOR")    ) ) m_osFLOOR    = os2;
    else if( os1.StartsWith( wxT("GMIN")     ) ) m_osGMIN     = os2;
    else if( os1.StartsWith( wxT("RELTOL")   ) ) m_osRELTOL   = os2;
    else if( os1.StartsWith( wxT("ROUND")    ) ) m_osROUND    = os2;
    else if( os1.StartsWith( wxT("SHORT")    ) ) m_osSHORT    = os2;
    else if( os1.StartsWith( wxT("TEMP")     ) ) m_osTEMP     = os2;
    else if( os1.StartsWith( wxT("TNOM")     ) ) m_osTNOM     = os2;
    else if( os1.StartsWith( wxT("TRREJECT") ) ) m_osTRREJECT = os2;
    else if( os1.StartsWith( wxT("TRSTEPGR") ) ) m_osTRSTEPG  = os2;
    else if( os1.StartsWith( wxT("TRSTEPHO") ) ) m_osTRSTEPH  = os2;
    else if( os1.StartsWith( wxT("TRSTEPSH") ) ) m_osTRSTEPS  = os2;
    else if( os1.StartsWith( wxT("TRTOL")    ) ) m_osTRTOL    = os2;
    else if( os1.StartsWith( wxT("VFLOOR")   ) ) m_osVFLOOR   = os2;
    else if( os1.StartsWith( wxT("VMAX")     ) ) m_osVMAX     = os2;
    else if( os1.StartsWith( wxT("VMIN")     ) ) m_osVMIN     = os2;
    else if( os1.StartsWith( wxT("VNTOL")    ) ) m_osVNTOL    = os2;

    else if( os1.StartsWith( wxT("DAMPST")   ) ) m_osDAMPST   = os2;
    else if( os1.StartsWith( wxT("HARMONIC") ) ) m_osHARMS    = os2;
    else if( os1.StartsWith( wxT("ITL1")     ) ) m_osITL1     = os2;
    else if( os1.StartsWith( wxT("ITL2")     ) ) m_osITL2     = os2;
    else if( os1.StartsWith( wxT("ITL3")     ) ) m_osITL3     = os2;
    else if( os1.StartsWith( wxT("ITL4")     ) ) m_osITL4     = os2;
    else if( os1.StartsWith( wxT("ITL5")     ) ) m_osITL5     = os2;
    else if( os1.StartsWith( wxT("ITL6")     ) ) m_osITL6     = os2;
    else if( os1.StartsWith( wxT("ITL7")     ) ) m_osITL7     = os2;
    else if( os1.StartsWith( wxT("ITL8")     ) ) m_osITL8     = os2;
    else if( os1.StartsWith( wxT("ITERMIN")  ) ) m_osITERMIN  = os2;
    else if( os1.StartsWith( wxT("NUMDGT")   ) ) m_osNUMDGT   = os2;
    else if( os1.StartsWith( wxT("RECURS")   ) ) m_osRECURS   = os2;
    else if( os1.StartsWith( wxT("TRANSITS") ) ) m_osTRANSITS = os2;

    else if( os1.StartsWith( wxT("METHOD")   ) ) m_osMETHOD   = os2;
    else if( os1.StartsWith( wxT("MODE")     ) ) m_osMODE     = os2;
    else if( os1.StartsWith( wxT("ORDER")    ) ) m_osORDER    = os2;
    else if( os1.StartsWith( wxT("PHASE")    ) ) m_osPHASE    = os2;

    else if( os1.StartsWith( wxT("BYPASS")   ) ) m_bBYPASS    = true;
    else if( os1.StartsWith( wxT("NOBYP")    ) ) m_bBYPASS    = false;
    else if( os1.StartsWith( wxT("CSTRAY" )  ) ) m_bCSTRAY    = true;
    else if( os1.StartsWith( wxT("NOCSTRAY") ) ) m_bCSTRAY    = false;
    else if( os1.StartsWith( wxT("FBBYPASS") ) ) m_bFBBYPASS  = true;
    else if( os1.StartsWith( wxT("NOFBBYP")  ) ) m_bFBBYPASS  = false;
    else if( os1.StartsWith( wxT("INCMODE")  ) ) m_bINCMODE   = true;
    else if( os1.StartsWith( wxT("NOINC")    ) ) m_bINCMODE   = false;
    else if( os1.StartsWith( wxT("LUBYPASS") ) ) m_bLUBYPASS  = true;
    else if( os1.StartsWith( wxT("NOLUBYP")  ) ) m_bLUBYPASS  = false;
    else if( os1.StartsWith( wxT("OPTS")     ) ) m_bOPTS      = true;
    else if( os1.StartsWith( wxT("NOOPTS")   ) ) m_bOPTS      = false;
    else if( os1.StartsWith( wxT("QUITCONV") ) ) m_bQUITCONV  = true;
    else if( os1.StartsWith( wxT("NOQUITCO") ) ) m_bQUITCONV  = false;
    else if( os1.StartsWith( wxT("RSTRAY")   ) ) m_bRSTRAY    = true;
    else if( os1.StartsWith( wxT("NORSTRAY") ) ) m_bRSTRAY    = false;
    else if( os1.StartsWith( wxT("TRACEL")   ) ) m_bTRACEL    = true;
    else if( os1.StartsWith( wxT("NOTRACEL") ) ) m_bTRACEL    = false;
  }

  return( bValidate( ) );
}

//**************************************************************************************************
// Format the OPTIONS command string.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOPT::bFormat( void )
{
  wxString  os1, os2;
  double    df1, df2;
  long      li1, li2;

  os1 = wxT(".OPTIONS");

  CnvtType::bStrToFlt( m_osABSTOL  , &df1 );
  CnvtType::bStrToFlt( GCP_ABSTOL  , &df2 );
  if( df1 != df2 )                  os1 << wxT(" ABSTOL=")       << m_osABSTOL;

  CnvtType::bStrToFlt( m_osCHGTOL  , &df1 );
  CnvtType::bStrToFlt( GCP_CHGTOL  , &df2 );
  if( df1 != df2 )                  os1 << wxT(" CHGTOL=")       << m_osCHGTOL;

  CnvtType::bStrToFlt( m_osDAMPMAX , &df1 );
  CnvtType::bStrToFlt( GCP_DAMPMAX , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DAMPMAX=")      << m_osDAMPMAX;

  CnvtType::bStrToFlt( m_osDAMPMIN , &df1 );
  CnvtType::bStrToFlt( GCP_DAMPMIN , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DAMPMIN=")      << m_osDAMPMIN;

  CnvtType::bStrToFlt( m_osDEFAD   , &df1 );
  CnvtType::bStrToFlt( GCP_DEFAD   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DEFAD=")        << m_osDEFAD;

  CnvtType::bStrToFlt( m_osDEFAS   , &df1 );
  CnvtType::bStrToFlt( GCP_DEFAS   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DEFAS=")        << m_osDEFAS;

  CnvtType::bStrToFlt( m_osDEFL    , &df1 );
  CnvtType::bStrToFlt( GCP_DEFL    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DEFL=")         << m_osDEFL;

  CnvtType::bStrToFlt( m_osDEFW    , &df1 );
  CnvtType::bStrToFlt( GCP_DEFW    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DEFW=")         << m_osDEFW;

  CnvtType::bStrToFlt( m_osDTMIN   , &df1 );
  CnvtType::bStrToFlt( GCP_DTMIN   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DTMIN=")        << m_osDTMIN;

  CnvtType::bStrToFlt( m_osDTRATIO , &df1 );
  CnvtType::bStrToFlt( GCP_DTRATIO , &df2 );
  if( df1 != df2 )                  os1 << wxT(" DTRATIO=")      << m_osDTRATIO;

  CnvtType::bStrToFlt( m_osFLOOR   , &df1 );
  CnvtType::bStrToFlt( GCP_FLOOR   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" FLOOR=")        << m_osFLOOR;

  CnvtType::bStrToFlt( m_osGMIN    , &df1 );
  CnvtType::bStrToFlt( GCP_GMIN    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" GMIN=")         << m_osGMIN;

  CnvtType::bStrToFlt( m_osRELTOL  , &df1 );
  CnvtType::bStrToFlt( GCP_RELTOL  , &df2 );
  if( df1 != df2 )                  os1 << wxT(" RELTOL=")       << m_osRELTOL;

  CnvtType::bStrToFlt( m_osROUND   , &df1 );
  CnvtType::bStrToFlt( GCP_ROUND   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" ROUND=")        << m_osROUND;

  CnvtType::bStrToFlt( m_osSHORT   , &df1 );
  CnvtType::bStrToFlt( GCP_SHORT   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" SHORT=")        << m_osSHORT;

  CnvtType::bStrToFlt( m_osTEMP    , &df1 );
  CnvtType::bStrToFlt( GCP_TEMP    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" TEMP=")         << m_osTEMP;

  CnvtType::bStrToFlt( m_osTNOM    , &df1 );
  CnvtType::bStrToFlt( GCP_TNOM    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" TNOM=")         << m_osTNOM;

  CnvtType::bStrToFlt( m_osTRREJECT, &df1 );
  CnvtType::bStrToFlt( GCP_TRREJECT, &df2 );
  if( df1 != df2 )                  os1 << wxT(" TRREJECT=")     << m_osTRREJECT;

  CnvtType::bStrToFlt( m_osTRSTEPG , &df1 );
  CnvtType::bStrToFlt( GCP_TRSTEPG , &df2 );
  if( df1 != df2 )                  os1 << wxT(" TRSTEPGROW=")   << m_osTRSTEPG;

  CnvtType::bStrToFlt( m_osTRSTEPH , &df1 );
  CnvtType::bStrToFlt( GCP_TRSTEPH , &df2 );
  if( df1 != df2 )                  os1 << wxT(" TRSTEPHOLD=")   << m_osTRSTEPH;

  CnvtType::bStrToFlt( m_osTRSTEPS , &df1 );
  CnvtType::bStrToFlt( GCP_TRSTEPS , &df2 );
  if( df1 != df2 )                  os1 << wxT(" TRSTEPSHRINK=") << m_osTRSTEPS;

  CnvtType::bStrToFlt( m_osTRTOL   , &df1 );
  CnvtType::bStrToFlt( GCP_TRTOL   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" TRTOL=")        << m_osTRTOL;

  CnvtType::bStrToFlt( m_osVFLOOR  , &df1 );
  CnvtType::bStrToFlt( GCP_VFLOOR  , &df2 );
  if( df1 != df2 )                  os1 << wxT(" VFLOOR=")       << m_osVFLOOR;

  CnvtType::bStrToFlt( m_osVMAX    , &df1 );
  CnvtType::bStrToFlt( GCP_VMAX    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" VMAX=")         << m_osVMAX;

  CnvtType::bStrToFlt( m_osVMIN    , &df1 );
  CnvtType::bStrToFlt( GCP_VMIN    , &df2 );
  if( df1 != df2 )                  os1 << wxT(" VMIN=")         << m_osVMIN;

  CnvtType::bStrToFlt( m_osVNTOL   , &df1 );
  CnvtType::bStrToFlt( GCP_VNTOL   , &df2 );
  if( df1 != df2 )                  os1 << wxT(" VNTOL=")        << m_osVNTOL;

  CnvtType::bStrToInt( m_osDAMPST  , &li1 );
  CnvtType::bStrToInt( GCP_DAMPST  , &li2 );
  if( li1 != li2 )                  os1 << wxT(" DAMPST=")       << m_osDAMPST;

  CnvtType::bStrToInt( m_osHARMS   , &li1 );
  CnvtType::bStrToInt( GCP_HARMS   , &li2 );
  if( li1 != li2 )                  os1 << wxT(" HARMONICS=")    << m_osHARMS;

  CnvtType::bStrToInt( m_osITL1    , &li1 );
  CnvtType::bStrToInt( GCP_ITL1    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL1=")         << m_osITL1;

  CnvtType::bStrToInt( m_osITL2    , &li1 );
  CnvtType::bStrToInt( GCP_ITL2    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL2=")         << m_osITL2;

  CnvtType::bStrToInt( m_osITL3    , &li1 );
  CnvtType::bStrToInt( GCP_ITL3    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL3=")         << m_osITL3;

  CnvtType::bStrToInt( m_osITL4    , &li1 );
  CnvtType::bStrToInt( GCP_ITL4    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL4=")         << m_osITL4;

  CnvtType::bStrToInt( m_osITL5    , &li1 );
  CnvtType::bStrToInt( GCP_ITL5    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL5=")         << m_osITL5;

  CnvtType::bStrToInt( m_osITL6    , &li1 );
  CnvtType::bStrToInt( GCP_ITL6    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL6=")         << m_osITL6;

  CnvtType::bStrToInt( m_osITL7    , &li1 );
  CnvtType::bStrToInt( GCP_ITL7    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL7=")         << m_osITL7;

  CnvtType::bStrToInt( m_osITL8    , &li1 );
  CnvtType::bStrToInt( GCP_ITL8    , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITL8=")         << m_osITL8;

  CnvtType::bStrToInt( m_osITERMIN , &li1 );
  CnvtType::bStrToInt( GCP_ITERMIN , &li2 );
  if( li1 != li2 )                  os1 << wxT(" ITERMIN=")      << m_osITERMIN;

  CnvtType::bStrToInt( m_osNUMDGT  , &li1 );
  CnvtType::bStrToInt( GCP_NUMDGT  , &li2 );
  if( li1 != li2 )                  os1 << wxT(" NUMDGT=")       << m_osNUMDGT;

  CnvtType::bStrToInt( m_osRECURS  , &li1 );
  CnvtType::bStrToInt( GCP_RECURS  , &li2 );
  if( li1 != li2 )                  os1 << wxT(" RECURS=")       << m_osRECURS;

  CnvtType::bStrToInt( m_osTRANSITS, &li1 );
  CnvtType::bStrToInt( GCP_TRANSITS, &li2 );
  if( li1 != li2 )                  os1 << wxT(" TRANSITS=")     << m_osTRANSITS;

  os2 = m_osMETHOD.Upper( );
  if( os2.CmpNoCase( GCP_METHOD ) ) os1 << wxT(" METHOD=")       << os2;

  os2 = m_osMODE  .Upper( );
  if( os2.CmpNoCase( GCP_MODE )   ) os1 << wxT(" MODE=")         << os2;

  os2 = m_osORDER .Upper( );
  if( os2.CmpNoCase( GCP_ORDER )  ) os1 << wxT(" ORDER=")        << os2;

  os2 = m_osPHASE .Upper( );
  if( os2.CmpNoCase( GCP_PHASE )  ) os1 << wxT(" PHASE=")        << os2;

  if( m_bBYPASS   != GCP_BYPASS   ) os1 << (m_bBYPASS   ? wxT(" BYPASS")   : wxT(" NOBYP")   );
  if( m_bCSTRAY   != GCP_CSTRAY   ) os1 << (m_bCSTRAY   ? wxT(" CSTRAY")   : wxT(" NOCSTRAY"));
  if( m_bFBBYPASS != GCP_FBBYPASS ) os1 << (m_bFBBYPASS ? wxT(" FBBYPASS") : wxT(" NOFBBYP") );
  if( m_bINCMODE  != GCP_INCMODE  ) os1 << (m_bINCMODE  ? wxT(" INCMODE")  : wxT(" NOINC")   );
  if( m_bLUBYPASS != GCP_LUBYPASS ) os1 << (m_bLUBYPASS ? wxT(" LUBYP")    : wxT(" NOLUBYP") );
  if( m_bOPTS     != GCP_OPTS     ) os1 << (m_bOPTS     ? wxT(" OPTS")     : wxT(" NOOPTS")  );
  if( m_bQUITCONV != GCP_QUITCONV ) os1 << (m_bQUITCONV ? wxT(" QUITCONV") : wxT(" NOQUITCO"));
  if( m_bRSTRAY   != GCP_RSTRAY   ) os1 << (m_bRSTRAY   ? wxT(" RSTRAY")   : wxT(" NORSTRAY"));
  if( m_bTRACEL   != GCP_TRACEL   ) os1 << (m_bTRACEL   ? wxT(" TRACEL")   : wxT(" NOTRACEL"));

  assign( os1 );

  return( bValidate( ) );
}

//**************************************************************************************************
// Copy the contents of a CmdNgSpiceOPT object.
//
// Argument List :
//   roCmdOPT - A reference to a CmdNgSpiceOPT object
//
// Return Values :
//   A reference to this object

CmdGnuCapOPT & CmdGnuCapOPT::operator = ( const CmdNgSpiceOPT & roCmdOPT )
{
  (CmdBase &) *this = (CmdBase &) roCmdOPT;

  m_osABSTOL   = roCmdOPT.m_osABSTOL;
  m_osCHGTOL   = roCmdOPT.m_osCHGTOL;
//m_osDAMPMAX  = roCmdOPT.m_osDAMPMAX;
//m_osDAMPMIN  = roCmdOPT.m_osDAMPMIN;
//m_osDAMPST   = roCmdOPT.m_osDAMPST;
  m_osDEFAD    = roCmdOPT.m_osDEFAD;
  m_osDEFAS    = roCmdOPT.m_osDEFAS;
  m_osDEFL     = roCmdOPT.m_osDEFL;
  m_osDEFW     = roCmdOPT.m_osDEFW;
//m_osDTMIN    = roCmdOPT.m_osDTMIN;
//m_osDTRATIO  = roCmdOPT.m_osDTRATIO;
//m_osFLOOR    = roCmdOPT.m_osFLOOR;
  m_osGMIN     = roCmdOPT.m_osGMIN;
  m_osRELTOL   = roCmdOPT.m_osRELTOL;
//m_osROUND    = roCmdOPT.m_osROUND;
//m_osSHORT    = roCmdOPT.m_osSHORT;
  m_osTEMP     = roCmdOPT.m_osTEMP;
  m_osTNOM     = roCmdOPT.m_osTNOM;
//m_osTRREJECT = roCmdOPT.m_osTRREJECT;
//m_osTRSTEPG  = roCmdOPT.m_osTRSTEPG;
//m_osTRSTEPH  = roCmdOPT.m_osTRSTEPH;
//m_osTRSTEPS  = roCmdOPT.m_osTRSTEPS;
  m_osTRTOL    = roCmdOPT.m_osTRTOL;
//m_osVFLOOR   = roCmdOPT.m_osVFLOOR;
//m_osVMAX     = roCmdOPT.m_osVMAX;
//m_osVMIN     = roCmdOPT.m_osVMIN;
  m_osVNTOL    = roCmdOPT.m_osVNTOL;

//m_osHARMS    = roCmdOPT.m_osHARMS;
  m_osITL1     = roCmdOPT.m_osITL1;
  m_osITL2     = roCmdOPT.m_osITL2;
  m_osITL3     = roCmdOPT.m_osITL3;
  m_osITL4     = roCmdOPT.m_osITL4;
  m_osITL5     = roCmdOPT.m_osITL5;
  m_osITL6     = roCmdOPT.m_osITL6;
//m_osITL7     = roCmdOPT.m_osITL7;
//m_osITL8     = roCmdOPT.m_osITL8;
//m_osITERMIN  = roCmdOPT.m_osITERMIN;
//m_osNUMDGT   = roCmdOPT.m_osNUMDGT;
//m_osRECURS   = roCmdOPT.m_osRECURS;
//m_osTRANSITS = roCmdOPT.m_osTRANSITS;

  m_osMETHOD   = roCmdOPT.m_osMETHOD;
//m_osMODE     = roCmdOPT.m_osMODE;
//m_osORDER    = roCmdOPT.m_osORDER;
//m_osPHASE    = roCmdOPT.m_osPHASE;

//m_bBYPASS    = roCmdOPT.m_osBYPASS;
//m_bCSTRAY    = roCmdOPT.m_osCSTRAY;
//m_bFBBYPASS  = roCmdOPT.m_osFBBYPASS;
//m_bINCMODE   = roCmdOPT.m_osINCMODE;
//m_bLUBYPASS  = roCmdOPT.m_osLUBYPASS;
//m_bOPTS      = roCmdOPT.m_osOPTS;
//m_bQUITCONV  = roCmdOPT.m_osQUITCONV;
//m_bRSTRAY    = roCmdOPT.m_osRSTRAY;
//m_bTRACEL    = roCmdOPT.m_osTRACEL;

  bFormat( );

  return( *this );
}

//**************************************************************************************************
// Print the object attributes.
//
// Argument List :
//   rosPrefix - A prefix to every line displayed (usually just spaces)

void  CmdGnuCapOPT::Print( const wxString & rosPrefix )
{
  CmdBase::Print( rosPrefix + wxT("CmdBase::") );

  std::cout << rosPrefix.mb_str( ) << "m_osABSTOL   : " << m_osABSTOL  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osCHGTOL   : " << m_osCHGTOL  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDAMPMAX  : " << m_osDAMPMAX .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDAMPMIN  : " << m_osDAMPMIN .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDEFAD    : " << m_osDEFAD   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDEFAS    : " << m_osDEFAS   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDEFL     : " << m_osDEFL    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDEFW     : " << m_osDEFW    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDTMIN    : " << m_osDTMIN   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osDTRATIO  : " << m_osDTRATIO .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osFLOOR    : " << m_osFLOOR   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osGMIN     : " << m_osGMIN    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osRELTOL   : " << m_osRELTOL  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osROUND    : " << m_osROUND   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osSHORT    : " << m_osSHORT   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTEMP     : " << m_osTEMP    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTNOM     : " << m_osTNOM    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTRREJECT : " << m_osTRREJECT.mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTRSTEPG  : " << m_osTRSTEPG .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTRSTEPH  : " << m_osTRSTEPH .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTRSTEPS  : " << m_osTRSTEPS .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTRTOL    : " << m_osTRTOL   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osVFLOOR   : " << m_osVFLOOR  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osVMAX     : " << m_osVMAX    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osVMIN     : " << m_osVMIN    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osVNTOL    : " << m_osVNTOL   .mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_osDAMPST   : " << m_osDAMPST  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osHARMS    : " << m_osHARMS   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL1     : " << m_osITL1    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL2     : " << m_osITL2    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL3     : " << m_osITL3    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL4     : " << m_osITL4    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL5     : " << m_osITL5    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL6     : " << m_osITL6    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL7     : " << m_osITL7    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITL8     : " << m_osITL8    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osITERMIN  : " << m_osITERMIN .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osNUMDGT   : " << m_osNUMDGT  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osRECURS   : " << m_osRECURS  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osTRANSITS : " << m_osTRANSITS.mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_osMETHOD   : " << m_osMETHOD  .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osMODE     : " << m_osMODE    .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osORDER    : " << m_osORDER   .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osPHASE    : " << m_osPHASE   .mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_bBYPASS    : " << (m_bBYPASS   ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bCSTRAY    : " << (m_bCSTRAY   ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bFBBYPASS  : " << (m_bFBBYPASS ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bINCMODE   : " << (m_bINCMODE  ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bLUBYPASS  : " << (m_bLUBYPASS ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bOPTS      : " << (m_bOPTS     ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bQUITCONV  : " << (m_bQUITCONV ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bRSTRAY    : " << (m_bRSTRAY   ? "true" : "false") << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_bTRACEL    : " << (m_bTRACEL   ? "true" : "false") << '\n';
}

//**************************************************************************************************
//                                          Test Utility                                           *
//**************************************************************************************************

#ifdef TEST_CMDGNUCAPOPT

using  namespace  std;

// Function prototypes

void  Usage( char * psAppName );

//**************************************************************************************************

int  main( int argc, char * argv[ ] )
{
  wxString  osCmd;
  wxString  os1;

  // Validate the argument count passed to the application
  if( argc > 2 )           { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }

  // Process the command line arguments
  os1 = wxConvLibc.cMB2WC( argv[ 1 ] );
  if( argc > 1 )
  {
    if( os1 == wxT("-h") ) { Usage( argv[ 0 ] ); exit( EXIT_SUCCESS ); }
    else                   { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }
  }

  // Display the utility banner
  cout << "\n  Class CmdGnuCapOPT Test Utility"
       << "\n     Version 1.05 (2015-01-03)\n";

  // Create a NG-SPICE OPTIONS command object
  CmdGnuCapOPT  oCmd_OPT;

  // Use the following command example to check the formatter and the parser :
  osCmd << wxT(".OPTIONS ABSTOL=1.20p CHGTOL=12.00f DAMPMAX=900.00m DAMPMIN=600.00m DEFL=120.00u ")
        << wxT("DEFW=120.00u DEFAD=11.00f DEFAS=11.00f DTMIN=1.10p DTRATIO=1.10G FLOOR=1.10E-21 ")
        << wxT("GMIN=1.20p RELTOL=3.00m ROUND=1.0E-10 SHORT=110.00n TEMP=30.00 TNOM=35.00 ")
        << wxT("TRREJECT=600.00m TRSTEPGROW=3.00 TRSTEPHOLD=100.00 TRSTEPSHRINK=9.00 TRTOL=9.00 ")
        << wxT("VFLOOR=1.10f VMAX=40.00 VMIN=-20.00 VNTOL=3.00u DAMPST=1 HARMONICS=10 ITL1=200 ")
        << wxT("ITL2=70 ITL3=10 ITL4=30 ITL5=2 ITL6=2 ITL7=2 ITL8=200 ITERMIN=2 NUMDGT=6 ")
        << wxT("RECURS=10 TRANSITS=3 NOBYP NOCSTRAY NOFBBYP NOINC NOLUBYP NOOPTS QUITCONV RSTRAY ")
        << wxT("NOTRACEL METHOD=EULER MODE=ANALOG ORDER=FORWARD PHASE=RADIANS");

  // Set things up for a formatter test
  oCmd_OPT.m_osABSTOL   = wxT("1.20p");
  oCmd_OPT.m_osCHGTOL   = wxT("12.00f");
  oCmd_OPT.m_osDAMPMAX  = wxT("900.00m");
  oCmd_OPT.m_osDAMPMIN  = wxT("600.00m");
  oCmd_OPT.m_osDEFAD    = wxT("11.00f");
  oCmd_OPT.m_osDEFAS    = wxT("11.00f");
  oCmd_OPT.m_osDEFL     = wxT("120.00u");
  oCmd_OPT.m_osDEFW     = wxT("120.00u");
  oCmd_OPT.m_osDTMIN    = wxT("1.10p");
  oCmd_OPT.m_osDTRATIO  = wxT("1.10Giga");
  oCmd_OPT.m_osFLOOR    = wxT("1.10E-21");
  oCmd_OPT.m_osGMIN     = wxT("1.20p");
  oCmd_OPT.m_osRELTOL   = wxT("3.00m");
  oCmd_OPT.m_osROUND    = wxT("1.0E-10");
  oCmd_OPT.m_osSHORT    = wxT("110.00n");
  oCmd_OPT.m_osTEMP     = wxT("30.00");
  oCmd_OPT.m_osTNOM     = wxT("35.00");
  oCmd_OPT.m_osTRREJECT = wxT("600.00m");
  oCmd_OPT.m_osTRSTEPG  = wxT("3.00");
  oCmd_OPT.m_osTRSTEPH  = wxT("100.00");
  oCmd_OPT.m_osTRSTEPS  = wxT("9.00");
  oCmd_OPT.m_osTRTOL    = wxT("9.00");
  oCmd_OPT.m_osVFLOOR   = wxT("1.10f");
  oCmd_OPT.m_osVMAX     = wxT("40.00");
  oCmd_OPT.m_osVMIN     = wxT("-20.00");
  oCmd_OPT.m_osVNTOL    = wxT("3.00u");

  oCmd_OPT.m_osDAMPST   = wxT("1");
  oCmd_OPT.m_osHARMS    = wxT("10");
  oCmd_OPT.m_osITL1     = wxT("200");
  oCmd_OPT.m_osITL2     = wxT("70");
  oCmd_OPT.m_osITL3     = wxT("10");
  oCmd_OPT.m_osITL4     = wxT("30");
  oCmd_OPT.m_osITL5     = wxT("2");
  oCmd_OPT.m_osITL6     = wxT("2");
  oCmd_OPT.m_osITL7     = wxT("2");
  oCmd_OPT.m_osITL8     = wxT("200");
  oCmd_OPT.m_osITERMIN  = wxT("2");
  oCmd_OPT.m_osNUMDGT   = wxT("6");
  oCmd_OPT.m_osRECURS   = wxT("10");
  oCmd_OPT.m_osTRANSITS = wxT("3");

  oCmd_OPT.m_osMETHOD   = wxT("EULER");
  oCmd_OPT.m_osMODE     = wxT("ANALOG");
  oCmd_OPT.m_osORDER    = wxT("FORWARD");
  oCmd_OPT.m_osPHASE    = wxT("RADIANS");

  oCmd_OPT.m_bBYPASS    = false;
  oCmd_OPT.m_bCSTRAY    = false;
  oCmd_OPT.m_bFBBYPASS  = false;
  oCmd_OPT.m_bINCMODE   = false;
  oCmd_OPT.m_bLUBYPASS  = false;
  oCmd_OPT.m_bOPTS      = false;
  oCmd_OPT.m_bQUITCONV  = true;
  oCmd_OPT.m_bRSTRAY    = true;
  oCmd_OPT.m_bTRACEL    = false;

  cout << "\nRun Formatter     : " << ( oCmd_OPT.bFormat( ) ? "OK" : "FAULT" );
  cout << "\nTest Cmd Format   : " << ( oCmd_OPT == osCmd   ? "OK" : "FAULT" );
  cout << "\nExample Command   : " << osCmd   .mb_str( );
  cout << "\noCmd_OPT Contents : " << oCmd_OPT.mb_str( ) << '\n';

  // Set things up for a parser test
  oCmd_OPT.bSetString( osCmd );
  cout << "\nRun Parser        : " << ( oCmd_OPT.bParse( ) ? "OK" : "FAULT" );
  oCmd_OPT.bFormat( );
  cout << "\nTest Cmd Format   : " << ( oCmd_OPT == osCmd  ? "OK" : "FAULT" );
  cout << "\nExample Command   : " << osCmd   .mb_str( );
  cout << "\noCmd_OPT Contents : " << oCmd_OPT.mb_str( ) << '\n';

  cout << '\n';

  exit( EXIT_SUCCESS );
}

//**************************************************************************************************

void  Usage( char * psAppName )
{
  cout << "\nUsage   : " << psAppName << " [-OPTIONS]"
       << "\nOptions :"
       << "\n  -h : Print usage (this message)\n";
}

#endif // TEST_CMDGNUCAPOPT

//**************************************************************************************************
