//**************************************************************************************************
//                                         CmdGnuCapOP.cpp                                         *
//                                        -----------------                                        *
// Started     : 2008-03-05                                                                        *
// Last Update : 2018-09-25                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "CmdGnuCapOP.hpp"

//**************************************************************************************************
// Constructor.

CmdGnuCapOP::CmdGnuCapOP( void ) : CmdBase( )
{
  bSetDefaults( );
}

//**************************************************************************************************
// Destructor.

CmdGnuCapOP::~CmdGnuCapOP( )
{
}

//**************************************************************************************************
// Check that the object attributes are valid.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOP::bValidate( void )
{
  double  df1, df2, df3;

  CmdBase::bValidate( );

  // Check the sweep start and stop values
  if( ! CnvtType::bStrToFlt( m_osStart, &df1 ) )
    SetErrMsg( wxT("Invalid sweep start value.") );
  if( ! CnvtType::bStrToFlt( m_osStop,  &df2 ) )
    SetErrMsg( wxT("Invalid sweep stop value.") );
  if( df2 < df1 )
    SetErrMsg( wxT("Start value greater than stop value.") );

  // Check the sweep step value and sweep scale
  if( ! CnvtType::bStrToFlt( m_osStep, &df3 ) )
    SetErrMsg( wxT("Invalid sweep step value.") );
  if( df3 <= 0.0 )
    SetErrMsg( wxT("Step value less than or equal to zero.") );
  switch( m_eScale )
  {
    case eSCALE_LIN :
      if( df1!=df2 && df3>fabs( df2 - df1 ) )
        SetErrMsg( wxT("Step value is greater than sweep range.") );
      break;

    case eSCALE_LOG :
      if( df1!=df2 && df3==1.0 )
        SetErrMsg( wxT("Invalid step multiplier, must be non-unity.") );
      break;

    case eSCALE_DEC :
      break;

    default         :
      SetErrMsg( wxT("Invalid step scale value.") );
  }

  return( bIsValid( ) );
}

//**************************************************************************************************
// Set the object attributes to they're default values.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOP::bSetDefaults( void )
{
  CmdBase::bSetDefaults( );

  m_eSimEng  = eSIMR_GNUCAP;
  m_eCmdType = eCMD_OP;

  m_osStart  = wxT("20.0");
  m_osStop   = wxT("30.0");
  m_osStep   = wxT("1.0");

  m_eScale   = eSCALE_LIN;

  return( true );
}

//**************************************************************************************************
// Parse the command string.
//
// Eg.s: .OP 27.0 BASIC
//       .OP 27.0 60.0 BY 1.0 BASIC
//       .OP 27.0 60.0 TI 1.01 BASIC
//       .OP 27.0 60.0 DE 10 BASIC
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOP::bParse( void )
{
  wxStringTokenizer  ostk1;
  wxString           os1;
  int                i1;
  double             df1;

  // Clear the object attributes
  os1 = (wxString &) *this;
  bSetDefaults( );
  assign( os1 );

  // Tokenize the command string
  ostk1.SetString( *this );
  i1 = ostk1.CountTokens( );
  if( i1!=3 && i1!=6 )                                 return( bValidate( ) );

  // Check command type
  os1 = ostk1.GetNextToken( ).Left( 3 ).Upper( );
  if( os1 != wxT(".OP") )                              return( bValidate( ) );

  // Extract the start temperature
  os1 = ostk1.GetNextToken( );
  if( CnvtType::bStrToFlt( os1, &df1 ) )   m_osStart = os1;
  else                                                 return( bValidate( ) );
  if( ostk1.CountTokens( ) > 1 )
  {
    // Extract the stop temperature
    os1 = ostk1.GetNextToken( );
    if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStop = os1;
    else                                               return( bValidate( ) );

    // Extract the sweep type
    os1 = ostk1.GetNextToken( );
    os1.MakeUpper( );
    if(      os1 == wxT("BY") ) m_eScale = eSCALE_LIN;
    else if( os1 == wxT("TI") ) m_eScale = eSCALE_LOG;
    else if( os1 == wxT("DE") ) m_eScale = eSCALE_DEC;
    else                                               return( bValidate( ) );

    // Extract the step size
    os1 = ostk1.GetNextToken( );
    if( CnvtType::bStrToFlt( os1, &df1 ) ) m_osStep = os1;
    else                                               return( bValidate( ) );
  }
  else
  { // Use default values for the remaining parameters
    m_osStop = os1;
    m_osStep = wxT("1.0");
    m_eScale = eSCALE_LIN;
  }

  // Check that the last field is "BASIC"
  if( ostk1.GetNextToken( ).Upper( ) != wxT("BASIC") ) return( bValidate( ) );

  return( bValidate( ) );
}

//**************************************************************************************************
// Format the command string.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  CmdGnuCapOP::bFormat( void )
{
  wxString  osCmd, os1;

  // Set the command name
  osCmd = wxT(".OP");

  // Set sweep parameters
  osCmd << wxT(' ') << m_osStart;
  if( m_osStop != m_osStart )
  {
    osCmd << wxT(' ') << m_osStop;

    switch( m_eScale )
    {
      case eSCALE_LIN : osCmd << wxT(" BY"); break;
      case eSCALE_LOG : osCmd << wxT(" TI"); break;
      case eSCALE_DEC : osCmd << wxT(" DE"); break;
      default         : break;
    }
    osCmd << wxT(' ') << m_osStep;
  }

  // Append format modifier
  osCmd << wxT(" BASIC");

  assign( osCmd );

  return( bValidate( ) );
}

//**************************************************************************************************
// Copy the contents of a CmdNgSpiceDC object.
//
// Argument List :
//   roCmdDC - A reference to a CmdNgSpiceDC object
//
// Return Values :
//   A reference to this object

CmdGnuCapOP & CmdGnuCapOP::operator = ( const CmdNgSpiceDC & roCmdDC )
{
  (CmdBase &) *this = (CmdBase &) roCmdDC;

  m_osStart = roCmdDC.m_osStart;
  m_osStop  = roCmdDC.m_osStop;
  m_osStep  = roCmdDC.m_osStep;
  m_eScale  = eSCALE_LIN;

  bFormat( );

  return( *this );
}

//**************************************************************************************************
// Print the object attributes.
//
// Argument List :
//   rosPrefix - A prefix to every line displayed (usually just spaces)

void  CmdGnuCapOP::Print( const wxString & rosPrefix )
{
  CmdBase::Print( rosPrefix + wxT("CmdBase::") );

  std::cout << rosPrefix.mb_str( ) << "m_osStart : " << m_osStart.mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStop  : " << m_osStop .mb_str( ) << '\n';
  std::cout << rosPrefix.mb_str( ) << "m_osStep  : " << m_osStep .mb_str( ) << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_eScale : ";
  switch( m_eScale )
  {
    case eSCALE_LIN  : std::cout << "eSCALE_LIN\n";  break;
    case eSCALE_LOG  : std::cout << "eSCALE_LOG\n";  break;
    case eSCALE_DEC  : std::cout << "eSCALE_DEC\n";  break;
    case eSCALE_NONE : std::cout << "eSCALE_NONE\n"; break;
    default :          std::cout << "Invalid\n";     break;
  }
}

//**************************************************************************************************
//                                          Test Utility                                           *
//**************************************************************************************************

#ifdef TEST_CMDGNUCAPOP

using  namespace  std;

// Function prototypes

void  Usage( char * psAppName );

//**************************************************************************************************

int  main( int argc, char * argv[ ] )
{
  wxString  osCmd;
  wxString  os1;

  // Validate the argument count passed to the application
  if( argc > 2 )           { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }

  // Process the command line arguments
  os1 = wxConvLibc.cMB2WC( argv[ 1 ] );
  if( argc > 1 )
  {
    if( os1 == wxT("-h") ) { Usage( argv[ 0 ] ); exit( EXIT_SUCCESS ); }
    else                   { Usage( argv[ 0 ] ); exit( EXIT_FAILURE ); }
  }

  // Display the utility banner
  cout << "\n  Class CmdGnuCapOP Test Utility"
       << "\n    Version 1.02 (12/08/2011)\n";

  // Create a GNU-CAP OP command object
  CmdGnuCapOP  tCmd_OP;

  // Use the following command example to check the formatter and the parser :
  osCmd = wxT(".OP 20.00 30.00 TI 1.01 BASIC");
//  osCmd = wxT(".OP 20.00 BASIC");

  // Set things up for a formatter test
  tCmd_OP.m_osStart = wxT("20.00");
  tCmd_OP.m_osStop  = wxT("30.00");
  tCmd_OP.m_osStep  = wxT("1.01");
  tCmd_OP.m_eScale  = eSCALE_LOG;
//  tCmd_OP.m_osStop  = wxT("20.00");
//  tCmd_OP.m_osStep  = wxT("1.00");
//  tCmd_OP.m_eScale  = eSCALE_LIN;
  cout << "\nRun Formatter    : " << ( tCmd_OP.bFormat( ) ? "OK" : "FAULT" );
  cout << "\nTest Cmd Format  : " << ( tCmd_OP == osCmd   ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\ntCmd_OP Contents : " << tCmd_OP.mb_str( ) << '\n';

  // Set things up for a parser test
  tCmd_OP.bSetString( osCmd );
  cout << "\nRun Parser       : " << ( tCmd_OP.bParse( ) ? "OK" : "FAULT" );
  tCmd_OP.bFormat( );
  cout << "\nTest Cmd Format  : " << ( tCmd_OP == osCmd  ? "OK" : "FAULT" );
  cout << "\nExample Command  : " << osCmd  .mb_str( );
  cout << "\ntCmd_OP Contents : " << tCmd_OP.mb_str( ) << '\n';

  cout << '\n';

  exit( EXIT_SUCCESS );
}

//**************************************************************************************************

void  Usage( char * psAppName )
{
  cout << "\nUsage   : " << psAppName << " [-OPTIONS]"
       << "\nOptions :"
       << "\n  -h : Print usage (this message)\n";
}

#endif // TEST_CMDGNUCAPOP

//**************************************************************************************************
