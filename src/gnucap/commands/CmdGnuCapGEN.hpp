//**************************************************************************************************
//                                         CmdGnuCapGEN.hpp                                        *
//                                        ------------------                                       *
// Description : This class contains values associated with the GNU-Cap GENERATOR command. It      *
//               inherits from the class CmdBase.                                                  *
// Started     : 2008-03-11                                                                        *
// Last Update : 2018-11-03                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDGCPGEN_HPP
#define CMDGCPGEN_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "utility/CnvtType.hpp"
#include "netlist/CpntNgSpiceIndSrc.hpp"

class CpntNgSpiceIndSrc;

// Local Macro and/or Constant Declarations

#define  GCP_AMPLITUDE  wxT("1.0")
#define  GCP_OFFSET     wxT("0.0")

#define  GCP_SINFREQ    wxT("0.0k")
#define  GCP_SINPHASE   wxT("0.0")

#define  GCP_PULINITV   wxT("0.0")
#define  GCP_PULMIN     wxT("0.0")
#define  GCP_PULMAX     wxT("0.0")
#define  GCP_PULDELAY   wxT("0.0m")
#define  GCP_PULRISE    wxT("0.0m")
#define  GCP_PULWIDTH   wxT("0.0m")
#define  GCP_PULFALL    wxT("0.0m")
#define  GCP_PULPERIOD  wxT("0.0m")

//**************************************************************************************************

class CmdGnuCapGEN : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    // Overall source characteristics
    wxString  m_osAmplitude;
    wxString  m_osOffset;

    // Sinusoidal source characteristics
    wxString  m_osSinFreq;
    wxString  m_osSinPhase;

    // Pulse source characteristics
    wxString  m_osPulInitV;
    wxString  m_osPulMin;
    wxString  m_osPulMax;
    wxString  m_osPulDelay;
    wxString  m_osPulRise;
    wxString  m_osPulWidth;
    wxString  m_osPulFall;
    wxString  m_osPulPeriod;

          CmdGnuCapGEN( void );
         ~CmdGnuCapGEN( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdGnuCapGEN & operator = ( const CpntNgSpiceIndSrc & roIndSrc );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDGCPGEN_HPP
