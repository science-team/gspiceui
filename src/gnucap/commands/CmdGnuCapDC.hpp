//**************************************************************************************************
//                                         CmdGnuCapDC.hpp                                         *
//                                        -----------------                                        *
// Description : This class contains values associated with the GNU-Cap DC command. It inherits    *
//               from the class CmdBase.                                                           *
// Started     : 2008-01-29                                                                        *
// Last Update : 2018-09-23                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDGCPDC_HPP
#define CMDGCPDC_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "ngspice/commands/CmdNgSpiceDC.hpp"
#include "utility/CnvtType.hpp"

class CmdNgSpiceDC;

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class CmdGnuCapDC : public CmdBase
{
  private :

    bool  bValidate( void ) override;

  public :

    wxString    m_osStart;   // Sweep start value
    wxString    m_osStop;    // Sweep stop  value
    wxString    m_osStep;    // Sweep step  value
    eTypeScale  m_eScale;    // Step scale
    wxString    m_osSource;  // Sweep source
    wxString    m_osTempC;   // Analysis temperature in Celcius

          CmdGnuCapDC( void );
         ~CmdGnuCapDC( );

    bool  bSetDefaults( void ) override;

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    CmdGnuCapDC & operator = ( const CmdNgSpiceDC & roCmdDC );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDGCPDC_HPP
