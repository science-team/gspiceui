//**************************************************************************************************
//                                         StrUtils.hpp                                            *
//                                        --------------                                           *
// Description : This file contains my own string utilities :                                      *
//                - various string compare algorithms                                              *
//                - a function to reduce a string to a single line                                 *
//                - functions to convert between strings and string arrays                         *
// Started     : 2008-05-15                                                                        *
// Last Update : 2016-10-11                                                                        *
// Copyright   : (C) 2008-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef ISTRCMP_HPP
#define ISTRCMP_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "netlist/Component.hpp"

// Function prototypes

      int                iStrCmpNode( const wxString & ros1, const wxString & ros2 );
      int                iStrCmpCpnt( const wxString & ros1, const wxString & ros2 );
      int                iStrCmpSrc ( const wxString & ros1, const wxString & ros2 );
const wxString      &  rosStrToLine ( const wxString & ros1 );
const wxArrayString & roasStrToArr  ( const wxString & ros1 );
const wxString      &  rosArrToStr  ( const wxArrayString & roas1 );

//**************************************************************************************************

#endif // ISTRCMP_HPP
