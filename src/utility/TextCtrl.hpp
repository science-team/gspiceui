//**************************************************************************************************
//                                          TextCtrl.hpp                                           *
//                                         --------------                                          *
// Description : This class extends wxTextCtrl, it adds some useful functionality (eg. increment   *
//               by more than one).                                                                *
// Started     : 2004-06-21                                                                        *
// Last Update : 2015-04-03                                                                        *
// Copyright   : (C) 2004-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef TEXTCTRL_HPP
#define TEXTCTRL_HPP

// Application Includes

#include "TypeDefs.hpp"

// wxWidgets Includes

#include <wx/textctrl.h>
#include <wx/textfile.h>

// Local Constant Declarations

#define  TXT_INITMSG    wxT("Empty")  // Default initialization message
#define  TXT_INITLNS      4           // Totals initial lines including init. msg.
#define  TXT_DISPMIN     10           // Minimum allowable lines in display area
#define  TXT_DISPDEF     15           // Default number of lines in display area
#define  TXT_DISPMAX     50           // Maximum allowable lines in display area
#define  TXT_LNSMIN     100           // Minimum allowable lines in text control
#define  TXT_LNSDEF     1E4           // Default number of lines in text control
#define  TXT_LNSMAX     1E6           // Maximum allowable lines in text control
#define  TXT_VIEWCOLS   122           // The number of columns in display area
#define  TXT_NBKTABHT    40           // Extra height in pixels needed by wxNotebook
#define  TXT_FONTHT      12.7         // Height in pixels of the font

//**************************************************************************************************

class TextCtrl : public wxTextCtrl
{
  private :

            wxString  m_osInitMsg;  // A message to be displayed when the control has been cleared
    static  int       m_iLinesMax;  // Max no. lines the text controls will hold
    static  int       m_iLinesDsp;  // No. of lines in text control display area
            int       m_iLinesCnt;  // No. of lines currently in text control

  public :

                  TextCtrl    ( void );
                 ~TextCtrl    ( );

            bool  bCreate     ( wxWindow * poWin, wxWindowID oWinID=-1 );
            bool  bIsCreated  ( void )        { return( GetParent() != NULL ); }

            bool  bClear      ( void );
            bool  bInitialize ( void );
            bool  bIsEmpty    ( void );

            bool  bSetInitMsg ( const wxString & rosMsg );
    static  bool  bSetLinesMax( int iLines );
    static  bool  bSetLinesDsp( int iLines );

    const   wxString & rosGetInitMsg ( void ) { return( m_osInitMsg ); }
            int          iGetLinesMax( void ) { return( m_iLinesMax ); }
            int          iGetLinesDsp( void ) { return( m_iLinesDsp ); }
            int          iGetLinesCnt( void ) { return( m_iLinesCnt ); }
    static  int          iGetPixelHt ( void )
                                       { return( (int) ((float) m_iLinesDsp * TXT_FONTHT + 0.5 )); }

            bool  bAppendLine ( const wxString & rosLine );
            bool  bAppendFile ( const wxString & roFName );
            bool  bLoadFile   ( const wxString & roFName );
};

//**************************************************************************************************

#endif // TEXTCTRL_HPP
