//**************************************************************************************************
//                                         PnlLblTxt.cpp                                           *
//                                        ---------------                                          *
// Started     : 2014-02-22                                                                        *
// Last Update : 2014-11-28                                                                        *
// Copyright   : (C) 2014-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "PnlLblTxt.hpp"

//**************************************************************************************************
// Implement an event table.

//wxBEGIN_EVENT_TABLE( PnlLblTxt, wxPanel )

//  EVT_CHOICE( ID_TXTCTRL, PnlLblTxt::OnText )

//wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.

PnlLblTxt::PnlLblTxt( void ) : wxPanel( )
{
}

//**************************************************************************************************
// Destructor.

PnlLblTxt::~PnlLblTxt( )
{
}

//**************************************************************************************************
// Layout the display objects.

void  PnlLblTxt::DoLayout( void )
{
  wxBoxSizer * poSzr;
  wxSizerFlags  oFlags;

  poSzr = new wxBoxSizer( wxHORIZONTAL );

  oFlags.Proportion( 1 );
  oFlags.Border( wxTOP, 8 );
  poSzr->Add( &m_oLblName, oFlags );

  oFlags.Proportion( 0 );
  oFlags.Border( wxTOP, 0 );
  poSzr->Add( &m_oTxtCtrl, oFlags );

  // Set the panel sizer and the min. & init. sizes as calculated by the sizer
  SetSizer( poSzr );
  poSzr->SetSizeHints( this );
}

//**************************************************************************************************
// Create an instance of this object.
//
// Argument List :
//   poWin   - The parent window
//   oWinID  - The window identifier
//   iNameWd - The width of the name label   in pixels
//   iTextWd - The width of the text control in pixels
//   roPosn  - The position
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlLblTxt::bCreate( wxWindow * poWin, wxWindowID oWinID, int iNameWd, int iTextWd,
                          const wxPoint & roPosn )
{
  if( bIsCreated( ) )                              return( true );

  // Create the base class (wxPanel)
  if( ! wxPanel::Create( poWin, oWinID, roPosn ) ) return( false );

  // Create the variable name label
  m_oLblName.Create( this, ID_UNUSED, wxT("Unknown"), wxDefaultPosition,
                     wxSize( iNameWd, GUI_CTRL_HT ), wxALIGN_LEFT );

  // Create the text control
  m_oTxtCtrl.Create( this, ID_TXTCTRL, wxT("")      , wxDefaultPosition,
                     wxSize( iTextWd, GUI_CTRL_HT ), wxTE_LEFT );

  // Layout the display objects
  DoLayout( );

  return( true );
}

//**************************************************************************************************
// Set the name label.
//
// Argument List :
//   rosName - The name of the variable
//
// Return Values :
//   Success - true
//   Failure - false

bool  PnlLblTxt::bSetName( const wxString & rosName )
{
  if( ! bIsCreated( ) )    return( false );
  if( rosName.IsEmpty( ) ) return( false );

  m_oLblName.SetLabel( rosName );

  return( true );
}

//**************************************************************************************************
// Set the text control contents.
//
// Argument List :
//   rosText - The text control contents
//
// Return Values :
//   Success - true
//   Failure - false

bool  PnlLblTxt::bSetText( const wxString & rosText )
{
  if( ! bIsCreated( ) ) return( false );

  m_oTxtCtrl.SetValue( rosText );

  return( true );
}

//**************************************************************************************************
// Get the control contents as a string.
//
// Return Values :
//   A reference to the control string contents

const  wxString & PnlLblTxt::rosGetText( void )
{
  static  wxString  os1;

  os1 = m_oTxtCtrl.GetValue( );

  return( os1 );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Choice control an item on the list is selected event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event
/*
void  PnlLblTxt::OnText( wxCommandEvent & roEvtCmd )
{
}
*/
//**************************************************************************************************
