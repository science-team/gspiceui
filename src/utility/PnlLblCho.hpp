//**************************************************************************************************
//                                         PnlLblCho.hpp                                           *
//                                        ---------------                                          *
// Description : This class extends wxPanel, and can substitute for the wxWidgets library class    *
//               wxChoice. It adds a label to the left of a wxChoice control.                      *
// Started     : 2014-02-20                                                                        *
// Last Update : 2015-02-25                                                                        *
// Copyright   : (C) 2014-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLLBLCHO_HPP
#define PNLLBLCHO_HPP

// Application Includes

#include "TypeDefs.hpp"

//**************************************************************************************************

class PnlLblCho : public wxPanel
{
  private :

    void  DoLayout  ( void );

  public :

    wxLabel   m_oLblName;  // The choice control name displayed to the left
    wxChoice  m_oChoice;   // The wxChoice object to contain the items

          PnlLblCho ( void );
         ~PnlLblCho ( );

    bool  bCreate   ( wxWindow * poWin, wxWindowID oWinID, int iNameWd=-1, int iChoiceWd=-1,
                      const wxPoint & roPosn=wxDefaultPosition );
    bool  bIsCreated( void ) { return( GetParent( )!=NULL ? true : false ); }

  private :

    // Event handlers
//    void  OnText( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_CHOICE = 1,

      ID_UNUSED,

      ID_FST = ID_CHOICE,
      ID_LST = ID_CHOICE
    };

    // Leave this as the last line as private access is envoked by macro
//    wxDECLARE_EVENT_TABLE( );
};

//**************************************************************************************************

#endif // PNLLBLCHO_HPP
