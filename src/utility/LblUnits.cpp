//**************************************************************************************************
//                                          LblUnits.cpp                                           *
//                                         --------------                                          *
// Started     : 2014-03-01                                                                        *
// Last Update : 2018-10-19                                                                        *
// Copyright   : (C) 2014-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "LblUnits.hpp"

//**************************************************************************************************
// Constructor.

LblUnits::LblUnits( void )
{
  bClear( );
}

//**************************************************************************************************
// Destructor.

LblUnits::~LblUnits( )
{
}

//**************************************************************************************************
// Create an instance of this object.
//
// Argument List :
//   poWin  - The parent window
//   oWinID - The window identifier
//   iWidth - The width of the wxLabel (wxStaticText) control in pixels
//
// Return Values :
//   true  - Success
//   false - Failure

bool  LblUnits::bCreate( wxWindow * poWin, wxWindowID oWinID, int iWidth )
{
  // Check if the object has already been created
  if( bIsCreated( ) )                           return( true );

  // Create the object
  if( ! wxLabel::Create( poWin, oWinID, wxT(""), wxDefaultPosition, wxSize( iWidth, GUI_CTRL_HT-2 ),
                         wxST_NO_AUTORESIZE ) ) return( false );

  return( true );
}

//**************************************************************************************************
// Clear object attributes.
//
// Return Values :
//   Success - true
//   Failure - false

bool  LblUnits::bClear( void )
{
  m_eUnitsType = eUNITS_NONE;
  m_iUnitsExp  = 0;

  if( bIsCreated( ) ) wxLabel::SetLabel( wxT("") );

  return( true );
}

//**************************************************************************************************
// Set the units type attribute.
//
// Argument List :
//   eUType - the enumerated units type
//
// Return Values :
//   true  - Success
//   false - Failure

bool  LblUnits::bSetUnitsType( eTypeUnits eUType )
{
  wxString  osUnits;
  wxChar    ocPrefix;

  if( ! bIsCreated( ) )                                          return( false );
  if( eUType==m_eUnitsType && !wxLabel::GetLabel( ).IsEmpty( ) ) return( true );

  m_eUnitsType = eUType;

  // Set the units label string if possible
  if( UnitsBase::bGetUnitsType( eUType, osUnits ) )
  {
    // Prepend units prefix if non-zero
    if( m_iUnitsExp != 0 )
      if( CnvtType::bUnitExpToPfx( m_iUnitsExp, &ocPrefix ) )
        osUnits.Prepend( ocPrefix );
  }
  else osUnits = wxT("");

  // Set the units label
  wxLabel::SetLabel( osUnits );

  return( true );
}

//**************************************************************************************************
// Set the units from a string.
//
// Argument List :
//   rosUnits - the units string
//
// Return Values :
//   true  - Success
//   false - Failure

bool  LblUnits::bSetUnits( const wxString & rosUnits )
{
  eTypeUnits  eUType;
  int         iExp;

  if( ! bIsCreated( ) ) return( false );

  if( ! rosUnits.IsEmpty( ) )
  {
    // Set the units label
    wxLabel::SetLabel( rosUnits );

    // Attempt to determine the enumerated units type
    if( bGetUnitsType( rosUnits, &eUType ) )
      m_eUnitsType = eUType;

    // Attempt to determine the units exponent
    if( ! rosUnits.Lower( ).StartsWith( wxT("metre") ) )
      if( CnvtType::bUnitPfxToExp( rosUnits[ 0 ], &iExp ) )
        m_iUnitsExp = iExp;
  }
  else bClear( );

  return( true );
}

//**************************************************************************************************
// Set the units as an exponent.
//
// Argument List :
//   iExp - the units exponent
//
// Return Values :
//   true  - Success
//   false - Failure

bool  LblUnits::bSetUnits( int iExp )
{
  if( iExp<-24 || iExp>24 ) return( false );
  if( iExp%3 != 0 )         return( false );

  m_iUnitsExp = iExp;

  bSetUnitsType( m_eUnitsType );

  return( true );
}

//**************************************************************************************************
// Get the units.
//
// Return Values :
//   Success - The selected units
//   Failure - An empty string

const wxString & LblUnits::rosGetUnits( void )
{
  static  wxString  osUnits;

  osUnits.Empty( );

  if( ! bIsCreated( ) ) return( osUnits );

  osUnits = wxLabel::GetLabel( );

  return( osUnits );
}

//**************************************************************************************************
// Print the object attributes.
//
// Argument List :
//   rosPrefix - A prefix to every line displayed (usually just spaces)

void  LblUnits::Print( const wxString & rosPrefix )
{
  std::cout << rosPrefix.mb_str( ) << "m_eUnitsType : ";
  switch( m_eUnitsType )
  {
    case eUNITS_CAP  : std::cout << "eUNITS_CAP";  break;
    case eUNITS_IND  : std::cout << "eUNITS_IND";  break;
    case eUNITS_RES  : std::cout << "eUNITS_RES";  break;
    case eUNITS_COND : std::cout << "eUNITS_COND"; break;
    case eUNITS_VOLT : std::cout << "eUNITS_VOLT"; break;
    case eUNITS_CURR : std::cout << "eUNITS_CURR"; break;
    case eUNITS_TIME : std::cout << "eUNITS_TIME"; break;
    case eUNITS_FREQ : std::cout << "eUNITS_FREQ"; break;
    case eUNITS_CHRG : std::cout << "eUNITS_CHRG"; break;
    case eUNITS_PHAD : std::cout << "eUNITS_PHAD"; break;
    case eUNITS_PHAR : std::cout << "eUNITS_PHAR"; break;
    case eUNITS_TMPC : std::cout << "eUNITS_TMPC"; break;
    case eUNITS_TMPF : std::cout << "eUNITS_TMPF"; break;
    case eUNITS_PCT  : std::cout << "eUNITS_PCT";  break;
    case eUNITS_EXP  : std::cout << "eUNITS_EXP";  break;

    case eUNITS_NONE : std::cout << "eUNITS_NONE"; break;

    default          : std::cout << "Invalid";
  }
  std::cout << '\n';

  std::cout << rosPrefix.mb_str( ) << "m_iUnitsExp  : " << m_iUnitsExp << '\n';
}

//**************************************************************************************************
