//**************************************************************************************************
//                                         PnlLblTxt.hpp                                           *
//                                        ---------------                                          *
// Description : This class extends wxPanel, and can substitute for the wxWidgets library class    *
//               wxTextCtrl. It adds a label to the left of a single line wxTextCtrl control.      *
// Started     : 2014-02-22                                                                        *
// Last Update : 2014-12-04                                                                        *
// Copyright   : (C) 2014-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLLBLTXT_HPP
#define PNLLBLTXT_HPP

// Application Includes

#include "TypeDefs.hpp"

//**************************************************************************************************

class PnlLblTxt : public wxPanel
{
  private :

    wxLabel     m_oLblName;  // The text control name displayed to the left
    wxTextCtrl  m_oTxtCtrl;  // The wxTextCtrl object to contain the text

    void  DoLayout  ( void );

  public :

          PnlLblTxt ( void );
         ~PnlLblTxt ( );

    bool  bCreate   ( wxWindow * poWin, wxWindowID oWinID, int iNameWd=-1, int iTextWd=-1,
                      const wxPoint & roPosn=wxDefaultPosition );
    bool  bIsCreated( void ) { return( GetParent( )!=NULL ? true : false ); }

    bool  bSetName  ( const wxString & rosName );
    bool  bSetText  ( const wxString & rosText );

    const  wxString & rosGetText( void );

  private :

    // Event handlers
//    void  OnText( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_TXTCTRL = 1,

      ID_UNUSED,

      ID_FST = ID_TXTCTRL,
      ID_LST = ID_TXTCTRL
    };

    // Leave this as the last line as private access is envoked by macro
//    wxDECLARE_EVENT_TABLE( );
};

//**************************************************************************************************

#endif // PNLLBLTXT_HPP
