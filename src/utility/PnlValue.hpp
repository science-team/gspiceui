//**************************************************************************************************
//                                          PnlValue.hpp                                           *
//                                         --------------                                          *
// Description : This class derives from the wxPanel base class. It provides  the functionality    *
//               for displaying and setting the value of  floating point variables  and can        *
//               optionally allow the  setting and processing of units.                            *
// Started     : 2004-09-14                                                                        *
// Last Update : 2018-11-04                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLVALUE_HPP
#define PNLVALUE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/PnlTxtSpn.hpp"
#include "utility/ChoUnits.hpp"
#include "utility/LblUnits.hpp"
#include "utility/CnvtType.hpp"

// wxWidgets Library Includes

#include <wx/spinbutt.h>

//**************************************************************************************************

class PnlValue : public wxPanel
{
  public :

    // Enumerated type to specify if or how the units should be displayed
    enum eTypeShow
    {
      eSHOW_NONE,  // Don't display units at all
      eSHOW_LBL,   // Display the units in a static label control
      eSHOW_CHO    // Display the units in a choice       control
    };

  private :

    // Flag indicating if units should be accommodated
    eTypeShow  m_eShowUnits;

    // Display objects
    wxLabel    m_oLblName;   // The static value name
    PnlTxtSpn  m_oSpnValue;  // Displays the numeric value
    ChoUnits   m_oChoUnits;  // Displayed if the units are    user selectable
    LblUnits   m_oLblUnits;  // Displayed if the units aren't user selectable

    void  DoLayout     ( void );

  public :

          PnlValue     ( void );
         ~PnlValue     ( );

    bool  bCreate      ( wxWindow * poWin, wxWindowID oWinID, int iNameWd, int iValueWd, int iUnitsWd,
                         const wxPoint & roPosn=wxDefaultPosition, eTypeShow eShowUnits=eSHOW_CHO );
    bool  bIsCreated   ( void )                      { return( GetParent( )!=NULL ? true : false ); }

    bool  bClear       ( void );

    bool  bSetName     ( const wxString & rosName );
    bool  bSetValueType( eTypeValue eVType );
    bool  bSetUnitsType( eTypeUnits eUType );
    bool  bSetSpnRange ( double dfMinValue, double dfMaxValue );
    bool  bSetSpnIncSz ( double dfMinIncSz, double dfMaxIncSz=-1.0 );
    bool  bSetDefValue ( double dfValue=NOVAL_DBL )  { return( m_oSpnValue.bSetValDef( dfValue ) ); }
    bool  bSetDefUnits ( const wxString & rosUnits ) { return( m_oChoUnits.bSetDefUnits( rosUnits ) );}

    bool  bSetValue    ( long   liValue, uint uiExp=NOVAL_UINT );
    bool  bSetValue    ( double dfValue, int   iExp=NOVAL_INT  );
    bool  bSetValue    ( const wxString & rosValue );
    bool  bSetUnits    ( const wxString & rosUnits );

    bool  bShowUnits   ( eTypeShow eShowUnits );

    const wxString & rosGetName     ( void );
          long        liGetValue    ( void );
          double      dfGetValue    ( void );
          int          iGetValue    ( void )         { return( (int)   liGetValue( ) ); }
          float        fGetValue    ( void )         { return( (float) dfGetValue( ) ); }
    const wxString & rosGetValue    ( void );
          eTypeUnits   eGetUnitsType( void );
    const wxString & rosGetUnits    ( void );

    void  Print( const wxString & rosPrefix=wxT("  ") );

  private :

    // Event handlers
    void  OnSpnScroll( wxSpinEvent    & roEvtSpn );
    void  OnChoUnits ( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum ePnlItemID
    {
      ID_SPN_VALUE = 0,
      ID_CHO_UNITS,
      ID_LBL_UNITS,

      ID_UNUSED,

      ID_FST = ID_SPN_VALUE,
      ID_LST = ID_LBL_UNITS
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLVALUE_HPP
