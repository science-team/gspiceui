//**************************************************************************************************
//                                         PnlLblCho.cpp                                           *
//                                        ---------------                                          *
// Started     : 2014-02-20                                                                        *
// Last Update : 2015-01-26                                                                        *
// Copyright   : (C) 2014-2016 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "PnlLblCho.hpp"

//**************************************************************************************************
// Implement an event table.

//wxBEGIN_EVENT_TABLE( PnlLblCho, wxPanel )

//  EVT_CHOICE( ID_CHOICE, PnlLblCho::OnChoice )

//wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.

PnlLblCho::PnlLblCho( void ) : wxPanel( )
{
}

//**************************************************************************************************
// Destructor.

PnlLblCho::~PnlLblCho( )
{
}

//**************************************************************************************************
// Layout the display objects.

void  PnlLblCho::DoLayout( void )
{
  wxBoxSizer * poSzr;
  wxSizerFlags  oFlags;

  poSzr = new wxBoxSizer( wxHORIZONTAL );

  oFlags.Proportion( 1 );
  oFlags.Border( wxTOP, 5 );
  poSzr->Add( &m_oLblName, oFlags );

  oFlags.Proportion( 0 );
  oFlags.Border( wxTOP, 0 );
  poSzr->Add( &m_oChoice , oFlags );

  // Set the panel sizer and the min. & init. sizes as calculated by the sizer
  SetSizer( poSzr );
  poSzr->SetSizeHints( this );
}

//**************************************************************************************************
// Create an instance of this object.
//
// Argument List :
//   poWin     - The parent window
//   oWinID    - The window identifier
//   iNameWd   - The width of the name label     in pixels
//   iChoiceWd - The width of the choice control in pixels
//   roPosn    - The position
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlLblCho::bCreate( wxWindow * poWin, wxWindowID oWinID, int iNameWd, int iChoiceWd,
                          const wxPoint & roPosn )
{
  if( bIsCreated( ) )                              return( true );

  // Create the base class (wxPanel)
  if( ! wxPanel::Create( poWin, oWinID, roPosn, wxSize( -1, GUI_CTRL_HT ) ) ) return( false );

  // Create the variable name label
  m_oLblName.Create( this, ID_UNUSED, wxT("Unknown"), wxDefaultPosition,
                     wxSize( iNameWd  , GUI_CTRL_HT ), wxALIGN_LEFT );

  // Create the choice control
  m_oChoice .Create( this, ID_CHOICE,                 wxDefaultPosition,
                     wxSize( iChoiceWd, GUI_CTRL_HT ) );

  // Layout the display objects
  DoLayout( );

  return( true );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Choice control an item on the list is selected event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event
/*
void  PnlLblCho::OnChoice( wxCommandEvent & roEvtCmd )
{
}
*/
//**************************************************************************************************
