//**************************************************************************************************
//                                        AppPrcBase.hpp                                           *
//                                       ----------------                                          *
// Description : This is a wxWidgets application designed to test and debug the PrcBase class.     *
//               (This application also demonstrates how to implement a console only application   *
//               in wxWidgets.)                                                                    *
// Started     : 2018-10-23                                                                        *
// Last Update : 2018-10-23                                                                        *
// Copyright   : (C) 2018 MSWaters                                                                 *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

// wxWidgets includes

#include <wx/cmdline.h>

// Application Includes

#include "Config.hpp"

// This flag indicates debug mode, if it's enabled the app. will produce lots of lovely console spew
bool  g_bDebug=false;

//**************************************************************************************************

class AppPrcBase : public wxApp
{
    PrcBase  m_oPrcBase;

  public :

    void  Usage( void );

    // Event handlers
    virtual  bool  OnInit  ( void );
//  virtual  int   OnRun   ( void );
    virtual  int   MainLoop( void );
//  virtual  int   OnExit  ( void );
};

//**************************************************************************************************
