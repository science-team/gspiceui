//**************************************************************************************************
//                                      AppPrcGNetList.cpp                                         *
//                                     --------------------                                        *
// Started     : 2018-10-23                                                                        *
// Last Update : 2018-10-24                                                                        *
// Copyright   : (C) 2018 MSWaters                                                                 *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "AppPrcGNetList.hpp"

//**************************************************************************************************

// Tell wxWidgets how to create an instance of this application class
wxIMPLEMENT_APP( AppPrcGNetList );

// Define a structure declaring the command line syntax
static  const  wxCmdLineEntryDesc  tCmdLnDesc[] =
{
#if wxCHECK_VERSION( 3,0,0 )
  { wxCMD_LINE_SWITCH, "h", ""    , "", wxCMD_LINE_VAL_NONE  , wxCMD_LINE_OPTION_HELP },
  { wxCMD_LINE_OPTION, "b", "bin" , "", wxCMD_LINE_VAL_STRING, 0                      },
  { wxCMD_LINE_OPTION, "a", "args", "", wxCMD_LINE_VAL_STRING, 0                      },
#else
  { wxCMD_LINE_SWITCH, wxT("h"), wxT("")    , wxT(""), wxCMD_LINE_VAL_NONE  , wxCMD_LINE_OPTION_HELP },
  { wxCMD_LINE_OPTION, wxT("b"), wxT("bin") , wxT(""), wxCMD_LINE_VAL_STRING, 0                      },
  { wxCMD_LINE_OPTION, wxT("a"), wxT("args"), wxT(""), wxCMD_LINE_VAL_STRING, 0                      },
#endif
  { wxCMD_LINE_NONE }
};

//**************************************************************************************************

bool  AppPrcGNetList::OnInit( void )
{
  wxCmdLineParser  oCmdLn;

  oCmdLn.SetDesc( tCmdLnDesc );                                    // Set the command set
  oCmdLn.SetCmdLine( argc, argv );                                 // Set the command line
  if( oCmdLn.Parse( false ) != 0 ) { Usage( ); return( false ); }  // Try to parse the command line
  if( oCmdLn.Found( wxT("h") ) )   { Usage( ); return( false ); }  // Process "-h" switch if found



  return( true );
}

//**************************************************************************************************

int  AppPrcGNetList::MainLoop( )
{
  // Display the utility banner
  std::cout << "\n  PrcGNetList Class Test Utility"
            << "\n     Version 0.20 2018-10-24)\n";

  std::cout << "\noPrcBase.Print( ) :\n";
  m_oPrcGNetList.Print( );

  m_oPrcGNetList.bExecAsync( );

  m_oPrcGNetList.PrintOutput( );

  std::cout << "\n";

  return( EXIT_SUCCESS );
}

//**************************************************************************************************

void  AppPrcGNetList::Usage( void )
{
  std::cout << "\nUsage   : " << GetAppName( ) << " [-OPTIONS] [FILE]"
            << "\nOptions : -h        : Print usage (this message)"
            << "\n\n";
}

//**************************************************************************************************
