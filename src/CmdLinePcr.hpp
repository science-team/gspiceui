//**************************************************************************************************
//                                         CmdLinePcr.hpp                                          *
//                                        ----------------                                         *
// Description : This class does the command line processing.                                      *
// Started     : 2005-02-22                                                                        *
// Last Update : 2018-10-13                                                                        *
// Copyright   : (C) 2005-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDLINEPCR_HPP
#define CMDLINEPCR_HPP

// Application includes

#include "Version.hpp"
#include "TypeDefs.hpp"
#include "process/PrcGNetList.hpp"

// wxWidgets includes

#include <wx/cmdline.h>
#include <wx/filename.h>

//**************************************************************************************************

class CmdLinePcr : wxCmdLineParser
{
  private :

    int            m_iArgC;
    wxChar **      m_ppsArgV;

  public :

    eTypeSimEng    m_eSimEng;
    eTypeEDA       m_eEdaSuite;
    eTypeViewer    m_eDataViewer;
    eTypeCmd       m_eAnalysis;
    wxString       m_osGuileProc;
    wxString       m_osConfigFile;
    wxString       m_osNetLstFile;
    wxArrayString  m_osaSchemFiles;
    bool           m_bCleanCfgFile;

  private :

    bool  bProcOption_a( int * piArg );  // Analysis page specifier
    bool  bProcOption_c( int * piArg );  // Rebuild/clean configuration file
    bool  bProcOption_d( int * piArg );  // Print debug info. to console
    bool  bProcOption_e( int * piArg );  // Specify the EDA tool suite
    bool  bProcOption_g( int * piArg );  // Specify Guile procedure for importing schematic file/s
    bool  bProcOption_h( int * piArg );  // Display usage and exit
    bool  bProcOption_r( int * piArg );  // Specify a configuration file
    bool  bProcOption_s( int * piArg );  // Simulator engine specifier
    bool  bProcOption_v( int * piArg );  // Display app. version and exit
    bool  bProcOption_w( int * piArg );  // Specify the waveform viewer to use

    bool  bCheckFiles  ( int * piArg );  // Check that file names are correct
    bool  bProcSchems  ( int * piArg );  // Schematic file name/s
    bool  bProcNetList ( int * piArg );  // Netlist file name

    void  Usage        ( void );
    void  Version      ( void );
    void  TooManyArgs  ( int iArg );

  public :

          CmdLinePcr( void );
         ~CmdLinePcr( );

    bool  bClear   ( void );
    bool  bSetCmdLn( int iArgC, wxChar ** ppsArgV );
    bool  bProcArgs( void );

    void  Print( const wxString & rosPrefix=wxT("  ") );
};

//**************************************************************************************************

#endif // CMDLINEPCR_HPP
