//**************************************************************************************************
//                                      DlgNgSpiceCfgSrc.hpp                                       *
//                                     ----------------------                                      *
// Description : This dialogue is used to enter values associated with an NG-Spice independent     *
//               voltage source definition. It is used for Transient analysis and is based on a    *
//               pulse function.                                                                   *
// Started     : 2005-05-10                                                                        *
// Last Update : 2018-11-03                                                                        *
// Copyright   : (C) 2005-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef DLGNGSPICECFGSRC_HPP
#define DLGNGSPICECFGSRC_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/PnlValue.hpp"
#include "netlist/CpntNgSpiceIndSrc.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class DlgNgSpiceCfgSrc : public wxDialog
{
  private :

    // Value panel controls for a sinusoidal source
    PnlValue  m_oPnlSinAmp;
    PnlValue  m_oPnlSinFreq;
    PnlValue  m_oPnlSinOffset;
    PnlValue  m_oPnlSinDelay;
    PnlValue  m_oPnlSinDamp;

    // Value panel controls for a pulse source
    PnlValue  m_oPnlPulInitV;
    PnlValue  m_oPnlPulValue;
    PnlValue  m_oPnlPulDelay;
    PnlValue  m_oPnlPulRise;
    PnlValue  m_oPnlPulWidth;
    PnlValue  m_oPnlPulFall;
    PnlValue  m_oPnlPulPeriod;

    // Button controls
    wxButton  m_oBtnOk;
    wxButton  m_oBtnClear;
    wxButton  m_oBtnCancel;

    // Storage for validated Independent Source values
    CpntNgSpiceIndSrc  m_oCpntIndSrc;

    // Object initialization functions
    void  Initialize( void );
    void  Create    ( void );
    void  ToolTips  ( void );
    void  DoLayout  ( void );

    PnlValue * poGetPanel( int iPnlID );

    void  SetValues( CpntNgSpiceIndSrc & roCpntSrc );
    void  GetValues( CpntNgSpiceIndSrc & roCpntSrc );

  public :

    explicit  DlgNgSpiceCfgSrc( wxWindow * poWin );
             ~DlgNgSpiceCfgSrc( );

    bool  bClear( void );

    bool  bSetValues( CpntNgSpiceIndSrc & roCpntSrc );
    bool  bGetValues( CpntNgSpiceIndSrc & roCpntSrc );

          bool         bSetValue( int iPnlID, const wxString & rosValue );
    const wxString & rosGetValue( int iPnlID );

    bool  bSetName( wxString & rosName );

    // Event handlers
    void  OnBtnOk    ( wxCommandEvent & roEvtCmd );
    void  OnBtnClear ( wxCommandEvent & roEvtCmd );
    void  OnBtnCancel( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum eDlgItemID
    {
      ID_PNL_SIN_AMP = 0,
      ID_PNL_SIN_FREQ,
      ID_PNL_SIN_OFFSET,
      ID_PNL_SIN_DELAY,
      ID_PNL_SIN_DAMP,
      ID_PNL_PUL_INITV,
      ID_PNL_PUL_VALUE,
      ID_PNL_PUL_DELAY,
      ID_PNL_PUL_RISE,
      ID_PNL_PUL_WIDTH,
      ID_PNL_PUL_FALL,
      ID_PNL_PUL_PERIOD,

      ID_BTN_OK,
      ID_BTN_CLEAR,
      ID_BTN_CANCEL,

      ID_UNUSED,      // Assigned to controls for which events are not used

      ID_PNL_FST = ID_PNL_SIN_AMP,
      ID_PNL_LST = ID_PNL_PUL_PERIOD,

      ID_FST = ID_PNL_SIN_AMP,
      ID_LST = ID_BTN_CANCEL
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // DLGNGSPICECFGSRC_HPP
