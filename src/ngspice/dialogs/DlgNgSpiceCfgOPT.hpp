//**************************************************************************************************
//                                      DlgNgSpiceCfgOPT.hpp                                       *
//                                     ----------------------                                      *
// Description : This dialogue is used to enter values associated with the NG-Spice OPTIONS        *
//               command.                                                                          *
// Started     : 2006-03-03                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2006-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef DLGNGSPICECFGOPT_HPP
#define DLGNGSPICECFGOPT_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "utility/PnlValue.hpp"
#include "utility/PnlLblCho.hpp"
#include "ngspice/commands/CmdNgSpiceOPT.hpp"
#include "base/NbkSimEngBase.hpp"

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class DlgNgSpiceCfgOPT : public wxDialog
{
  private :

    // Value panel controls (for float values)
    PnlValue       m_oPnlABSTOL;
    PnlValue       m_oPnlCHGTOL;
    PnlValue       m_oPnlCONVABSS;
    PnlValue       m_oPnlCONVSTEP;
    PnlValue       m_oPnlDEFL;
    PnlValue       m_oPnlDEFW;
    PnlValue       m_oPnlDEFAD;
    PnlValue       m_oPnlDEFAS;
    PnlValue       m_oPnlGMIN;
    PnlValue       m_oPnlPIVREL;
    PnlValue       m_oPnlPIVTOL;
    PnlValue       m_oPnlRAMPTIME;
    PnlValue       m_oPnlRELTOL;
    PnlValue       m_oPnlRSERIES;
    PnlValue       m_oPnlRSHUNT;
    PnlValue       m_oPnlTEMP;
    PnlValue       m_oPnlTNOM;
    PnlValue       m_oPnlTRTOL;
    PnlValue       m_oPnlVNTOL;

    // Value panel controls (for integer values)
    PnlValue       m_oPnlGMINSTEP;
    PnlValue       m_oPnlITL1;
    PnlValue       m_oPnlITL2;
    PnlValue       m_oPnlITL3;
    PnlValue       m_oPnlITL4;
    PnlValue       m_oPnlITL5;
    PnlValue       m_oPnlITL6;
    PnlValue       m_oPnlMAXEVIT;
    PnlValue       m_oPnlMAXOPALT;
    PnlValue       m_oPnlMAXORD;
    PnlValue       m_oPnlMAXWARNS;
    PnlValue       m_oPnlSRCSTEPS;

    // Value panel controls (for string values)
    PnlLblCho      m_oChoMETHOD;

    // Value panel controls (for boolean values)
    wxCheckBox     m_oCbxBADMOS3;
    wxCheckBox     m_oCbxINTERP;
    wxCheckBox     m_oCbxNOOPAC;
    wxCheckBox     m_oCbxNOOPALT;
    wxCheckBox     m_oCbxOPTS;
    wxCheckBox     m_oCbxTRYTOCOM;
    wxCheckBox     m_oCbxWARN;

    // Button controls
    wxButton       m_oBtnOk;
    wxButton       m_oBtnCancel;
    wxButton       m_oBtnDefaults;

    // Storage for validated OPTION values
    CmdNgSpiceOPT  m_oCmdOPT;

    // Object initialization functions
    void  Initialize( void );
    void  Create    ( void );
    void  ToolTips  ( void );
    void  DoLayout  ( void );

    PnlValue * poGetPanel( int iPnlID );

    void  SetValues( CmdNgSpiceOPT & roCmdOPT );
    void  GetValues( CmdNgSpiceOPT & roCmdOPT );

  public :

    explicit  DlgNgSpiceCfgOPT( wxWindow * poWin );
             ~DlgNgSpiceCfgOPT( );

    bool  bClear( void );

    bool  bSetValues( CmdNgSpiceOPT & roCmdOPT );
    bool  bGetValues( CmdNgSpiceOPT & roCmdOPT );

          bool         bSetValue( int iPnlID, const wxString & rosValue );
    const wxString & rosGetValue( int iPnlID );

    // Event handlers
    void  OnBtnOk      ( wxCommandEvent & roEvtCmd );
    void  OnBtnDefaults( wxCommandEvent & roEvtCmd );
    void  OnBtnCancel  ( wxCommandEvent & roEvtCmd );

    // In order to be able to react to a menu command, it must be given a
    // unique identifier such as a const or an enum.
    enum eDlgItemID
    {
      ID_PNL_ABSTOL = 0,
      ID_PNL_CHGTOL  ,
      ID_PNL_CONVABSS,
      ID_PNL_CONVSTEP,
      ID_PNL_DEFL    ,
      ID_PNL_DEFW    ,
      ID_PNL_DEFAD   ,
      ID_PNL_DEFAS   ,
      ID_PNL_GMIN    ,
      ID_PNL_GMINSTEP,
      ID_PNL_ITL1    ,
      ID_PNL_ITL2    ,
      ID_PNL_ITL3    ,
      ID_PNL_ITL4    ,
      ID_PNL_ITL5    ,
      ID_PNL_ITL6    ,
      ID_PNL_MAXEVIT ,
      ID_PNL_MAXOPALT,
      ID_PNL_MAXORD  ,
      ID_PNL_MAXWARNS,
      ID_PNL_PIVREL  ,
      ID_PNL_PIVTOL  ,
      ID_PNL_RAMPTIME,
      ID_PNL_RELTOL  ,
      ID_PNL_RSERIES ,
      ID_PNL_RSHUNT  ,
      ID_PNL_SRCSTEPS,
      ID_PNL_TEMP    ,
      ID_PNL_TNOM    ,
      ID_PNL_TRTOL   ,
      ID_PNL_VNTOL   ,

      ID_CHO_METHOD  ,

      ID_CBX_BADMOS3 ,
      ID_CBX_INTERP  ,
      ID_CBX_NOOPAC  ,
      ID_CBX_NOOPALT ,
      ID_CBX_OPTS    ,
      ID_CBX_TRYTOCOM,
      ID_CBX_WARN    ,

      ID_BTN_OK      ,
      ID_BTN_DEFAULTS,
      ID_BTN_CANCEL  ,

      ID_UNUSED      ,  // Assigned to controls for which events are not used

      ID_FST = ID_PNL_ABSTOL,
      ID_LST = ID_BTN_CANCEL
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // DLGNGSPICECFGOPT_HPP
