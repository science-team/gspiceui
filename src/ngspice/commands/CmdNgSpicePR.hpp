//**************************************************************************************************
//                                        CmdNgSpicePR.hpp                                         *
//                                       ------------------                                        *
// Description : This class contains values associated with the NG-Spice PRINT command. It         *
//               inherits from the class CmdBase.                                                  *
// Started     : 2007-10-16                                                                        *
// Last Update : 2018-09-25                                                                        *
// Copyright   : (C) 2007-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CMDNGSPR_HPP
#define CMDNGSPR_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/CmdBase.hpp"
#include "netlist/NetList.hpp"
#include "gnucap/commands/CmdGnuCapPR.hpp"

class CmdGnuCapPR;

// wxWidgets Includes

#include <wx/tokenzr.h>

//**************************************************************************************************

class CmdNgSpicePR : public CmdBase
{
  private :

         eTypeCmd  m_eAnaType;

    bool  bValidate( void ) override;

  public :

    wxArrayString  m_osaNodes;
    wxArrayString  m_osaCpnts;
             bool  m_bParams[ ePARAM_NONE ];
             bool  m_bCpxPts[ eCPXPT_NONE ];

          CmdNgSpicePR( void );
          CmdNgSpicePR( const CmdNgSpicePR & roCmdPR );
         ~CmdNgSpicePR( );

    bool  bSetDefaults( void ) override;
    bool  bSetAnaType ( eTypeCmd eAnaType );

         eTypeCmd  eGetAnaType( void ) const { return( m_eAnaType ); }

    bool  bParse ( void ) override;
    bool  bFormat( void ) override;

    const wxString & rosGetParamLst( void );
             int       iGetParamCnt( void );

    CmdNgSpicePR & operator = ( const CmdNgSpicePR & roCmdPR );
    CmdNgSpicePR & operator = ( const CmdGnuCapPR  & roCmdPR );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CMDNGSPR_HPP
