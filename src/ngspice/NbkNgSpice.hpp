//**************************************************************************************************
//                                         NbkNgSpice.hpp                                          *
//                                        ----------------                                         *
// Description : Provides a container class for the analysis configuration panels for the          *
//               different analyses provided by NG-Spice electronic circuit simulator.             *
// Started     : 2004-05-08                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef NBKNGSPICE_HPP
#define NBKNGSPICE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/NbkSimEngBase.hpp"
#include "ngspice/dialogs/DlgNgSpiceCfgOPT.hpp"
#include "ngspice/panels/PnlNgSpiceOP.hpp"
#include "ngspice/panels/PnlNgSpiceDC.hpp"
#include "ngspice/panels/PnlNgSpiceAC.hpp"
#include "ngspice/panels/PnlNgSpiceTR.hpp"

//**************************************************************************************************

class NbkNgSpice : public NbkSimEngBase
{
  private :

    // Analysis pages
    PnlNgSpiceOP * m_poPnlNgSpiceOP;
    PnlNgSpiceDC * m_poPnlNgSpiceDC;
    PnlNgSpiceAC * m_poPnlNgSpiceAC;
    PnlNgSpiceTR * m_poPnlNgSpiceTR;
//  PnlNgSpiceFO * m_poPnlNgSpiceFO;

    // OPTIONS line setup dialog
    DlgNgSpiceCfgOPT   m_oDlgCfgOPT;

  public :

          NbkNgSpice( wxWindow * poParent, wxWindowID oWinID );
         ~NbkNgSpice( );

    bool  bClear( void ) override;

    bool  bLoad( const SimnBase & roSimn ) override;
    bool  bSave(       SimnBase & roSimn ) override;

    bool  bSetPage(       eTypeCmd     eAnalysis ) override;
    bool  bSetPage( const wxString & rosAnalysis ) override;

    const  wxString & rosGetPage( void ) override;
           eTypeCmd     eGetPage( void ) override;

    // Event handlers
    void  OnPageChangd( wxNotebookEvent & roEvtNbk );
    void  OnBtnOptions( wxCommandEvent  & roEvtCmd );

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // NBKNGSPICE_HPP
