//**************************************************************************************************
//                                        PnlNgSpiceOP.cpp                                         *
//                                       ------------------                                        *
// Started     : 2015-01-12                                                                        *
// Last Update : 2018-11-01                                                                        *
// Copyright   : (C) 2015-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "PnlNgSpiceOP.hpp"

//**************************************************************************************************
// Implement an event table.

wxBEGIN_EVENT_TABLE( PnlNgSpiceOP, PnlAnaBase )

  EVT_CHOICE( PnlAnaBase::ID_CHO_SRCNAME, PnlNgSpiceOP::OnSrcName )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.
//
// Argunebt List:
//   poWin - A pointer to the parent window

PnlNgSpiceOP::PnlNgSpiceOP( wxWindow * poWin ) : PnlAnaBase( poWin )
{
  bSetAnalysType( eCMD_OP );  // Set the analysis panel type
  Create( );                  // Create the analysis panel
  bClear( );                  // Clear all object attributes
}

//**************************************************************************************************
// Destructor.

PnlNgSpiceOP::~PnlNgSpiceOP( )
{
}

//**************************************************************************************************
// Create the display objects.

void  PnlNgSpiceOP::Create( void )
{
  PnlAnaBase::CreateBase( );    // Create the base controls
  PnlAnaBase::CreateScale( );   // Create the scale controls
  PnlAnaBase::CreateSigSrc( );  // Create the input signal source controls
              CreateSigSrc( );  // Create additional input signal source controls

#ifdef LAYOUT_MNGR
  PnlAnaBase::DoLayout( );      // Layout the panel's GUI objects
#endif // LAYOUT_MNGR

  // The following block of code is required to circumvent a bug in wxWidgets. All declared wxChoice
  // controls attempt a clear( ) operation in their destructor whether or not thay have actually
  // been created. Causes a null pointer warning. ??? 2018-10-20
  PnlAnaBase::CreateTemp( );
#ifdef LAYOUT_MNGR
  m_oPnlTemp.GetParent( )->Hide( );
#else
  m_oSbxTemp.Hide( );
#endif
  m_oPnlTemp.Hide( );

  // Set the sweep parameter labels
#ifdef LAYOUT_MNGR
  bSetSwpParsLbl( wxT("Operating Point Sweep") );
#else
  m_oSbxSwpPars.SetLabel( wxT(" Operating Point Sweep ") );
#endif // LAYOUT_MNGR
  m_oPnlStart  .bSetName( wxT("Start Temperature") );
  m_oPnlStop   .bSetName( wxT("Stop Temperature") );

  // Display the PnlValue units as a label
  m_oPnlStart.bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlStop .bShowUnits( PnlValue::eSHOW_LBL );
  m_oPnlStep .bShowUnits( PnlValue::eSHOW_LBL );

  // Set sweep parameter units
  m_oPnlStart.bSetUnitsType( eUNITS_TMPC );
  m_oPnlStop .bSetUnitsType( eUNITS_TMPC );
  m_oPnlStep .bSetUnitsType( eUNITS_TMPC );

  // Layout the PnlValue controls
  m_oPnlStart.Layout( );
  m_oPnlStop .Layout( );
  m_oPnlStep .Layout( );

  // Disable the scale options NG-Spice doesn't support
  m_oRbxSweep.Enable( eSCALE_LOG, false );
  m_oRbxSweep.Enable( eSCALE_DEC, false );
  m_oRbxSweep.Enable( eSCALE_OCT, false );

  // Disable the checkboxes for the parameters NG-Spice cannot calculate
  m_oCbxCurrent.Disable( );
  m_oCbxPower  .Disable( );
  m_oCbxResist .Disable( );
}

//**************************************************************************************************
// Create the source component display objects.

void  PnlNgSpiceOP::CreateSigSrc( void )
{
  wxPanel * poPnl;

  poPnl = static_cast< wxPanel * >( m_oChoSrcName.GetParent( ) );

  // Create and add signal source controls
#ifdef LAYOUT_MNGR
  m_oPnlSrcLvl.bCreate( poPnl, ID_PNL_SRCLVL, -1, 95, 82 );
#else
  m_oSbxSigSrc.SetSize( wxSize( 330, 60 ) );
  m_oPnlSrcLvl.bCreate( poPnl, ID_PNL_SRCLVL, -1, 95, 82, wxPoint( 142, 200 ) );
#endif // LAYOUT_MNGR
}

//**************************************************************************************************
// Clear the object attributes.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlNgSpiceOP::bClear( void )
{
  // Clear the base class
  PnlAnaBase::bClear( );

  // Set the step scale type and default sweep values
  m_oPnlStart.bSetValue( (float)   0.0 );
  m_oPnlStop .bSetValue( (float) 100.0 );
  m_oPnlStep .bSetValue( (float)  10.0 );

  // Set default scale value
  bSetScale( eSCALE_LIN );

  // Set input source default values
  m_oChoSrcName.Clear( );
  m_oChoSrcName.Append( wxT("None") );
  m_oChoSrcName.SetSelection( 0 );
  m_oPnlSrcLvl.bSetValue( (float) 0.0 );
  m_oPnlSrcLvl.bSetUnitsType( eUNITS_NONE );

  // Set parameters check box default values
  m_oCbxVoltage.SetValue( true );
  m_oCbxCurrent.SetValue( false );
  m_oCbxPower  .SetValue( false );
  m_oCbxResist .SetValue( false );

  return( true );
}

//**************************************************************************************************
// Load information from a simulation object.
//
// Argument List :
//   roSimn - A SimnGnuCap object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlNgSpiceOP::bLoad( SimnNgSpice & roSimn )
{
  bool  bRtn=true;

  // Load the components into the signal source choice box
  PnlAnaBase::LoadSrcNames( roSimn.m_oaCpnts, wxT("VIRLC") );

  // Go no further if the OP command isn't valid
  if( ! roSimn.m_oCmdDC.bIsValid( ) )                        return( false );
  if( roSimn.m_oCmdDC.m_osSource.Upper( ) != wxT("TEMP") )   return( false );

  // Set the source component (a sweep source is not mandatory for an OP analysis)
  PnlAnaBase::bSetSrcCpnt( roSimn.m_oCpntSigSrc );

  // Set the sweep values
  if( ! m_oPnlStart.bSetValue( roSimn.m_oCmdDC.m_osStart ) ) bRtn = false;
  if( ! m_oPnlStop .bSetValue( roSimn.m_oCmdDC.m_osStop  ) ) bRtn = false;
  if( ! m_oPnlStep .bSetValue( roSimn.m_oCmdDC.m_osStep  ) ) bRtn = false;

  return( bRtn );
}

//**************************************************************************************************
// Save information to a simulation object.
//
// Argument List :
//   roSimn - A SimnGnuCap object
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PnlNgSpiceOP::bSave( SimnNgSpice & roSimn )
{
  wxString  os1;

  m_osErrMsg.Empty( );

  // Set the analysis type
  roSimn.m_oCmdPR.bSetAnaType( eCMD_OP );

  // Set the sweep values
  roSimn.m_oCmdDC.m_osStart = m_oPnlStart.rosGetValue( );
  roSimn.m_oCmdDC.m_osStop  = m_oPnlStop .rosGetValue( );
  roSimn.m_oCmdDC.m_osStep  = m_oPnlStep .rosGetValue( );

  // Set the sweep type to temperature
  roSimn.m_oCmdDC.m_osSource = wxT("TEMP");

  // Set the sweep source (a sweep source is not compulsory for a OP analysis)
  if( m_oChoSrcName.GetStringSelection( ) != wxT("None") )
  {
    os1 = m_oChoSrcName.GetStringSelection( );
    roSimn.m_oCpntSigSrc = roSimn.NetList::roGetCpnt( os1 );
    os1 = wxT("DC ") + m_oPnlSrcLvl.rosGetValue( );
    if( roSimn.m_oCpntSigSrc.eGetType( ) == eCPNT_IVS ) os1 += wxT('V');
    if( roSimn.m_oCpntSigSrc.eGetType( ) == eCPNT_ICS ) os1 += wxT('A');
    roSimn.m_oCpntSigSrc.bSetValue( os1 );
  }
  else roSimn.m_oCpntSigSrc.bClear( );

  // Store the parameters to derive
  roSimn.m_oCmdPR.m_bParams[ ePARAM_VLT ] = m_oCbxVoltage.GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_CUR ] = m_oCbxCurrent.GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_PWR ] = m_oCbxPower  .GetValue( );
  roSimn.m_oCmdPR.m_bParams[ ePARAM_RES ] = m_oCbxResist .GetValue( );

  // Create the command strings
  roSimn.m_oCmdDC.bFormat( );
  roSimn.m_oCmdPR.bFormat( );

  // Check for errors
  if( ! roSimn.m_oCmdDC.bIsValid( ) ) SetErrMsg( roSimn.m_oCmdDC.rosGetErrMsg( ) );
  if( ! roSimn.m_oCmdPR.bIsValid( ) ) SetErrMsg( roSimn.m_oCmdPR.rosGetErrMsg( ) );

  return( bIsOk( ) );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// Source component choice box event handler.
//
// Argument List :
//   roEvtCmd - An object holding information about the event

void  PnlNgSpiceOP::OnSrcName( wxCommandEvent & roEvtCmd )
{
  wxString  os1;

  if( m_oChoSrcName.GetStringSelection( ) != wxT("None") )
  {
    // Set the units type
    os1 = m_oChoSrcName.GetStringSelection( );
    m_oPnlSrcLvl.bSetUnitsType( Component::eGetUnitsType( os1 ) );

    // Set the source value
    if( m_oPnlSrcLvl.dfGetValue( ) == 0.0 )
      m_oPnlSrcLvl.bSetValue( (double) 1.0 );
  }
  else
  {
    m_oPnlSrcLvl.bSetUnitsType( eUNITS_NONE );
    m_oPnlSrcLvl.bSetValue( (double) 0.0 );
  }
}

//**************************************************************************************************
