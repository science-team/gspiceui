//**************************************************************************************************
//                                        PnlNgSpiceTR.hpp                                         *
//                                       ------------------                                        *
// Description : This class derives from the PnlAnaBase base class and provides a GUI for the user *
//               to configure a Transient Analysis for the NG-Spice electronic circuit simulation  *
//               engine.                                                                           *
// Started     : 2004-08-05                                                                        *
// Last Update : 2018-10-22                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PNLNGSPICETR_HPP
#define PNLNGSPICETR_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "Config.hpp"
#include "base/PnlAnaBase.hpp"
#include "netlist/SimnNgSpice.hpp"
#include "ngspice/dialogs/DlgNgSpiceCfgSrc.hpp"

//**************************************************************************************************

class PnlNgSpiceTR : public PnlAnaBase
{
  private :

    // Signal source configuration controls
    DlgNgSpiceCfgSrc  m_oDlgCfgSrc;

    void  Create      ( void );
    void  CreateSigSrc( void ) override;

  public :

    explicit  PnlNgSpiceTR( wxWindow * poWin );
             ~PnlNgSpiceTR( );

    bool  bClear( void ) override;

    bool  bLoad( SimnNgSpice & roSimn );
    bool  bSave( SimnNgSpice & roSimn );

    // Event handlers
    void  OnSrcName ( wxCommandEvent & roEvtCmd );
    void  OnBtnSetup( wxCommandEvent & roEvtCmd );

    // Declare friend classes
    friend  class  NbkNgSpice;

    // Unique display control identifiers
    enum ePnlItemID
    {
      ID_BTN_SETUP = PnlAnaBase::ID_LST+1,

      ID_UNUSED,

      ID_FST = ID_BTN_SETUP,
      ID_LST = ID_BTN_SETUP
    };

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // PNLNGSPICETR_HPP
