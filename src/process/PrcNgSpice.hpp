//**************************************************************************************************
//                                         PrcNgSpice.hpp                                          *
//                                        ----------------                                         *
// Description : This class provides the interface to the NG-Spice circuit simulator.              *
// Started     : 2004-05-07                                                                        *
// Last Update : 2018-09-24                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCNGSPICE_HPP
#define PRCNGSPICE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PrcSimEngBase.hpp"
#include "netlist/SimnGnuCap.hpp"
#include "ngspice/commands/CmdNgSpiceDC.hpp"
#include "ngspice/commands/CmdNgSpiceAC.hpp"
#include "ngspice/commands/CmdNgSpiceTR.hpp"
#include "ngspice/commands/CmdNgSpicePR.hpp"
#include "utility/CnvtType.hpp"

//**************************************************************************************************

class PrcNgSpice : public PrcSimEngBase
{
  private :

    // Storage for the result file column labels
    wxString  m_osColLbls;

    // Function to match a component to a set of node labels
    bool  bMatchCpnt( SimnNgSpice & roSimn, wxString & rosToNodes );

    // Functions to create the simulator commands
    void  Convert2Nodes( wxArrayString & rosaCpnts, SimnNgSpice & roSimn );

    // Create the WIDTH command
    wxString & rosMakeCmdWIDTH( wxString & rosCmdPR );

    // Format the results file
    bool  bFmtGeneral  ( void );
    bool  bFmtColLabels( void );
    bool  bFmtDataLines( void );
    bool  bFmtPhaseData( void ); // ??? (02/02/2006) Not yet implemented

  public :

                   PrcNgSpice( void );
                  ~PrcNgSpice( );

    // Create the process argument list
    virtual  bool  bMakeArgLst( SimnBase & roSimn ) override;

    // Format the results file
    virtual  bool  bFmtResults( void ) override;
};

//**************************************************************************************************

#endif // PRCNGSPICE_HPP
