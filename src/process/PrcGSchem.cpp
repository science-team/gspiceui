//**************************************************************************************************
//                                          PrcGSchem.cpp                                          *
//                                         ---------------                                         *
// Started     : 2008-02-18                                                                        *
// Last Update : 2018-10-26                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "PrcGSchem.hpp"

//**************************************************************************************************
// Constructor.

PrcGSchem::PrcGSchem( void ) : PrcBase( wxPROCESS_REDIRECT )
{
  // Initialize the object attributes
  m_oaNameSchems.Clear( );

  // Attempt to set and find the binary file
  bSetEdaSuite( g_oConfig.eGetEdaSuite( ) );
}

//**************************************************************************************************
// Destructor.

PrcGSchem::~PrcGSchem( )
{
}

//**************************************************************************************************
// Set the EDA tool suite..
//
// Argument List :
//   eEDA - The enumerated type specifier for the desired tool suite
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PrcGSchem::bSetEdaSuite( eTypeEDA eEDA )
{
  bool  bRtn;

  switch( eEDA )
  {
    case eEDA_LEPTON : bRtn = bSetBinFile( BIN_LSCHEM ); break;
    case eEDA_GEDA   : bRtn = bSetBinFile( BIN_GSCHEM ); break;
    default          : bRtn = FALSE;
  }

  return( bRtn );
}

//**************************************************************************************************
// Set the schematic file name/s.
//
// Argument List:
//   rosFileNames - A string containing the full path and file name/s
//
// Return Values:
//   true  - Success
//   false - Failure

bool  PrcGSchem::bSetSchemFiles( const wxString & rosFileNames )
{
  wxStringTokenizer  ostk1;
  wxArrayString      oas1;

  ostk1.SetString( rosFileNames );
  while( ostk1.HasMoreTokens( ) )
    oas1.Add( ostk1.GetNextToken( ) );

  return( bSetSchemFiles( oas1 ) );
}

//**************************************************************************************************
// Set the schematic file name/s.
//
// Argument List:
//   roasFileNames - A string array containing the full path and file name/s
//
// Return Values:
//   true  - Success
//   false - Failure

bool  PrcGSchem::bSetSchemFiles( const wxArrayString & roasFileNames )
{
  wxFileName  ofn1;
  size_t      szt1;

  // Clear the current list of schematic files
  m_oaNameSchems.Clear( );

  // Check the argument is empty
  if( roasFileNames.IsEmpty( ) )                              return( true );

  // Add the new schematic file name/s to the list
  for( szt1=0; szt1<roasFileNames.GetCount( ); szt1++ )
  {
    ofn1 = roasFileNames.Item( szt1 );
    if( ofn1.GetPath( ).IsEmpty( ) ) ofn1.SetPath( wxT(".") );

    if( ! ofn1.IsOk( )       ) continue;
    if( ! ofn1.FileExists( ) ) continue;

    m_oaNameSchems.Add( ofn1 );
  }

  // Check that at least one schematic file name was accepted
  if( m_oaNameSchems.IsEmpty( ) )                               return( false );

  // Set the log file path
  ofn1 = roGetLogFile( );
  ofn1.SetPath( m_oaNameSchems.Item( 0 ).GetPath( ) );
  bSetLogFile( ofn1.GetFullPath( ) );

  // Check if any of the schematic files were invalid
  if( m_oaNameSchems.GetCount( ) != roasFileNames.GetCount( ) ) return( false );

  return( true );
}

//**************************************************************************************************
// Get the EDA tool suite.
//
// Return Values :
//   Success - The enumerate type for the EDA tool suite
//   Failure - eEDA_NONE

eTypeEDA  PrcGSchem::eGetEdaSuite( void )
{
  wxString  os1;

  os1 = roGetBinFile( ).GetFullName( );

  if( os1 == BIN_LSCHEM ) return( eEDA_LEPTON );
  if( os1 == BIN_GSCHEM ) return( eEDA_GEDA   );
  else                    return( eEDA_NONE   );
}

//**************************************************************************************************
// Get a schematic file from the list of schematic files.
//
// Return Values:
//   Success - The procedure name
//   Failure - An empty file name

const wxString & PrcGSchem::rosGetSchemFiles( void )
{
  static  wxString  osSchems;
  size_t  sz1;

  osSchems.Empty( );

  for( sz1=0; sz1<m_oaNameSchems.GetCount( ); sz1++ )
  {
    if( ! osSchems.IsEmpty( ) ) osSchems << wxT(' ');
    osSchems << m_oaNameSchems.Item( sz1 ).GetFullPath( );
  }

  return( osSchems );
}

//**************************************************************************************************
// Edit the schematic file/s.
//
// (Eg. using the following : gschem ../sch/test-amp1.sch)
//
// Return Values :
//   true  - Success
//   false - Failure

bool  PrcGSchem::bExec( void )
{
  wxString  osArgLst;
  size_t    sz1;

  // Test file names needed by this function
  if( ! bBinExists( ) )          return( false );
  if( rosGetSchemFiles( ).IsEmpty( ) )
  {
    PrcBase::SetErrMsg( wxT("The schematic file/s have not been set.") );
    return( false );
  }

  // Disable backup files in gschem eg. circuit.sch~
  osArgLst << wxT("-c \"(auto-save-interval 0)\" ");

#ifndef __WXMSW__
  // Don't display the status windows at startup
  osArgLst << wxT("-c \"(view-status)\" ");

  // Don't display the side bar at startup
  osArgLst << wxT("-c \"(view-sidebar)\" ");

  // Use the light colour scheme
  osArgLst << wxT("-c \"(view-light-colors)\"");
#endif

  // Append input file name/s to argument list
  for( sz1=0; sz1<m_oaNameSchems.GetCount( ); sz1++ )
    osArgLst << wxT(" ") << rosEscSpaceChrs( m_oaNameSchems.Item( sz1 ).GetFullPath( ) );

  // Set the command line arguments
  if( ! bSetArgLst( osArgLst ) ) return( false );

#if defined( __WXMSW__ ) && defined( GSPICEUI_DEBUG )
  // Set the execute flags
  m_iFlags = wxEXEC_SHOW_CONSOLE;
#endif

  // Execute the process
  if( ! PrcBase::bExecAsync( ) ) return( false );

  return( true );
}

//**************************************************************************************************
