//**************************************************************************************************
//                                        NbkSimEngBase.cpp                                        *
//                                       -------------------                                       *
// Started     : 2004-05-05                                                                        *
// Last Update : 2018-09-27                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#include "NbkSimEngBase.hpp"

//**************************************************************************************************
// Allocate storage for static data members.

Component  NbkSimEngBase::m_oCpntSigSrc;

//**************************************************************************************************
// Implement an event table.

wxBEGIN_EVENT_TABLE( NbkSimEngBase, wxNotebook )

  EVT_NOTEBOOK_PAGE_CHANGED( -1, NbkSimEngBase::OnPageChangd )

wxEND_EVENT_TABLE( )

//**************************************************************************************************
// Constructor.
//
// Arguments:
//   poParent - The parent window
//   oWinID   - The display object ID
//   roPosn   - The notebook position
//   roSize   - The notebook size

NbkSimEngBase::NbkSimEngBase( wxWindow * poParent, wxWindowID oWinID )
                            : wxNotebook( poParent, oWinID )
{
  // Set the simulator engine type specifier
  m_eSimEng = eSIMR_NONE;
}

//**************************************************************************************************
// Default constructor.
// (Used for two stage creation ie. must call Create( ) method.)

NbkSimEngBase::NbkSimEngBase( void ) : wxNotebook( )
{
  m_eSimEng = eSIMR_NONE;
}

//**************************************************************************************************
// Destructor.

NbkSimEngBase::~NbkSimEngBase( )
{
}

//**************************************************************************************************
// Clear the object attributes.
//
// Return Values :
//   true  - Success
//   false - Failure

bool  NbkSimEngBase::bClear( void )
{
  m_osErrMsg.Empty( ); // Clear the error string

  return( true );
}

//**************************************************************************************************
//                                         Event Handlers                                          *
//**************************************************************************************************
// This event is generated when a notebook page has just changed.
//
// Argument List:
//   roEvtNbk - An object holding information about the event

void  NbkSimEngBase::OnPageChangd( wxNotebookEvent & roEvtNbk )
{
}
//**************************************************************************************************
