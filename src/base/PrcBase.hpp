//**************************************************************************************************
//                                           PrcBase.hpp                                           *
//                                          -------------                                          *
// Description : Provides a wrapper for the wxProcess class adding a bit of extra desirable        *
//               functionality.                                                                    *
// Started     : 2004-01-29                                                                        *
// Last Update : 2018-10-26                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCBASE_HPP
#define PRCBASE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "utility/TextCtrl.hpp"
#include "utility/StrUtils.hpp"

// wxWidgets Includes

#include <wx/filename.h>
#include <wx/process.h>
#include <wx/utils.h>

// Local Constant Declarations

#define DEF_LOG_FILE  wxT("./prcbase.log")  // Default log file path and name

//**************************************************************************************************

class PrcBase : protected wxProcess
{
  private :

    wxFileName    m_oNameBin;   // The file name of the binary to be executed
    wxFileName    m_oNameLog;   // The log file name for the textual process output
    wxString      m_osArgLst;   // The argument list appended to the binary name
    int           m_iFlags;     // Flags which may be used in the function wxExecute( )
#if wxCHECK_VERSION( 3,0,0 )
    wxExecuteEnv  m_oEnv;       // Structure containing working directory and environment variables
#endif
    wxString      m_osErrMsg;   // Error message
    int           m_iPid;       // The process ID number
    int           m_iExitCode;  // The process exit code

  protected :

    bool  bFindBinFile( wxFileName & roFileBin );
    bool  bLogOutput  ( void );

  public :

    explicit  PrcBase( int iFlags=wxPROCESS_REDIRECT );
             ~PrcBase( );

    bool  bBinExists( void );

    bool  bIsExec( void ) { return( m_iPid>0 ? true : false ); }
    bool  bIsOk  ( void ) { return( m_iExitCode==0 && m_osErrMsg.IsEmpty( ) ); }

    bool  bSetBinFile( const wxString & rosFileName );
    bool  bSetArgLst ( const wxString & rosArgLst   );
    bool  bSetCwd    ( const wxString & rosCwd      );
    bool  bSetLogFile( const wxString & rosFileName );

    void  SetErrMsg( const wxString & rosErrMsg ) { if(m_osErrMsg.IsEmpty()) m_osErrMsg=rosErrMsg; }
    void  ClrErrMsg( void )                       { m_osErrMsg.Empty( ); }

    const wxFileName &  roGetBinFile ( void ) { return( m_oNameBin  ); }
    const wxString   & rosGetArgLst  ( void ) { return( m_osArgLst  ); }
#if wxCHECK_VERSION( 3,0,0 )
    const wxString   & rosGetCwd     ( void ) { return( m_oEnv.cwd  ); }
#else
    const wxString   & rosGetCwd     ( void ) { static wxString osCwd; osCwd=wxGetCwd(); return(osCwd); }
#endif
    const wxFileName &  roGetLogFile ( void ) { return( m_oNameLog  ); }
    const wxString   & rosGetErrMsg  ( void ) { return( m_osErrMsg  ); }
          int            iGetExitCode( void ) { return( m_iExitCode ); }

    const wxString & rosEscSpaceChrs( const wxString & rosPath );

    virtual  bool  bExecAsync( void );
    virtual  bool  bExecSync ( void );
    virtual  bool  bKill     ( void );

    bool  bDelLogFile( void );

    void  PrintIO ( TextCtrl & roTxtCtl );
    void  PrintCmd( TextCtrl & roTxtCtl );
    void  PrintRsp( TextCtrl & roTxtCtl );

    // The following function is intended for debugging
    virtual  void  Print( const wxString & rosPrefix=wxT("  ") );
             void  PrintOutput( void );

    // Event handlers
    void  OnTerminate( int iPid, int iStatus );
};

//**************************************************************************************************

#endif // PRCBASE_HPP
