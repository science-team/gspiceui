//**************************************************************************************************
//                                        PrcSimEngBase.hpp                                        *
//                                       -------------------                                       *
// Description : This is the base class for classes which provide the interface to the SPICE       *
//               circuit simulator.                                                                *
// Started     : 2004-04-25                                                                        *
// Last Update : 2017-05-23                                                                        *
// Copyright   : (C) 2004-2017 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef PRCSIMENGBASE_HPP
#define PRCSIMENGBASE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/PrcBase.hpp"
#include "base/SimnBase.hpp"
#include "netlist/NetList.hpp"

//**************************************************************************************************

class PrcSimEngBase : public PrcBase
{
  protected :

    eTypeSimEng  m_eSimEng;       // Simulation engine specifier
    wxFileName   m_oNameResults;  // The file name to contain the process output
    wxTextFile   m_oFileResults;  // A actual text file object to contain the process output

//  bool  bErrorCheck( const wxString & rosFileName );  // ??? Not yet implemented

  public :

          PrcSimEngBase( void );
         ~PrcSimEngBase( );

    bool  bSetResultsFile( const wxString & rosFileName );

          eTypeSimEng   eGetSimEng     ( void ) { return( m_eSimEng      ); }
    const wxFileName & roGetResultsFile( void ) { return( m_oNameResults ); }

    // Create the process argument list
    virtual  bool  bMakeArgLst( SimnBase & roSimn );

    // Execute the simulation
    virtual  bool  bExec( void );

    // Format the results file
    virtual  bool  bFmtResults( void );

    // The following function is intended for debugging
    void  Print( const wxString & rosPrefix=wxT("  ") );
};

//**************************************************************************************************

#endif // PRCSIMENGBASE_HPP
