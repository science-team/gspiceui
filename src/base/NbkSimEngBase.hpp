//**************************************************************************************************
//                                        NbkSimEngBase.hpp                                        *
//                                       -------------------                                       *
// Description : Base class for all simulator notebooks. These notebook are intended to contain    *
//               the individual analysis panels.                                                   *
// Started     : 2004-05-05                                                                        *
// Last Update : 2018-10-08                                                                        *
// Copyright   : (C) 2004-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef NBKSIMENGBASE_HPP
#define NBKSIMENGBASE_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "netlist/NetList.hpp"
#include "base/SimnBase.hpp"
#include "base/PnlAnaBase.hpp"

// wxWidgets Includes

#include <wx/notebook.h>

// Local Constant Declarations

#ifndef LAYOUT_MNGR
  #define  NBKSIMENG_WIDTH   572
  #define  NBKSIMENG_HEIGHT  272
#endif // LAYOUT_MNGR

//**************************************************************************************************

class NbkSimEngBase : public wxNotebook
{
  protected :

    wxString     m_osErrMsg;  // Error message
    eTypeSimEng  m_eSimEng;   // Attribute to identify the simulation engine type

    // The signal source name and value (used only for signal source synch'ing)
    static  Component  m_oCpntSigSrc;

  public :

                   NbkSimEngBase( wxWindow * poParent, wxWindowID oWinID );
                   NbkSimEngBase( void );
                  ~NbkSimEngBase( );

    virtual  bool  bClear( void );
             bool  bIsOk ( void )                         { return( m_osErrMsg.IsEmpty( ) ); }

    virtual  bool  bLoad( const SimnBase & roSimn ) = 0;
    virtual  bool  bSave(       SimnBase & roSimn ) = 0;

    virtual  bool  bSetPage(       eTypeCmd     eAnalysis ) = 0;
    virtual  bool  bSetPage( const wxString & rosAnalysis ) = 0;

    virtual  const  wxString & rosGetPage( void ) = 0;
    virtual         eTypeCmd     eGetPage( void ) = 0;

           eTypeSimEng  eGetSimEng( void ) { return( m_eSimEng  ); }

    const  wxString & rosGetErrMsg( void ) { return( m_osErrMsg ); }
           void          SetErrMsg( const wxString & rosErrMsg )
                                                          { if( bIsOk( ) ) m_osErrMsg = rosErrMsg; }

    // Event handlers
    void  OnPageChangd( wxNotebookEvent & roEvtNbk );
    void  OnBtnOptions( wxCommandEvent  & roEvtCmd );

    // Leave this as the last line as private access is envoked by macro
#if wxCHECK_VERSION( 3,0,0 )
    wxDECLARE_EVENT_TABLE( );
#else
    wxDECLARE_EVENT_TABLE( )
#endif
};

//**************************************************************************************************

#endif // NBKSIMENGBASE_HPP
