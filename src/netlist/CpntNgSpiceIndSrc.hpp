//**************************************************************************************************
//                                      CpntNgSpiceIndSrc.hpp                                      *
//                                     -----------------------                                     *
// Description : This class represents a component definition line for an NG-Spice independent     *
//               voltage or current source.                                                        *
// Started     : 2008-06-12                                                                        *
// Last Update : 2018-10-03                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CPNTNGSPICEINDSRC_HPP
#define CPNTNGSPICEINDSRC_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "netlist/Component.hpp"
#include "gnucap/commands/CmdGnuCapGEN.hpp"

class CmdGnuCapGEN;

// wxWidgets Includes

#include <wx/tokenzr.h>

// Local Constant Declarations

#define  NGS_SINAMP     wxT("0.0")
#define  NGS_SINOFFSET  wxT("0.0")
#define  NGS_SINFREQ    wxT("0.0k")
#define  NGS_SINDELAY   wxT("0.0m")
#define  NGS_SINDAMP    wxT("0.0")

#define  NGS_PULINITV   wxT("0.0")
#define  NGS_PULVALUE   wxT("0.0")
#define  NGS_PULDELAY   wxT("0.0m")
#define  NGS_PULRISE    wxT("0.0m")
#define  NGS_PULWIDTH   wxT("0.0m")
#define  NGS_PULFALL    wxT("0.0m")
#define  NGS_PULPERIOD  wxT("0.0m")

//**************************************************************************************************

class CpntNgSpiceIndSrc : public Component
{
  private :

    bool  bParseValue ( void ) override;
    bool  bFormatValue( void ) override;
    bool  bValidate   ( void ) override;

    void  ClrValues( void );

  public :

    // Value for a sinusoidal source
    wxString  m_osSinOffset;
    wxString  m_osSinAmp;
    wxString  m_osSinFreq;
    wxString  m_osSinDelay;
    wxString  m_osSinDamp;

    // Value for a pulse source
    wxString  m_osPulInitV;
    wxString  m_osPulValue;
    wxString  m_osPulDelay;
    wxString  m_osPulRise;
    wxString  m_osPulWidth;
    wxString  m_osPulFall;
    wxString  m_osPulPeriod;

          CpntNgSpiceIndSrc( void );
         ~CpntNgSpiceIndSrc( );

    bool  bClear( void ) override;

    CpntNgSpiceIndSrc & operator = ( const Component    & roCpnt   );
    CpntNgSpiceIndSrc & operator = ( const CmdGnuCapGEN & roCmdGEN );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // CPNTNGSPICEINDSRC_HPP
