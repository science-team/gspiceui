//**************************************************************************************************
//                                         SimnGnuCap.hpp                                          *
//                                        ----------------                                         *
// Description : A class to contain the values required to define a GNU-Cap simulation.            *
// Started     : 2008-05-07                                                                        *
// Last Update : 2018-09-24                                                                        *
// Copyright   : (C) 2008-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef SIMNGNUCAP_HPP
#define SIMNGNUCAP_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "base/SimnBase.hpp"
#include "gnucap/commands/CmdGnuCapOPT.hpp"
#include "gnucap/commands/CmdGnuCapOP.hpp"
#include "gnucap/commands/CmdGnuCapDC.hpp"
#include "gnucap/commands/CmdGnuCapAC.hpp"
#include "gnucap/commands/CmdGnuCapTR.hpp"
#include "gnucap/commands/CmdGnuCapFO.hpp"
#include "gnucap/commands/CmdGnuCapPR.hpp"
#include "gnucap/commands/CmdGnuCapGEN.hpp"
#include "netlist/SimnNgSpice.hpp"

class SimnNgSpice;

//**************************************************************************************************

class SimnGnuCap : public SimnBase
{
  public :

    CmdGnuCapOP   m_oCmdOP;
    CmdGnuCapDC   m_oCmdDC;
    CmdGnuCapAC   m_oCmdAC;
    CmdGnuCapTR   m_oCmdTR;
    CmdGnuCapFO   m_oCmdFO;

    CmdGnuCapOPT  m_oCmdOPT;
    CmdGnuCapPR   m_oCmdPR;
    CmdGnuCapGEN  m_oCmdGEN;

  private :

    // Function to extract information from the circuit description
    bool  bLoadSimEng ( void ) override;
    bool  bLoadSimCmds( void ) override;
    bool  bLoadSigSrc ( void ) override;

  public :

          SimnGnuCap( void );
         ~SimnGnuCap( );

    bool  bClear     ( void ) override;
    bool  bClrCmds   ( void ) override;
    bool  bClrTstPts ( void ) override;

    bool  bValidate  ( void ) override;

    bool  bLoad      ( void ) override;
    bool  bSave      ( void ) override;

    bool  bSetAnaType( eTypeCmd eAnaType ) override { return( m_oCmdPR.bSetAnaType( eAnaType ) ); }

    bool  bAddTstNode( const wxString & rosName ) override;
    bool  bAddTstCpnt( const wxString & rosName ) override;

          eTypeCmd           eGetAnaType ( void ) const override { return( m_oCmdPR.eGetAnaType());}
    const wxArrayString & rosaGetTstNodes( void ) const override { return( m_oCmdPR.m_osaNodes ); }
    const wxArrayString & rosaGetTstCpnts( void ) const override { return( m_oCmdPR.m_osaCpnts ); }

    SimnGnuCap & operator = ( const SimnNgSpice & roSimn );

    void  Print( const wxString & rosPrefix=wxT("  ") ) override;
};

//**************************************************************************************************

#endif // SIMNGNUCAP_HPP
