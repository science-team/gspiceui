//**************************************************************************************************
//                                          Version.hpp                                            *
//                                         -------------                                           *
// Description : Version specifier for the GNU Spice GUI application.                              *
// Started     : 2003-08-15                                                                        *
// Last Update : Refer below                                                                       *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef VERSION_HPP
#define VERSION_HPP

#define  APP_NAME       wxT("gSpiceUI")
#define  APP_VERSION    wxT("1.2.36")
#define  APP_DATE       wxT("2018-11-16")
#define  APP_COPYRIGHT  wxT("Copyright (C) MSWaters 2003-2018")

#endif // VERSION_HPP

//**************************************************************************************************
// Things To Do : (Refer also to the ToDo file)
//
// 2018-11-18 If the schematic file is updated and the sweep parameters are changed when a
//            simulation is run only the schematic changes are included in the simulation file.
// 2018-11-16 When changing the simulation engine the data viewer utility should be closed since the
//            simulation engines use different files to store simulation results.
// 2018-11-04 Sample images of gSpiceUI on SourceForge need to be updated.
// 2018-10-30 Had several instances where following long simulation runs the simulation engine has
//            spontaneously changed from GNU-Cap to NG-Spice. Seems to happen when gSpiceUI locks up
//            and is terminates un-naturally.
// 2017-06-05 On a long simulation run, the GUI isn't redrawn when covered. It ends up blank.
// 2015-02-20 All the schematics in sch/demos should work, many don't. Which ones?
// 2015-02-12 Create a MOSFET demo. using the ARF446.
// 2015-02-10 Close all bug reports.
//**************************************************************************************************
// NG-Spice :
//
// 2010-03-31 NG-Spice seems to be broken for JFETs.
// 2009-10-06 NUMDGT should set the number digits to print when printing tables of data. Doesn't
//            work in batch mode.
// 2007-05-11 PRINT statement parser is broken. Eg. PRINT TRAN V(0,1) V(0,2) is interpreted as one
//            parameter -V(1)-V(2) instead of two; in  contrast PRINT TRAN V(1,0) V(2,0) is
//            interpreted correctly. In  addition eg. PRINT AC VDB(0,1) won't run at all.
//            Notes : The .PRINT statement parsing starts in the function fixem( char * )
//                    (src/frontend/dotcards.c) which calls gettoks( char * ) which calls
//                    gettok( char ** ) (src/misc/string.c). I think the bug is in gettok( char ** )
//                    or how it is used by gettoks( char * ).
//**************************************************************************************************
// GNU-Cap :
//
// 2017-05-27 GNU-Cap doesn't like paths containing space characters eg. :
//              gnucap -b /home/msw/tmp/Google\ Drive/Documents/Electronics/bjt-amp-ce-1.ckt
// 2010-03-22 GNU-Cap seems to be broken for JFETs.
// 2009-08-21 For GNU-Cap AC analysis where a NPN BJT is used, get an error message before each line
//            of the results (eg. "open circuit: internal node 2").
// 2005-04-21 At times the first result line from a DC analysis is clearly incorrect.
// 2005-04-20 In the AC analysis the following source line is not interpreted correctly :
//            "Vin 2 3 GENERATOR(1) AC 100.00m". The AC magnitude "100.00m" is not interpreted as
//            "0.1"; replace it with "0.1" and everything works OK.
// 2004-03-29 The PRINT command doesn't produce a label for the independent parameter ie.
//            temperature for OP, voltage for DC.
//**************************************************************************************************
// gnetlist :
//
// 2005-07-19 If a relative model file path specified in schematic file the path is regarded as
//            relative to the CWD not the schematic file path which would be more logical.
//**************************************************************************************************
// Gaw :
//
// 2016-03-28 Gaw seems to break if a decreasing DC sweep is performed.
//**************************************************************************************************
// Gwave :
//
// 2016-04-06 When looking for the gwave binary look for "gwave2" and then "gwave".
// 2014-04-15 Can no longer compile the sources owing to Guile dependencies.
// 2004-03-29 GWave can't handle the banner that GNU-Cap includes in it's output.
//**************************************************************************************************
// wxWidgets Library Bugs :
//
// 2017-05-14  v3.0.3  Doesn't handle file paths containing space characters, they aren't escaped
//                     when sent to the system command line.
// 2007-09-07  v2.8.5  Cannot set the font in the choice control in the PnlValue class when no file
//                     loaded at startup. It should inherit the font from it's parent; every other
//                     control does. This seems to be bug in wxWidgets v2.8.5 and v2.6.3.
// 2004-08-13  v2.4.2  In class wxFileDialog styles can only be set in the constructor. The method
//                     SetStyle( ) does nothing.
// 2003-11-17  v2.4.1  In class wxFileDialog wildcards can only be set in the constructor. The
//                     method SetWildcard( ) is not properly implemented. See constructor in
//                     src/generic/filedlgg.cpp and set method in include/wx/generic/filedlgg.h.
//**************************************************************************************************
