//**************************************************************************************************
//                                        App_gSpiceUI.hpp                                         *
//                                       ------------------                                        *
// Description : The main application class. It all begins here.                                   *
// Started     : 2003-08-15                                                                        *
// Last Update : 2018-10-23                                                                        *
// Copyright   : (C) 2003-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef APP_GSPICEUI_HPP
#define APP_GSPICEUI_HPP

// Application includes

#include "CmdLinePcr.hpp"
#include "Config.hpp"
#include "main/FrmMain.hpp"

// This flag indicates debug mode, if it's enabled the app. will produce lots of lovely console spew
bool    g_bDebug;

// This object is the global interface to the configuration file
Config  g_oConfig;

//**************************************************************************************************

class App_gSpiceUI : public wxApp
{
  private :

    wxLocale    m_oLocale;
    CmdLinePcr  m_oCmdLn;
    FrmMain   * m_poFrmMain;

    void  UpdateCfg( void );

  public :

          App_gSpiceUI( void );
         ~App_gSpiceUI( );

    // Event handlers
    virtual  bool  OnInit  ( void );
//  virtual  int   OnRun   ( void );
    virtual  int   OnExit  ( void );

    // Leave this as the last line as private access is envoked by this macro
//  wxDECLARE_EVENT_TABLE( );
};

//**************************************************************************************************

#endif // APP_GSPICEUI_HPP
