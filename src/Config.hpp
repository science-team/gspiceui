//**************************************************************************************************
//                                          Config.hpp                                             *
//                                         ------------                                            *
// Description : This class is an application specific interface to the wxWidget wxConfig object.  *
// Started     : 2016-09-25                                                                        *
// Last Update : 2018-10-28                                                                        *
// Copyright   : (C) 2016-2018 MSWaters                                                            *
//**************************************************************************************************

//**************************************************************************************************
//                                                                                                 *
//      This program is free software; you can redistribute it and/or modify it under the          *
//      terms of the GNU General Public License as published by the Free Software Foundation;      *
//      either version 3 of the License, or (at your option) any later version.                    *
//                                                                                                 *
//**************************************************************************************************

#ifndef CONFIG_HPP
#define CONFIG_HPP

// Application Includes

#include "TypeDefs.hpp"
#include "CmdLinePcr.hpp"
#include "utility/PnlTxtSpn.hpp"
#include "utility/StrUtils.hpp"

// wxWidgets Includes

#include <wx/config.h>

// Local constant declarations

#define  CFG_DEF_TMPFILEMGT   eTFM_DELETE
#define  CFG_DEF_FRMLAYOUT    0
#define  CFG_DEF_PRECISION    CNVT_DEF_FLT_RES
#define  CFG_DEF_NBKMAXLNS    TXT_LNSDEF
#define  CFG_DEF_SPNPERIOD    SPN_PERIOD_DEF
#define  CFG_DEF_TOOLTIPS     false
#define  CFG_DEF_TOOLTIPDLY   700
#define  CFG_DEF_KEEPNETLST   false
#define  CFG_DEF_MAINPOSNX    100
#define  CFG_DEF_MAINPOSNY    100
#define  CFG_DEF_MAINSIZEW    793
#define  CFG_DEF_MAINSIZEH    623

#define  CFG_DEF_HELPPOSNX    500
#define  CFG_DEF_HELPPOSNY    1
#define  CFG_DEF_HELPSIZEW    500
#define  CFG_DEF_HELPSIZEH    700

#define  CFG_DEF_VERBOSEMODE  false
#define  CFG_DEF_GUILEPROC    wxT("spice-sdb")
#define  CFG_DEF_INCLUDEMODE  false
#define  CFG_DEF_EMBEDMODE    false
#define  CFG_DEF_NOMUNGEMODE  false

#define  CFG_DEF_SIMENG       wxT("NG-Spice")
#define  CFG_DEF_ANALYSIS     wxT("OP")
#define  CFG_DEF_PHASEUNITS   eUNITS_PHAD
#define  CFG_DEF_SYNCSIGSRCS  true
#define  CFG_DEF_SYNCTEMPS    true
#define  CFG_DEF_AUTOREGEN    false

#define  CFG_DEF_EDASUITE     wxT("Lepton-EDA")

#define  CFG_DEF_DATAVIEWER   wxT("Gaw")

//**************************************************************************************************

class Config
{
  private :

            wxFileName  m_oFileName;  // The RC file name
    static  wxConfig *  m_poCfg;      // Pointer to the wxWidgets configuration object

          bool         bSet( const wxString & rosGroup, const wxString & rosKey, int  iValue );
          bool         bSet( const wxString & rosGroup, const wxString & rosKey, bool bValue );
          bool         bSet( const wxString & rosGroup, const wxString & rosKey,
                             const wxString & rosValue );

          int          iGet( const wxString & rosGroup, const wxString & rosKey, int  iDefault );
          bool         bGet( const wxString & rosGroup, const wxString & rosKey, bool bDefault );
    const wxString & rosGet( const wxString & rosGroup, const wxString & rosKey,
                             const wxString & rosDefault );

  public :

          Config( void );
         ~Config( );

    bool  bOpen  ( const wxString & rosFileName=wxEmptyString );
    bool  bIsOpen( void ) { return( m_poCfg != NULL ); }
    bool  bClose ( void );
    bool  bFlush ( void );
    bool  bClean ( void );

          bool         bSetFileName( const wxString & rosFileName );
    const wxString & rosGetFileName( void );

    // Set configuration functions
    bool  bSetFrmLayout  ( uint             uiLayout       );  // MAIN       group
    bool  bSetTmpFileMgt ( eTypeTmpFileMgt  eTmpFileMgt    );
    bool  bSetPrecision  ( uint             uiFltRes       );
    bool  bSetNbkMaxLns  ( uint             uiLnCnt        );
    bool  bSetSpnPeriod  ( uint             ui_mSec        );
    bool  bSetToolTips   ( bool             bEnable        );
    bool  bSetToolTipDly ( uint             ui_mSec        );
    bool  bSetKeepNetLst ( bool             bEnable        );
    bool  bSetMainPosnX  ( int              iPosnX         );
    bool  bSetMainPosnY  ( int              iPosnY         );
    bool  bSetMainSizeW  ( int              iSizeW         );
    bool  bSetMainSizeH  ( int              iSizeH         );

    bool  bSetHelpPosnX  ( int              iPosnX         );  // HELP       group
    bool  bSetHelpPosnY  ( int              iPosnY         );
    bool  bSetHelpSizeW  ( int              iSizeW         );
    bool  bSetHelpSizeH  ( int              iSizeH         );

    bool  bSetNetLstFile ( const wxString      &  rosFile  );  // FILES      group
    bool  bSetSchemFiles ( const wxString      &  rosFiles );
    bool  bSetSchemFiles ( const wxArrayString & roasFiles );
    bool  bSetRecentFiles( const wxString      &  rosFiles );
    bool  bSetRecentFiles( const wxArrayString & roasFiles );
    bool  bSetDirLastAcc ( const wxString      &  rosDir   );

    bool  bSetVerboseMode( bool             bEnable        );  // GNETLIST   group
    bool  bSetGuileProc  ( const wxString & rosGuileProc   );
    bool  bSetIncludeMode( bool             bEnable        );
    bool  bSetEmbedMode  ( bool             bEnable        );
    bool  bSetNoMungeMode( bool             bEnable        );

    bool  bSetSimEng     ( const wxString & rosSimEng      );  // SIMULATOR  group
    bool  bSetSimEng     ( eTypeSimEng      eSimEng        );
    bool  bSetAnalysis   ( eTypeCmd         eAnalysis      );
    bool  bSetPhaseUnits ( eTypeUnits       eUnits         );
    bool  bSetSyncSigSrcs( bool             bEnable        );
    bool  bSetSyncTemps  ( bool             bEnable        );
    bool  bSetAutoRegen  ( bool             bEnable        );

    bool  bSetEdaSuite   ( const wxString & rosEdaSuite    );  // EDA_SUITE  group
    bool  bSetEdaSuite   ( eTypeEDA           eEdaSuite    );

    bool  bSetDataViewer ( const wxString & rosDataViewer  );  // DATAVIEWER group
    bool  bSetDataViewer ( eTypeViewer        eDataViewer  );

    // Get configuration functions
          uint              uiGetFrmLayout  ( void );
          eTypeTmpFileMgt    eGetTmpFileMgt ( void );
          uint              uiGetPrecision  ( void );
          uint              uiGetNbkMaxLns  ( void );
          uint              uiGetSpnPeriod  ( void );
          bool               bGetToolTips   ( void );
          uint              uiGetToolTipDly ( void );
          bool               bGetKeepNetLst ( void );
          int                iGetMainPosnX  ( void );
          int                iGetMainPosnY  ( void );
          int                iGetMainSizeW  ( void );
          int                iGetMainSizeH  ( void );

          int                iGetHelpPosnX  ( void );
          int                iGetHelpPosnY  ( void );
          int                iGetHelpSizeW  ( void );
          int                iGetHelpSizeH  ( void );

    const wxString      &  rosGetNetLstFile ( void );
    const wxString      &  rosGetSchemFiles ( void );
    const wxArrayString & roasGetSchemFiles ( void );
    const wxString      &  rosGetRecentFiles( void );
    const wxArrayString & roasGetRecentFiles( void );
    const wxString      &  rosGetDirLastAcc ( void );

          bool               bGetVerboseMode( void );
    const wxString      &  rosGetGuileProc  ( void );
          bool               bGetIncludeMode( void );
          bool               bGetEmbedMode  ( void );
          bool               bGetNoMungeMode( void );

    const wxString      &  rosGetSimEng     ( void );
          eTypeSimEng        eGetSimEng     ( void );
          eTypeCmd           eGetAnalysis   ( void );
          eTypeUnits         eGetPhaseUnits ( void );
          bool               bGetSyncSigSrcs( void );
          bool               bGetSyncTemps  ( void );
          bool               bGetAutoRegen  ( void );

    const wxString      &  rosGetEdaSuite   ( void );
          eTypeEDA           eGetEdaSuite   ( void );

    const wxString      &  rosGetDataViewer ( void );
          eTypeViewer        eGetDataViewer ( void );

    // The following function is intended for debugging
    void  Print( const wxString & rosPrefix=wxT("  ") );
};

//**************************************************************************************************

#endif // CONFIG_HPP
